<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'controllers/Restcall.php');
//session_start();
class Cart extends CI_Controller {
	 protected $nonce_chars;
	public function __construct()	
    {
        parent::__construct();
        $this->load->library('curl');
        $this->load->helper('menu_helper');
		$this->load->helper('cookie');	

     }


    public function index()
	{
	
		$obj_rest = new Restcall();
		$res_menu = $this->curl->simple_get($this->config->item('api_url').'wp-json/wp-api-menus/v2/menus/25');
		$menu_data = json_decode($res_menu);
		$last = $this->uri->total_segments();
		$cat_slug = $this->uri->segment($last);

		
		//$cart_data = $this->execute($options);
		$currency = $this->session->userdata('currency');
		$current_cart_data = $this->session->userdata('cart');

		//$size_of_session_estimate = strlen( serialize( $_SESSION ) );
		//print_r($current_cart_data);exit;
		foreach ($current_cart_data as $key => $value) {
			if($value['currency']!=$currency){
				$this->update_cart_currency();
			}
		}
		$cart_data = $this->session->userdata('cart');
		

		
		$product_ids = array();
		$new_product_arr = array();
		$cart_total = array();
		if(!empty($cart_data)){
			foreach ($cart_data as $cart) {
				$product_ids[] = $cart['product_id'];
			}
			$cart_product_ids = $product_ids;
			$product_ids = implode($product_ids,",");	
			//echo $product_ids;
			$options =  array(
							"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/products?include=".$product_ids,
							"method" =>"GET",
							"auth"	 =>	"true",
							"param"	 => array("include"=>$product_ids)
							 );
			
			$product = $obj_rest->_execute($options);
			
			$new_product_arr = array();
			foreach ($cart_product_ids as $cart_products_id){
				for($i=0;$i<count($product);$i++){
					if($cart_products_id==$product[$i]['id']){
						$new_product_arr[] = $product[$i];
					}
				}
			}	
			//print_r($new_product_arr);
			$cart_total = $this->calulate_cart_total();
		}
		//var_dump($cart_total);exit;	
		$stored_cookie = array();
		if (get_cookie('wishlist_cookie')) {		
			$stored_cookie  = get_cookie('wishlist_cookie');			
			$stored_cookie = explode(',', $stored_cookie);
		}
		/*print_r($cart_data);
		print_r($cart_total);exit;*/
		$data  = array(
						"menu_data"		=>$menu_data,
						"cart" 			=> $cart_data,
						"product" 		=> $new_product_arr,
						"cart_total"	=>$cart_total,
						"stored_cookie" => $stored_cookie,
						"meta_title"	=>"",
						"meta_keyword"	=>"",
						"meta_desc"		=> "",
						);

		$this->load->view('cart',$data);
	}	

	public function get_current_quantity($product_id){
		$cart_data = $this->session->userdata('cart');
		if(!empty($cart_data)){
			foreach($cart_data as $cart){
				if($cart['product_id']==$product_id){			
					return $cart['quantity'];
				}
			}	
		}
		
		return 0;
	}

	public function add_to_cart(){
		$this->load->library('curl');
		
		if(isset($_POST))
		 {
		 	$response = array();
		 	$this->form_validation->set_rules('id','Product','required|xss_clean');
		 	$this->form_validation->set_rules('quantity','Quantity','required|xss_clean');
		 	$currency = $this->session->userdata('currency');
		 	if($this->form_validation->run())
				{
					$product_id=$this->input->post('id',true);
					$quantity=$this->input->post('quantity',true);

					$cart_flag = $this->input->post('flag',true);
					//var_dump($quantity);exit;
					///check for current quantity
					if(isset($cart_flag) && $cart_flag=="update"){
						$curr_quantity = 0;
					}else{
						$curr_quantity = $this->get_current_quantity($product_id);
					}
					
					$new_quantity = $curr_quantity + $quantity;
					/*echo $new_quantity;
					exit;*/

					$args  = array(
									'product_id' => $product_id ,
									'quantity' => $new_quantity,
									'cart_item_data' => array('currency' => $currency )
								  );
					$res_cart = $this->curl->simple_post($this->config->item('api_url').'wp-json/wc/v2/cart/add',$args);
					$res_cart = json_decode($res_cart,true);
					//print_r($res_cart);exit;
					$res_cart =  array(
									'currency' 	=> $res_cart['currency'],
									'key'	   	=> $res_cart['key'],
									'product_id'=> $res_cart['product_id'],
									'quantity'	=> $res_cart['quantity'],
									'line_total'=> $res_cart['line_total']
									 );
					
					$current_cart_data = $this->session->userdata('cart');
					$new_cart  = array($res_cart['key'] => $res_cart );
					//$this->session->unset_userdata('cart');
					if(!$current_cart_data){
						$current_cart_data = array();
					}
					
					$final_cart = array_merge($current_cart_data,$new_cart);
					
					
					$cart_data = array("cart"=>$final_cart);
					$this->session->set_userdata($cart_data);
					$this->add_cart_db($cart_data);
					//print_r($cart_data);exit;	
					$stored_cookie  =  array();
					if (get_cookie('wishlist_cookie')) {	
						$stored_cookie  = get_cookie('wishlist_cookie');
						$stored_cookie = explode(',', $stored_cookie);
					}					

					/*$new_cookie = '';
					if (($key = array_search($product_id, $stored_cookie)) !== false) {
					    unset($stored_cookie[$key]);
					    if ($stored_cookie) {
					    	$new_cookie = implode(',', $stored_cookie);	    	
					    }
						delete_cookie('wishlist_cookie');
						if ($new_cookie) {
							set_cookie('wishlist_cookie',$new_cookie,'3600');				
						}
					}*/
					
					 $cartCountData = $this->session->userdata('cart');
					 //print_r($cartCountData);exit;
				    if($cartCountData){
				      $cartCount = count($cartCountData); 
				    }else{
				      $cartCount = 0;
				    }
				    $total = $this->calulate_cart_total();
				    //print_r($total);
					//print_r($stored_cookie);exit;
					$response['status'] = 'success';
					$response['message'] = "Product added to cart successfully";
					$response['cart_count']	= $cartCount;
					$response['cart_calculation'] = $total;
					$response['line_item_total'] = $res_cart['line_total'];
				}else{
					$response['status'] = 'error';
					$response['message']=$this->form_validation->error_string();
				}
				echo json_encode($response);
		 }
		 	
		
	}

	public function remove_from_cart(){
		if(isset($_POST))
		 {
		 	$response = array();
		 	$this->form_validation->set_rules('id','Cart Item Id','required|xss_clean');
			if($this->form_validation->run())
				{
					$cartData = $this->session->userdata('cart');
					$id=$this->input->post('id',true);
					
					foreach ($cartData as $key => $value) {
						# code...
						
						if($key==$id){
							unset($cartData[$key]);
						}
					}
					$cart_data = array("cart"=>$cartData);
					$this->session->set_userdata($cart_data);
					$this->add_cart_db($cart_data);
					if(!empty($cartData)){
						$cart_total = $this->calulate_cart_total();
						$cartCount = count($cartData);
						$response['status'] = 'success';
						$response['message'] = "Product removed from cart successfully";
						$response['cart_count']	= $cartCount;
						$response['cart_total'] = $cart_total;
						echo json_encode($response);
					}else{
						echo json_encode( array('total' => 0 ));
					}
					
					
					//print_r($cartData);
					//echo json_encode($cartData);
				}else{
					$response['status'] = 'error';
					$response['message']=$this->form_validation->error_string();
					echo json_decode($response);
				}
		}
	}


	public function clear_cart()
	{
		$this->session->unset_userdata('cart');
		$content = "";
		$this->add_cart_db($content);

	}

	public function calulate_cart_total(){
		$obj_rest = new Restcall();
		$cartData = $this->session->userdata('cart');
		$currency = $this->session->userdata('currency');
		//print_r($currency);exit;
		foreach ($cartData as $cart) {
			$param_cart[]  = array('product_id' => $cart['product_id'],'quantity'=>$cart['quantity'],"cart_item_data"=>  array('currency' => $currency) ); 
		}
		//echo "<pre>";
		
		$options =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v2/cart/add_multiple",
						"method" =>"POST",
						"auth"	 =>	"true",
						"param"	 => array(),
						"body"	 => $param_cart
						 );
		
		$cart_total = $obj_rest->_execute($options);
		return $cart_total;
		//print_r($cart_total);exit;
	}

	private function update_cart_currency(){
		$cartData = $this->session->userdata('cart');
		$currency = $this->session->userdata('currency');
		//print_r($currency);exit;
		$this->session->unset_userdata('cart');
		foreach ($cartData as $key => $value) {
			$data = array("product_id"=>$value['product_id'],"quantity"=>$value['quantity'],"currency"=>$currency);
			
			$this->update_cart($data);
		}
		//$this->add_to_cart();
	}

	private function update_cart($data){

		$this->load->library('curl');
		
		 	$response = array();
		 	
		 			$currency =$data['currency'];
		 	
					$product_id= $data['product_id'];
					$quantity= $data['quantity'];
					$args  = array(
									'product_id' => $product_id ,
									'quantity' => $quantity,
									'cart_item_data' => array('currency' => $currency )
								  );
					
					$res_cart = $this->curl->simple_post($this->config->item('api_url').'wp-json/wc/v2/cart/add',$args);
					$res_cart = json_decode($res_cart,true);
					$current_cart_data = $this->session->userdata('cart');
					$new_cart  = array($res_cart['key'] => $res_cart );
					//$this->session->unset_userdata('cart');
					if(!$current_cart_data){
						$current_cart_data = array();
					}
					
					$final_cart = array_merge($new_cart,$current_cart_data);

					//print_r($final_cart);exit;
					$cart_data = array("cart"=>$final_cart);
					$this->session->set_userdata($cart_data);
					$this->add_cart_db($cart_data);
					$stored_cookie  =  array();
					if (get_cookie('wishlist_cookie')) {	
						$stored_cookie  = get_cookie('wishlist_cookie');
						$stored_cookie = explode(',', $stored_cookie);
					}					

					$new_cookie = '';
					if (($key = array_search($product_id, $stored_cookie)) !== false) {
					    unset($stored_cookie[$key]);
					    if ($stored_cookie) {
					    	$new_cookie = implode(',', $stored_cookie);	    	
					    }
						delete_cookie('wishlist_cookie');
						if ($new_cookie) {
							set_cookie('wishlist_cookie',$new_cookie,'3600');				
						}
					}
					 
					
		 }
		 	
		public function calculate($pin=null){
			//print_r($pin);
		if(isset($_POST) && $_POST['pincode']!="")
		 {
		 	$this->form_validation->set_rules('pincode','pincode','required|xss_clean');
			if($this->form_validation->run())
				{

					$pincode=$this->input->post('pincode',true);
					$this->session->set_userdata("pincode",$pincode);
					switch ($pincode) {
						case '400601':
							$shipping_cost = "39.00";
							break;
						case '400602':
							$shipping_cost = "75.00";
							break;
						case '400603':
							$shipping_cost = "85.00";
							break;
						case '400604':
							$shipping_cost = "95.00";
							break;
						case '400605':
							$shipping_cost = "120.00";
							break;
						case '400606':
							$shipping_cost = "150.00";
							break;	
						
						default:
							$shipping_cost = "00.00";
							break;
					}
					echo json_encode($shipping_cost);
				}
		 }else{
		 	
		 	if($pin!=""){
		 		//$this->session->set_userdata("pincode",$pin);
					switch ($pin) {
						case '400601':
							$shipping_cost = "39.00";
							break;
						case '400602':
							$shipping_cost = "75.00";
							break;
						case '400603':
							$shipping_cost = "85.00";
							break;
						case '400604':
							$shipping_cost = "95.00";
							break;
						case '400605':
							$shipping_cost = "120.00";
							break;
						case '400606':
							$shipping_cost = "150.00";
							break;	
						
						default:
							$shipping_cost = "00.00";
							break;
					}
					return $shipping_cost;
		 	}else{
		 		//echo 123;
		 		$shipping_cost = "0";
		 		return $shipping_cost;
		 	}
		 }
	}

	public function add_cart_db($cart_data){
		$userdata = $this->session->userdata('user');
		//print_r($userdata);exit;
		$data = $this->master_model->getRecords('table_cart',array("user_id"=>$userdata['id']),'table_cart.*');
		if($userdata['id']!=""){
			if($cart_data==""){
				$this->master_model->deleteRecord('table_cart','id',$data[0]['id']);
			}else{
				if(count($data)>0){
					if(empty($cart_data['cart'])){
						$this->master_model->deleteRecord('table_cart','id',$data[0]['id']);
					}else{
						$input_array_data = array(
							'user_id' 	 => $userdata['id'],
							'contents' => serialize($cart_data)
							 );
			
						$this->master_model->updateRecord('table_cart',$input_array_data,array('id'=>$data[0]['id']));
					}
					
				}else{
					$input_array = array(
							'user_id' 	 => $userdata['id'],
							'contents' => serialize($cart_data)
							 );
					$this->master_model->insertRecord('table_cart',$input_array);
				}
			}	
		}
		
	}

	public function delete_cart_db(){
		$userdata = $this->session->userdata('user');
		$data = $this->master_model->getRecords('table_cart',array("user_id"=>$userdata['id']),'table_cart.*');
		if($userdata['id']!=""){
			if(count($data)>0){
				$this->master_model->deleteRecord('table_cart','id',$data[0]['id']);
			}

		}
	}
	

}