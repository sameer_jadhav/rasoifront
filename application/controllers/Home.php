<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//require_once(APPPATH.'controllers/Restcall.php');
class Home extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
		$this->load->helper('menu_helper');
		
    }

	public function index()
	{

		$this->load->library('curl');
		$this->load->helper('cookie');	
		//$this->load->library('../controllers/Resttest');
	

		$access_token="3252438771.1677ed0.04968556e643484087fa4ee48c1dc956";
		$photo_count=3;
		     
		$json_link="https://api.instagram.com/v1/users/self/media/recent/?";
		$json_link.="access_token={$access_token}&count={$photo_count}";



		//$json = file_get_contents($json_link);
		//print_r($json);exit;
		//$insta_feed = json_decode($json, true, 512, JSON_BIGINT_AS_STRING);

		$data  = array(
						//"insta_data"=>$insta_feed,
						"meta_title"	=>"Organic Skincare Products",
						"meta_keyword"	=>"organic, skinfood, skincare, handcrafted, microbatch, fresh, plant based, vegan, chemical free",
						"meta_desc"		=> "THE SKIN PANTRY is a range of handcrafted skincare products with organic, natural, plant based and wild sourced ingredients, that are created on the belief that if you shouldn’t put on your skin what you wouldn’t put into your mouth!",
						);
		$this->load->view('home',$data);
	}

	public function newletterSub(){
		$this->load->model('email_sending');
		$this->load->model('my_model');
		
		if(isset($_POST))
		 {
		 	$this->form_validation->set_rules('subscribe_email','Email ID','required|xss_clean');
		 	if($this->form_validation->run())
				{
					
					$newsletter_email=$this->input->post('subscribe_email',true);
					$email_id = $newsletter_email;
					$check = $this->master_model->getRecords('table_subscription', array('email_id'=>$newsletter_email), 'id', '' );
					if(count($check)>0){
						echo "duplicate";
					}else
					{
						
					$info_arr=array('from'=>'noreply@theskinpantry.com','to'=>$email_id,'subject'=>'Subscribed for TheSkinpantry','view'=>'subscribe');
					$other_info = array('email_id'=>$newsletter_email);
					$status = $this->email_sending->sendmail($info_arr,$other_info);

					$admin_id = 'contact@theskinpantry.com';
					$info_arr_admin=array('from'=>'noreply@theskinpantry.com','to'=>$admin_id,'subject'=>'New User Subscribed for TheSkinpantry','view'=>'subscribe-admin');
					$other_info_admin = array('email_id'=>$newsletter_email);
					$status_admin = $this->email_sending->sendmail($info_arr_admin,$other_info_admin);
						$resp = $this->my_model->subsciption(array('email_id'=>$newsletter_email,"status"=>1));
						if($status){
							echo "success";
						}else{ 
							echo "fail";
						}	
					}
					
				}
		 }
	}

	

	
}