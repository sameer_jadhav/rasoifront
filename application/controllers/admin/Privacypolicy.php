<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Privacypolicy extends Private_Admin {
	function __construct() {
        parent::__construct();        
	}
	public function index() { 
        $data['privacypolicy']  = $this->db->get('privacypolicy')->row();       
		$this->view('privacypolicy', $data);
	}

    public function update()
    {        
        $data = array(
                'content' => $this->input->post('content'),               
            );        
            $this->db->update('privacypolicy', $data, array('id' => 1));     
        redirect(admin_url('privacypolicy'));
    }

}