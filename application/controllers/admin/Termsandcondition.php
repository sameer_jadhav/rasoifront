<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Termsandcondition extends Private_Admin {
	function __construct() {
        parent::__construct();        
	}
	public function index() { 
        $data['termsandcondition']  = $this->db->get('termsandcondition')->row();       
		$this->view('termsandcondition', $data);
	}

    public function update()
    {        
        $data = array(
                'content' => $this->input->post('content'),               
            );        
            $this->db->update('termsandcondition', $data, array('id' => 1));     
        redirect(admin_url('termsandcondition'));
    }

}