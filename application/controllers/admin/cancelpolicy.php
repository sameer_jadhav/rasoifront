<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cancelpolicy extends Private_Admin {
	function __construct() {
        parent::__construct();        
	}
	public function index() { 
        $data['cancelpolicy']  = $this->db->get('cancelpolicy')->row();       
		$this->view('cancel-policy', $data);
	}

    public function update()
    {        
        $data = array(
                'content' => $this->input->post('content'),               
            );        
            $this->db->update('cancelpolicy', $data, array('id' => 1));     
        redirect(admin_url('cancelpolicy'));
    }

}