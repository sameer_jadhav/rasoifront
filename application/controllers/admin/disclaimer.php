<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Disclaimer extends Private_Admin {
	function __construct() {
        parent::__construct();        
	}
	public function index() { 
        $data['disclaimer']  = $this->db->get('disclaimer')->row();       
		$this->view('disclaimer', $data);
	}

    public function update()
    {        
        $data = array(
                'content' => $this->input->post('content'),               
            );        
            $this->db->update('disclaimer', $data, array('id' => 1));     
        redirect(admin_url('disclaimer'));
    }

}