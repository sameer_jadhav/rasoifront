<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Admin_Controller {
	function __construct() {
        parent::__construct();
        $this->load->model('login_model', 'model');
	}
	
	public function index() { 
	$this->_layout = "one-column";	
		$this->view('login');
	}	
	public function processLogin() {
		$data = $this->input->post();
	
		/*print_r($data);
		exit;*/
		$return = $this->model->login($data, 'admin');

		if($return) {
			$this->session->set_userdata('admin', $return);
			$page = 'home';
		} else {
			$this->session->set_flashdata('msg', showError('Please enter correct details.'));
			$page = 'login';
		}
		redirect(admin_url($page));
	}
	public function logout() {
		$this->session->unset_userdata('admin');
		redirect(admin_url('login'));
	}
	
}