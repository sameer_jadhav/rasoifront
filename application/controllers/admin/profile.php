<?php

class Profile extends Private_Admin {

    function __construct() {
        parent::__construct();
    }

    public function index() {    
        $data['result'] = $this->db->where('id', $this->_admindata['id'])->get('admin')->row();
        $this->view('profile', $data);
    }

    public function password_change() {
        $data['result'] = $this->db->where('id', $this->_admindata['id'])->get('admin')->row();       
        $this->view('password_change', $data);
    }

    public function save() {
         $data = array(                
                'name' => $this->input->post('name'),            
                //'mobile' => $this->input->post('mobile'),
        );
        if (!empty($_FILES['photo']['name'])) {
            $upload = _do_upload_photo('photo', 'admin');
            $data['photo'] = $upload;
        }   
         
        $this->db->update('admin', $data, array('id' => $this->_admindata['id']));
        $update = $this->db->affected_rows();
        if ($update) {
            $this->session->set_flashdata('msg', showSuccess("Profile Updated Successfully."));            
        }
        redirect(admin_url('profile'));
    
    }

    public function password_save() {

        $id = $this->input->post('id');
        $password = $this->input->post('password');
        $new_password = $this->input->post('new_password');
        $result = $this->db->where('id', $id)->get('admin')->row();
        if ($result->password == $password) {
            $data = array(
                'password' => $this->input->post('new_password'),
            );
            $this->db->where('id', $id);
            $insert = $this->db->update('admin', $data);
            if ($insert) {
                $this->session->set_flashdata('msg', showSuccess("Password Updated Successfully Please Login Again."));
                $this->session->unset_userdata('user');
                redirect(admin_url('login'));
            } else {
                $this->session->set_flashdata('msg', showError("Something went wrong!"));
            }
        } else {

            $this->session->set_flashdata('msg', showError("Old Password Doest Matched!"));
            redirect(admin_url('profile/password_change'));
        }
    }


}
