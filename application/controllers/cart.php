<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'controllers/Restcall.php');
if(!isset($_SESSION)) 
	    { 
	        session_start(); 
	    }
class Cart extends CI_Controller {
	 protected $nonce_chars;
	public function __construct()	
    {
        parent::__construct();
        $this->load->library('curl');
        $this->load->helper('menu_helper');
		$this->load->helper('cookie');	

     }


    public function index()
	{
	
		$obj_rest = new Restcall();
		$res_menu = $this->curl->simple_get($this->config->item('api_url').'wp-json/wp-api-menus/v2/menus/25');
		$menu_data = json_decode($res_menu);
		$last = $this->uri->total_segments();
		$cat_slug = $this->uri->segment($last);

		$product_ids = array();
		$new_product_arr = array();
		$cart_total = array();
		$cart_product = array();
		$cart_data = array();

		//$cart_data = $this->execute($options);
		$currency = $this->session->userdata('currency');
		$current_cart_data = $this->session->userdata('cart');

		//$size_of_session_estimate = strlen( serialize( $_SESSION ) );
		//print_r($current_cart_data);exit;
		if(!empty($current_cart_data)){
			foreach ($current_cart_data as $key => $value) {
				if($value['currency']!=$currency){
					$this->update_cart_currency();
				}
			}
			//$this->session->unset_userdata('cart');
			$cart_data = $this->session->userdata('cart');
		

		//print_r($cart_data);
		
		
		if(!empty($cart_data)){
			foreach ($cart_data as $cart) {
				$product_ids[] = $cart['product_id'];
			}
			$cart_product_ids = $product_ids;
			$product_ids = implode($product_ids,",");	
			//echo $product_ids;
			$options =  array(
							"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/products?include=".$product_ids."&per_page=70",
							"method" =>"GET",
							"auth"	 =>	"true",
							"param"	 => array("include"=>$product_ids,"per_page"=>70)
							 );
			
			$product = $obj_rest->_execute($options);
			
			//print_r($product);exit;
			$new_product_arr = array();
			foreach ($cart_product_ids as $cart_products_id){
				for($i=0;$i<count($product);$i++){
					if($cart_products_id==$product[$i]['id']){
						$new_product_arr[] = $product[$i];
					}
				}
			}	
			$cart_product = $new_product_arr;
			//$cart_shipping_cost  = $this->calc_ship($cart_product,$cart_data);
			$cart_total = $this->calulate_cart_total();
			//print_r($cart_total);exit;
			$shipping_cost_tax = round($cart_total['shipping_total']*(0.05));
			$cart_total['cart_subtotal'] =  round($cart_total['subtotal'] / (1.05));
			$cart_total['cart_tax'] =  round($cart_total['subtotal'] - $cart_total['cart_subtotal'] + $shipping_cost_tax);
			
		}
	}
		//print_r($cart);
		//print_r($cart_total);exit;	
		$stored_cookie = array();
		if (get_cookie('wishlist_cookie')) {		
			$stored_cookie  = get_cookie('wishlist_cookie');			
			$stored_cookie = explode(',', $stored_cookie);
		}
		

		$options =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/products?featured=true&per_page=70",
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 => array("featured"=>"true","per_page"=>70)
						 );
		
		$featured = $obj_rest->_execute($options);


		$options_cat =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/products/categories",
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 => array()
						 );
		//print_r($options_cat);
		$category = $obj_rest->_execute($options_cat);

		unset($category[0]);
		$category = array_values($category);

		$options_product =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/products?status=publish&per_page=70",
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 => array("status"=>"publish","per_page"=>70)
						 );
		
		$products = $obj_rest->_execute($options_product);

		$home_sql = "select wm.meta_key,wm.meta_value from wp_posts as wp join wp_postmeta as wm ON wm.post_id=wp.ID where wp.post_status='publish' and wp.post_type='page' and wp.post_name= 'cart' AND wm.meta_key IN ('seo_description','seo_title','seo_keyword','seo_h1')";
		$home_query = $this->db->query($home_sql);
        $seo_content = $home_query->result_array();
        
        foreach ($seo_content as $seocontent) {
        	
        	switch ($seocontent['meta_key']) {
        		case 'seo_description':
        			$meta_desc = $seocontent['meta_value'];
        			break;
        		case 'seo_h1':
        			$meta_h1 = $seocontent['meta_value'];
        			break;
        		case 'seo_keyword':
        			$meta_keyword = $seocontent['meta_value'];
        			break;
        		case 'seo_title':
        			$meta_title = $seocontent['meta_value'];
        			break;
        		
        		default:
        			# code...
        			break;
        	}
        }
		

		$data  = array(
						"menu_data"		=>$menu_data,
						"cart" 			=> $cart_data,
						"product_fly"	=> $cart_product,
						"product" 		=> $new_product_arr,
						"cart_total"	=>$cart_total,
						"featured"		=> $featured,
						"stored_cookie" => $stored_cookie,
						"category" 	  => $category,
						"products"	  => $products,
						"meta_title"	=>$meta_title,
						"meta_keyword"	=>$meta_keyword,
						"meta_desc"		=> $meta_desc,
						"meta_h1"		=> $meta_h1,
						);

		$this->load->view('cart',$data);
	}	

	public function get_current_quantity($product_id,$variation_id){
		$cart_data = $this->session->userdata('cart');
		if(!empty($cart_data)){
			foreach($cart_data as $cart){
				if($cart['product_id']==$product_id && $cart['variation_id']==$variation_id){			
					return $cart['quantity'];
				}
			}	
		}
		
		return 0;
	}

	public function getCartfly(){
		$obj_rest = new Restcall();
		$currency = $this->session->userdata('currency');
		$current_cart_data = $this->session->userdata('cart');
		$product_ids = array();
		$new_product_arr = array();
		$cart_total = array();
		$cart_product = array();
		$cart_data = array();
		//$size_of_session_estimate = strlen( serialize( $_SESSION ) );
		//print_r($current_cart_data);exit;
		if(!empty($current_cart_data)){
			foreach ($current_cart_data as $key => $value) {
				if($value['currency']!=$currency){
					$this->update_cart_currency();
				}
			}
		//$this->session->unset_userdata('cart');
			$cart_data = $this->session->userdata('cart');
		
		

		//print_r($cart_data);exit;
		
		

		if(!empty($cart_data)){
			foreach ($cart_data as $cart) {
				$product_ids[] = $cart['product_id'];
			}
			$cart_product_ids = $product_ids;
			$product_ids = implode($product_ids,",");	
			//echo $product_ids;
			$options =  array(
							"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/products?include=".$product_ids,
							"method" =>"GET",
							"auth"	 =>	"true",
							"param"	 => array("include"=>$product_ids)
							 );
			
			$product = $obj_rest->_execute($options);
			
			//print_r($product);exit;
			$new_product_arr = array();
			foreach ($cart_product_ids as $cart_products_id){
				for($i=0;$i<count($product);$i++){
					if($cart_products_id==$product[$i]['id']){
						$new_product_arr[] = $product[$i];
					}
				}
			}	
			$cart_product = $new_product_arr;
			//print_r($new_product_arr);
			$cart_total = $this->calulate_cart_total();
			//print_r($cart_total);exit;
			$shipping_tax = $cart_total['shipping_total'] * 0.05;
			$cart_total['cart_subtotal'] =  round($cart_total['subtotal'] / (1.05));
			$cart_total['cart_tax'] =  round($cart_total['subtotal'] - $cart_total['cart_subtotal'] + $shipping_tax);
		}
	}
		$data  = array(
						
						"cart" 			=> $cart_data,
						"product_fly"	=> $cart_product,
						"cart_total"	=>$cart_total,
						
						);
		return $data;
		
	}

	public function cart_sidebar(){
		$obj_rest = new Restcall();
		$currency = $this->session->userdata('currency');
		$current_cart_data = $this->session->userdata('cart');

		foreach ($current_cart_data as $key => $value) {
			if($value['currency']!=$currency){
				$this->update_cart_currency();
			}
		}

		$cart_data = $this->session->userdata('cart');
		$cart_total = array();
		if(!empty($cart_data)){
			$cart_total = $this->calulate_cart_total();
			$cart_total['cart_subtotal'] =  round($cart_total['subtotal'] / (1.05));
			$cart_total['cart_tax'] =  round($cart_total['subtotal'] - $cart_total['cart_subtotal'] + ($cart_total['shipping_total']*0.05 ));
		}
		$data  = array(
						
						"cart_total"	=>$cart_total,
						);
		$this->load->view('cart-sidebar',$data);
	}

	public function refresh_cart(){
		if(!isset($_SESSION)) 
	    { 
	        session_start(); 
	    }
		$obj_rest = new Restcall();

		$currency = $this->session->userdata('currency');
		$current_cart_data = $this->session->userdata('cart');

		//$size_of_session_estimate = strlen( serialize( $_SESSION ) );
		//print_r($current_cart_data);exit;
		foreach ($current_cart_data as $key => $value) {
			if($value['currency']!=$currency){
				$this->update_cart_currency();
			}
		}
		//$this->session->unset_userdata('cart');
		$cart_data = $this->session->userdata('cart');
		

		//print_r($cart_data);exit;
		
		$product_ids = array();
		$new_product_arr = array();
		$cart_total = array();
		$cart_product = array();

		if(!empty($cart_data)){
			foreach ($cart_data as $cart) {
				$product_ids[] = $cart['product_id'];
			}
			$cart_product_ids = $product_ids;
			$product_ids = implode($product_ids,",");	
			//echo $product_ids;
			$options =  array(
							"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/products?include=".$product_ids."&per_page=70",
							"method" =>"GET",
							"auth"	 =>	"true",
							"param"	 => array("include"=>$product_ids,"per_page"=>70)
							 );
			
			$product = $obj_rest->_execute($options);
			
			//print_r($product);
			$new_product_arr = array();
			foreach ($cart_product_ids as $cart_products_id){
				for($i=0;$i<count($product);$i++){
					if($cart_products_id==$product[$i]['id']){
						$new_product_arr[] = $product[$i];
					}
				}
			}	
			$cart_product = $new_product_arr;
			//print_r($new_product_arr);
			$cart_total = $this->calulate_cart_total();
			$cart_total['cart_subtotal'] =  round($cart_total['subtotal'] / (1.18));
			$cart_total['cart_tax'] =  round($cart_total['subtotal'] - $cart_total['cart_subtotal']);
		}

		$data  = array(
						
						"cart" 			=> $cart_data,
						"product_fly"	=> $cart_product,
						"cart_total"	=>$cart_total,
						
						);
		
		$this->load->view('cart-fly',$data);

	}

	public function refresh_cart_page(){
		$obj_rest = new Restcall();
		$currency = $this->session->userdata('currency');
		$current_cart_data = $this->session->userdata('cart');

		//$size_of_session_estimate = strlen( serialize( $_SESSION ) );
		//print_r($current_cart_data);exit;
		foreach ($current_cart_data as $key => $value) {
			if($value['currency']!=$currency){
				$this->update_cart_currency();
			}
		}
		//$this->session->unset_userdata('cart');
		$cart_data = $this->session->userdata('cart');
		

		//print_r($cart_data);exit;
		
		$product_ids = array();
		$new_product_arr = array();
		$cart_total = array();
		$cart_product = array();

		if(!empty($cart_data)){
			foreach ($cart_data as $cart) {
				$product_ids[] = $cart['product_id'];
			}
			$cart_product_ids = $product_ids;
			$product_ids = implode($product_ids,",");	
			//echo $product_ids;
			$options =  array(
							"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/products?include=".$product_ids."&per_page=70",
							"method" =>"GET",
							"auth"	 =>	"true",
							"param"	 => array("include"=>$product_ids,"per_page"=>70)
							 );
			
			$product = $obj_rest->_execute($options);
			
			//print_r($product);exit;
			$new_product_arr = array();
			foreach ($cart_product_ids as $cart_products_id){
				for($i=0;$i<count($product);$i++){
					if($cart_products_id==$product[$i]['id']){
						$new_product_arr[] = $product[$i];
					}
				}
			}	
			$cart_product = $new_product_arr;
			//print_r($new_product_arr);
			$cart_total = $this->calulate_cart_total();
			$cart_total['cart_subtotal'] =  round($cart_total['subtotal'] / (1.18));
			$cart_total['cart_tax'] =  round($cart_total['subtotal'] - $cart_total['cart_subtotal']);
		}

		$data  = array(
						
						"cart" 			=> $cart_data,
						"product"	=> $cart_product,
						"cart_total"	=>$cart_total,
						
						);
		$this->load->view('cart-page-ajax',$data);
	}

	public function add_to_cart(){
		if(!isset($_SESSION)) 
	    { 
	        session_start(); 
	    }
		$this->load->library('curl');
		
		if(isset($_POST))
		 {
		 	$response = array();
		 	$this->form_validation->set_rules('id','Product','required|xss_clean');
		 	$this->form_validation->set_rules('quantity','Quantity','required|xss_clean');
		 	$this->form_validation->set_rules('variation_id','Variation','required|xss_clean');
		 	$currency = $this->session->userdata('currency');
		 	if($this->form_validation->run())
				{
					$product_id=$this->input->post('id',true);
					$quantity=$this->input->post('quantity',true);
					$variation_id=$this->input->post('variation_id',true);
					$product_weight = $this->input->post('product_weight',true);
					$cart_flag = $this->input->post('flag',true);
					//var_dump($quantity);exit;
					///check for current quantity
					//exit;
					if(isset($cart_flag) && $cart_flag=="var_update"){
						$variation_id=$this->input->post('variation_id',true);
						$curr_quantity = $this->get_current_quantity($product_id,$variation_id);
						$cartData = $this->session->userdata('cart');
						$cart_id=$this->input->post('cart_id',true);
						
						foreach ($cartData as $key => $value) {
							# code...
							
							if($key==$cart_id){
								unset($cartData[$key]);
							}
						}
						$cart_data = array("cart"=>$cartData);
						$this->session->set_userdata($cart_data);
						$this->add_cart_db($cart_data);
					}
					else if(isset($cart_flag) && $cart_flag=="update"){
						$curr_quantity = 0;
					}else{
						$curr_quantity = $this->get_current_quantity($product_id,$variation_id);
					}
					
					$new_quantity = $curr_quantity + $quantity;
					/*echo $new_quantity;
					exit;*/

					$args  = array(
									'product_id' => $product_id ,
									'quantity' => $new_quantity,
									'variation_id' => $variation_id,
									'cart_item_data' => array('currency' => $currency ,"weight"=>$product_weight)
								  );
					//print_r($args);exit;
					$res_cart = $this->curl->simple_post($this->config->item('api_url').'wp-json/wc/v2/cart/add',$args);
					$res_cart = json_decode($res_cart,true);
					//print_r($res_cart);exit;
					$res_cart =  array(
									'currency' 	=> $res_cart['currency'],
									'key'	   	=> $res_cart['key'],
									'product_id'=> $res_cart['product_id'],
									'quantity'	=> $res_cart['quantity'],
									'weight'	=> $res_cart['weight'],
									'variation_id' => $res_cart['variation_id'],
									'line_total'=> $res_cart['line_total']
									 );
					
					$current_cart_data = $this->session->userdata('cart');
					$new_cart  = array($res_cart['key'] => $res_cart );
					//$this->session->unset_userdata('cart');
					if(!$current_cart_data){
						$current_cart_data = array();
					}
					
					$final_cart = array_merge($current_cart_data,$new_cart);
					
					
					$cart_data = array("cart"=>$final_cart);
					$this->session->set_userdata($cart_data);
					$this->add_cart_db($cart_data);
					//print_r($cart_data);exit;	
					$stored_cookie  =  array();
					if (get_cookie('wishlist_cookie')) {	
						$stored_cookie  = get_cookie('wishlist_cookie');
						$stored_cookie = explode(',', $stored_cookie);
					}					

					
					
					 $cartCountData = $this->session->userdata('cart');
					 //print_r($cartCountData);exit;
				    if($cartCountData){
				      $cartCount = count($cartCountData); 
				    }else{
				      $cartCount = 0;
				    }
				    $total = $this->calulate_cart_total();
				    //print_r($total);
					//print_r($stored_cookie);exit;
					$response['status'] = 'success';
					$response['message'] = "Product added to cart successfully";
					$response['cart_count']	= $cartCount;
					$response['cart_calculation'] = $total;
					$response['line_item_total'] = $res_cart['line_total'];
				}else{
					$response['status'] = 'error';
					$response['message']=$this->form_validation->error_string();
				}
				echo json_encode($response);
		 }
		 	
		
	}

	public function remove_from_cart(){
		if(isset($_POST))
		 {
		 	$response = array();
		 	$this->form_validation->set_rules('id','Cart Item Id','required|xss_clean');
			if($this->form_validation->run())
				{
					$cartData = $this->session->userdata('cart');
					$id=$this->input->post('id',true);
					
					foreach ($cartData as $key => $value) {
						# code...
						
						if($key==$id){
							unset($cartData[$key]);
						}
					}
					$cart_data = array("cart"=>$cartData);
					$this->session->set_userdata($cart_data);
					$this->add_cart_db($cart_data);
					if(!empty($cartData)){
						$cart_total = $this->calulate_cart_total();
						$cartCount = count($cartData);
						$response['status'] = 'success';
						$response['message'] = "Product removed from cart successfully";
						$response['cart_count']	= $cartCount;
						$response['cart_total'] = $cart_total;
						echo json_encode($response);
					}else{
						echo json_encode( array('total' => 0 ));
					}
					
					
					//print_r($cartData);
					//echo json_encode($cartData);
				}else{
					$response['status'] = 'error';
					$response['message']=$this->form_validation->error_string();
					echo json_decode($response);
				}
		}
	}


	public function clear_cart()
	{
		$this->session->unset_userdata('cart');
		$content = "";
		$this->add_cart_db($content);

	}

	public function calulate_cart_total(){
		if(!isset($_SESSION)) 
	    { 
	        session_start(); 
	    }
		$obj_rest = new Restcall();
		$cartData = $this->session->userdata('cart');
		$currency = $this->session->userdata('currency');
		//print_r($currency);exit;

		foreach ($cartData as $cart) {
			$param_cart[]  = array('product_id' => $cart['product_id'],"variation_id"=>$cart['variation_id'],'quantity'=>$cart['quantity'],"cart_item_data"=>  array('currency' => $currency) ); 
		}

		//echo "<pre>";
		
		$options =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v2/cart/add_multiple",
						"method" =>"POST",
						"auth"	 =>	"true",
						"param"	 => array(),
						"body"	 => $param_cart
						 );
		
		$cart_total = $obj_rest->_execute($options);
		$cart_shipping_cost = $this->calc_ship($cartData);
		$cart_total['shipping_total'] = $cart_shipping_cost;
		$shipping_cost_tax = $cart_shipping_cost*0.05;
		$cart_total['total'] = round($cart_total['total']+ $cart_shipping_cost+$shipping_cost_tax);
		//print_r($cart_total);exit;
		return $cart_total;
		
	}

	private function update_cart_currency(){
		$cartData = $this->session->userdata('cart');
		$currency = $this->session->userdata('currency');
		//print_r($currency);exit;
		$this->session->unset_userdata('cart');
		foreach ($cartData as $key => $value) {
			$data = array("product_id"=>$value['product_id'],"quantity"=>$value['quantity'],"currency"=>$currency);
			
			$this->update_cart($data);
		}
		//$this->add_to_cart();
	}

	private function update_cart($data){

		$this->load->library('curl');
		
		 	$response = array();
		 	
		 			$currency =$data['currency'];
		 	
					$product_id= $data['product_id'];
					$quantity= $data['quantity'];
					$args  = array(
									'product_id' => $product_id ,
									'quantity' => $quantity,
									'cart_item_data' => array('currency' => $currency )
								  );
					
					$res_cart = $this->curl->simple_post($this->config->item('api_url').'wp-json/wc/v2/cart/add',$args);
					$res_cart = json_decode($res_cart,true);
					$current_cart_data = $this->session->userdata('cart');
					$new_cart  = array($res_cart['key'] => $res_cart );
					//$this->session->unset_userdata('cart');
					if(!$current_cart_data){
						$current_cart_data = array();
					}
					
					$final_cart = array_merge($new_cart,$current_cart_data);

					//print_r($final_cart);exit;
					$cart_data = array("cart"=>$final_cart);
					$this->session->set_userdata($cart_data);
					$this->add_cart_db($cart_data);
					$stored_cookie  =  array();
					if (get_cookie('wishlist_cookie')) {	
						$stored_cookie  = get_cookie('wishlist_cookie');
						$stored_cookie = explode(',', $stored_cookie);
					}					

					$new_cookie = '';
					if (($key = array_search($product_id, $stored_cookie)) !== false) {
					    unset($stored_cookie[$key]);
					    if ($stored_cookie) {
					    	$new_cookie = implode(',', $stored_cookie);	    	
					    }
						delete_cookie('wishlist_cookie');
						if ($new_cookie) {
							set_cookie('wishlist_cookie',$new_cookie,'3600');				
						}
					}
					 
					
		 }

		 public function validate_shipping(){

		 	if(isset($_POST) && $_POST['pincode']!="")
			 {
			 	$this->form_validation->set_rules('pincode','pincode','required|xss_clean');
				if($this->form_validation->run())
					{

						$pincode=$this->input->post('pincode',true);
						
						$this->session->set_userdata("pincode",$pincode);
						
						$pincode_data = $this->master_model->getRecords("table_pincode",array("pincode"=>$pincode),"charges");
				 		if(!empty($pincode_data)){
				 			$shipping_cost = 1;
				 		}else{
				 			$shipping_cost = 0;
				 		}
				 		//$this->session->set_userdata("shipping_cost",$shipping_cost);
						echo json_encode($shipping_cost);
					}
			 }
		 	

		 	
			//$this->session->set_userdata("shipping_area_code",$area_code);
		 }

		public function calc_ship($cart_data){
			$cart_data = array_values($cart_data);
			//print_r($cart_data);
			$final_weight = 0;
			for($i=0;$i<count($cart_data);$i++){
				
				$weight = "";

                     $weight = $cart_data[$i]['weight'] * $cart_data[$i]['quantity'];
                        
                     $final_weight = $final_weight+$weight;
                    
                
			}
			$shipping_data = $this->master_model->getRecords("table_shipping",array(),"shipping_rate");
			$weight_kg = $final_weight/1000 ;
			//print_r($shipping_data);exit;
			
			$rate1 = $shipping_data[0]['shipping_rate'] ;
			$rate2 = $shipping_data[1]['shipping_rate'];
			$whole_kg = floor($weight_kg); 
			$fraction_kg = $weight_kg - $whole_kg; // .25
			//echo $fraction_kg;exit;
			$kg_rate = $whole_kg * $rate2;
			if($kg_rate!=0 && $fraction_kg>0.5){
				$gm_rate = $shipping_data[1]['shipping_rate'];
			}else if($kg_rate==0 && $fraction_kg<=0.5){
				$gm_rate = $shipping_data[0]['shipping_rate'];
			}else if($fraction_kg>0.5){
				$gm_rate = $shipping_data[1]['shipping_rate'];
			}else  if($fraction_kg!=0){
				$gm_rate = $shipping_data[0]['shipping_rate'];	
			}else {
				$gm_rate = 0;
			}
			//echo $gm_rate;exit;
			
			$shipping_cost = $kg_rate+$gm_rate;
			//echo $shipping_cost;exit;
			$this->session->set_userdata("shipping_cost",$shipping_cost);
			return $shipping_cost;
			
		}
		 	
		public function calculate($pin=null){
			//print_r($pin);
		if(isset($_POST) && $_POST['pincode']!="")
		 {
		 	$this->form_validation->set_rules('pincode','pincode','required|xss_clean');
			if($this->form_validation->run())
				{

					$pincode=$this->input->post('pincode',true);
					
					//$this->session->set_userdata("pincode",$pincode);
					
					
					$pincode_data = $this->master_model->getRecords("table_pincode",array("pincode"=>$pincode),"charges");
			 		if(!empty($pincode_data)){
			 			$shipping_cost = 1;
			 		}else{
			 			$shipping_cost = 0;
			 		}
					echo json_encode($shipping_cost);
				}
		 }else{
		 	
		 	if($pin!=""){/*
		 		//$this->session->set_userdata("pincode",$pin);
					switch ($pin) {
						case '400601':
							$shipping_cost = "39.00";
							break;
						case '400602':
							$shipping_cost = "75.00";
							break;
						case '400603':
							$shipping_cost = "85.00";
							break;
						case '400604':
							$shipping_cost = "95.00";
							break;
						case '400605':
							$shipping_cost = "120.00";
							break;
						case '400606':
							$shipping_cost = "150.00";
							break;	
						
						default:
							$shipping_cost = "00.00";
							break;
					}
					return $shipping_cost;
		 	*/}else{
		 		//echo 123;
		 		/*$shipping_cost = "0";
		 		return $shipping_cost;*/
		 	}
		 }
	}

	public function add_cart_db($cart_data){
		$userdata = $this->session->userdata('user');
		//print_r($userdata);exit;
		$data = $this->master_model->getRecords('table_cart',array("user_id"=>$userdata['id']),'table_cart.*');
		if($userdata['id']!=""){
			if($cart_data==""){
				$this->master_model->deleteRecord('table_cart','id',$data[0]['id']);
			}else{
				if(count($data)>0){
					if(empty($cart_data['cart'])){
						$this->master_model->deleteRecord('table_cart','id',$data[0]['id']);
					}else{
						$input_array_data = array(
							'user_id' 	 => $userdata['id'],
							'contents' => serialize($cart_data)
							 );
			
						$this->master_model->updateRecord('table_cart',$input_array_data,array('id'=>$data[0]['id']));
					}
					
				}else{
					$input_array = array(
							'user_id' 	 => $userdata['id'],
							'contents' => serialize($cart_data)
							 );
					$this->master_model->insertRecord('table_cart',$input_array);
				}
			}	
		}
		
	}

	public function delete_cart_db(){
		$userdata = $this->session->userdata('user');
		$data = $this->master_model->getRecords('table_cart',array("user_id"=>$userdata['id']),'table_cart.*');
		if($userdata['id']!=""){
			if(count($data)>0){
				$this->master_model->deleteRecord('table_cart','id',$data[0]['id']);
			}

		}
	}
	

}