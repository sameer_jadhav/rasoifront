<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'controllers/cart.php');

class Custom404ctrl extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        /*
        $check_auth_client = $this->MyModel->check_auth_client();
		if($check_auth_client != true){
			die($this->output->get_output());
		}
		*/
		$this->load->helper('menu_helper');
    }

	public function index()
	{
		$this->load->library('curl');
		/*$res_menu = $this->curl->simple_get($this->config->item('api_url').'wp-json/wp-api-menus/v2/menus/25');
		$menu_data = json_decode($res_menu);*/
		
		$this->load->model('master_model');
		$master = $this->master_model->getRow('master');		
		$about_images = $this->master_model->getRecords('about_images');		

		$obj_cart = new cart();	
		$cart_data = $obj_cart->getCartfly();

		$data  = array(
						"master"=>$master,
						"about_images" =>$about_images,
						"meta_title"	=>"Rasoi Tatva | Spices Online in Mumbai",
						"cart" 			=> $cart_data['cart'],
						"product_fly"	=> $cart_data['product_fly'],
						"cart_total"	=> $cart_data['cart_total'],
						"meta_keyword"	=> "Buy best Indian Spices in Mumbai",
						"meta_desc"		=> "Whole Spices | Ground Spices | Select Blends Buy best Indian spices  in Mumbai with ‘just out of farm’ freshness & flavour online from Rasoi Tatva.",
						);
		$this->load->view('404',$data);
	}


	
}