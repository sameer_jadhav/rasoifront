<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faqs extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        /*
        $check_auth_client = $this->MyModel->check_auth_client();
		if($check_auth_client != true){
			die($this->output->get_output());
		}
		*/
		$this->load->helper('menu_helper');
    }

	public function index()
	{
		$this->load->library('curl');
		
		$this->load->model('master_model');
		$faqs = $this->master_model->getRecords('faq');
		//print_r($faqs);exit;
		$home_sql = "select wm.meta_key,wm.meta_value from wp_posts as wp join wp_postmeta as wm ON wm.post_id=wp.ID where wp.post_status='publish' and wp.post_type='page' and wp.post_name= 'faq' AND wm.meta_key IN ('seo_description','seo_title','seo_keyword','seo_h1')";
		$home_query = $this->db->query($home_sql);
        $seo_content = $home_query->result_array();
        
        foreach ($seo_content as $seocontent) {
        	
        	switch ($seocontent['meta_key']) {
        		case 'seo_description':
        			$meta_desc = $seocontent['meta_value'];
        			break;
        		case 'seo_h1':
        			$meta_h1 = $seocontent['meta_value'];
        			break;
        		case 'seo_keyword':
        			$meta_keyword = $seocontent['meta_value'];
        			break;
        		case 'seo_title':
        			$meta_title = $seocontent['meta_value'];
        			break;
        		
        		default:
        			# code...
        			break;
        	}
        }
		$data  = array("faqs"=>$faqs,"meta_title"	=>$meta_title,
						"meta_keyword"	=>$meta_keyword,
						"meta_desc"		=> $meta_desc,
						"meta_h1"		=> $meta_h1);

		$this->load->view('faqs',$data);
	}

	
	
}