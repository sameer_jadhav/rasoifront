<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'controllers/Restcall.php');
require_once(APPPATH.'controllers/cart.php');
class Home extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
		$this->load->helper('menu_helper');
		
		//$this->load->library('../controllers/cart');

		
    }

	public function index()
	{
		$obj_cart = new cart();	
		
		$this->load->library('curl');
		$this->load->helper('cookie');
		$obj_rest = new Restcall();	
		$last = $this->uri->total_segments();
		$slug = $this->uri->segment($last);
		if ($slug=="") {
			$slug = 'home';			
		}
		//$this->load->library('../controllers/Resttest');

		$sql = "SELECT * FROM testimonial WHERE FIND_IN_SET('$slug', pages) ORDER BY sequence ASC";		
		$query = $this->db->query($sql);
        $testimonial = $query->result_array();
		//print_r($testimonial);exit;
		$home_sql = "select wm.meta_key,wm.meta_value from wp_posts as wp join wp_postmeta as wm ON wm.post_id=wp.ID where wp.post_status='publish' and wp.post_type='page' and wp.post_name= 'home' AND wm.meta_key IN ('seo_description','seo_title','seo_keyword','seo_h1')";
		$home_query = $this->db->query($home_sql);
        $seo_content = $home_query->result_array();
        
        foreach ($seo_content as $seocontent) {
        	
        	switch ($seocontent['meta_key']) {
        		case 'seo_description':
        			$meta_desc = $seocontent['meta_value'];
        			break;
        		case 'seo_h1':
        			$meta_h1 = $seocontent['meta_value'];
        			break;
        		case 'seo_keyword':
        			$meta_keyword = $seocontent['meta_value'];
        			break;
        		case 'seo_title':
        			$meta_title = $seocontent['meta_value'];
        			break;
        		
        		default:
        			# code...
        			break;
        	}
        }

       
        $fields = "id,caption,media_type,media_url,permalink,thumbnail_url,timestamp,username";
		//$access_token="IGQVJYbmNUWk9SMnZAyYllLSExBTDZAWSmE3Wkszelc5UG5sTUJTZAkZAtZAzEyQ1FHVE5Kc2FrWERiQThubUZAId2RYa1BzaVVnZAjgxLXI5NHFfY2NNWEJxRmdYamNzbUlhLWhsR0xmR253";
		$access_token="IGQVJWV0hxUVBoRTJTRnJreEZAxVk1kbElHbm1LZAmY2cmhEdnpYVFRvZAGhWRGJ1WldKNkh0b3AwMldLbU5wbkhpV1I3dFQtenp4c2Vram5NdW4yWEtxMDh0R1dwTTBaeFlZAam1yamZAvNU85X2dXd2VTTwZDZD";

		$photo_count=3;
		     
		$json_link="https://graph.instagram.com/me/media?fields={$fields}";
		$json_link.="&access_token={$access_token}&limit={$photo_count}";
		$stored_cookie = array();
		if (get_cookie('wishlist_cookie')) {		
			$stored_cookie  = get_cookie('wishlist_cookie');			
			$stored_cookie = explode(',', $stored_cookie);
		}
		//print_r($stored_cookie);exit;
		//echo $this->config->item('api_url')."wp-json/wc/v3/products?featured=true";exit;

		$options_cat =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/products/categories",
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 => array()
						 );
		//print_r($options_cat);
		$category = $obj_rest->_execute($options_cat);
		///get cart flyer
		$cart_data = $obj_cart->getCartfly();
		//print_r($cart_data);exit;

		
		unset($category[0]);
		$category = array_values($category);
		

		$options_product =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/products?status=publish&per_page=40&orderby=menu_order&order=asc",
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 => array("status"=>"publish","per_page"=>40,"orderby"=>"menu_order","order"=>"asc")
						 );
		
		$products = $obj_rest->_execute($options_product);
		 //print_r($products);exit;

		
		$options =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/products?featured=true&status=publish&orderby=menu_order&order=asc",
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 => array("featured"=>"true","status"=>"publish","orderby"=>"menu_order","order"=>"asc")
						 );
		
		$featured = $obj_rest->_execute($options);


		//print_r($featured);exit;

		$json = file_get_contents($json_link);
		//print_r($json);exit;
		$insta_feed = json_decode($json, true, 512, JSON_BIGINT_AS_STRING);
		//print_r($insta_feed);exit;
		$data  = array(
						"insta_data"=>$insta_feed,
						"category" 	  => $category,
						"products"	  => $products,
						"stored_cookie" => $stored_cookie,
						"testimonial" =>$testimonial,
						"cart" 			=> $cart_data['cart'],
						"product_fly"	=> $cart_data['product_fly'],
						"cart_total"	=> $cart_data['cart_total'],
						"trending" 	  => $featured,
						"meta_title"	=>$meta_title,
						"meta_keyword"	=>$meta_keyword,
						"meta_desc"		=> $meta_desc,
						"meta_h1"		=> $meta_h1,

						);
		$this->load->view('home',$data);
	}

	public function newletterSub(){
		$this->load->model('email_sending');
		$this->load->model('my_model');
		
		if(isset($_POST))
		 {
		 	$this->form_validation->set_rules('subscribe_email','Email ID','required|xss_clean');
		 	if($this->form_validation->run())
				{
					
					$newsletter_email=$this->input->post('subscribe_email',true);
					$email_id = $newsletter_email;
					$check = $this->master_model->getRecords('table_subscription', array('email_id'=>$newsletter_email), 'id', '' );
					if(count($check)>0){
						echo "duplicate";
					}else
					{
						
					$info_arr=array('from'=>'noreply@rasoitatva.com','to'=>$email_id,'subject'=>'Subscribed for rasoitatva','view'=>'subscribe');
					$other_info = array('email_id'=>$newsletter_email);
					$status = $this->email_sending->sendmail($info_arr,$other_info);

					//print_r($status);exit;
					$admin_id = 'rasoitatva@gmail.com';
					$info_arr_admin=array('from'=>'noreply@rasoitatva.com','to'=>$admin_id,'subject'=>'New User Subscribed for rasoitatva','view'=>'subscribe-admin');
					$other_info_admin = array('email_id'=>$newsletter_email);
					$status_admin = $this->email_sending->sendmail($info_arr_admin,$other_info_admin);
						$resp = $this->my_model->subsciption(array('email_id'=>$newsletter_email,"status"=>1));
						if($status){
							echo "success";
						}else{ 
							echo "fail";
						}	
					}
					
				}
		 }
	}

	

	
}
