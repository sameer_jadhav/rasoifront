 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'controllers/Restcall.php');
session_start();

class Logout extends CI_Controller {
	public function __construct()	
    {
        parent::__construct();

       	
		$this->load->library('curl');
        $this->load->helper('cookie');
		$this->load->library('session');
		$this->load->library('facebook');

    }

    public function index(){
        session_destroy();
        unset($_SESSION['access_token']);
    	$this->facebook->destroy_session();
        $this->session->unset_userdata('user');
        delete_cookie('wishlist_cookie');
        redirect(base_url());
    }
}
 