<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'controllers/Restcall.php');
require_once(APPPATH.'controllers/cart.php');
class Shop extends CI_Controller {
	 //protected $nonce_chars;
	public function __construct()	
    {
        parent::__construct();
        /*
        $check_auth_client = $this->MyModel->check_auth_client();
		if($check_auth_client != true){
			die($this->output->get_output());
		}
		*/
		
		$this->load->library('curl');
		$this->load->helper('menu_helper');
	}

	public function index()
	{
		
		$obj_rest = new Restcall();
		$this->load->helper('cookie');			
		$stored_cookie = array();
		if (get_cookie('wishlist_cookie')) {		
			$stored_cookie  = get_cookie('wishlist_cookie');			
			$stored_cookie = explode(',', $stored_cookie);
		}
		
		$options_cat =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/products/categories",
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 => array()
						 );
		$category = $obj_rest->_execute($options_cat);
		unset($category[0]);
		$category = array_values($category);
		$home_sql = "select wm.meta_key,wm.meta_value from wp_posts as wp join wp_postmeta as wm ON wm.post_id=wp.ID where wp.post_status='publish' and wp.post_type='page' and wp.post_name= 'shop' AND wm.meta_key IN ('seo_description','seo_title','seo_keyword','seo_h1')";
		$home_query = $this->db->query($home_sql);
        $seo_content = $home_query->result_array();
        
        foreach ($seo_content as $seocontent) {
        	
        	switch ($seocontent['meta_key']) {
        		case 'seo_description':
        			$meta_desc = $seocontent['meta_value'];
        			break;
        		case 'seo_h1':
        			$meta_h1 = $seocontent['meta_value'];
        			break;
        		case 'seo_keyword':
        			$meta_keyword = $seocontent['meta_value'];
        			break;
        		case 'seo_title':
        			$meta_title = $seocontent['meta_value'];
        			break;
        		
        		default:
        			# code...
        			break;
        	}
        }

		$options_product =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/products?status=publish&per_page=40&orderby=menu_order&order=asc",
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 => array("status"=>"publish","per_page"=>40,"orderby"=>"menu_order","order"=>"asc")
						 );
		
		$products = $obj_rest->_execute($options_product);
		//print_r($products);exit;

		$options_feat =  array(
						"url" 	 => $this->config->item('api_url')."wp-json/wc/v3/products?featured=true",
						"method" =>"GET",
						"auth"	 =>	"true",
						"param"	 => array("featured"=>"true")
						 );
		
		$featured = $obj_rest->_execute($options_feat);
		//print_r($featured);exit;
		///get cart flyer
		$obj_cart = new cart();	
		$cart_data = $obj_cart->getCartfly();
		//print_r($cart_data);exit;
		

		$data  = array(

						
						"products" 		=> $products,
						"featured"		=> $featured,
						"stored_cookie" => $stored_cookie,
						"category"		=> $category,
						"cart" 			=> $cart_data['cart'],
						"product_fly"	=> $cart_data['product_fly'],
						"cart_total"	=> $cart_data['cart_total'],
						"meta_title"	=> $meta_title,
						"meta_keyword"	=> $meta_keyword,
						"meta_desc"		=> $meta_desc,
						"meta_h1"		=> $meta_h1,
						);
		$this->load->view('category',$data);
	}

}