<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model {
	private $_table = 'admin';
	function __construct() {
        parent::__construct();
	}
	public function login($data, $from) {
		$where = array('email'=>$data['email'], 'password' => $data['password']   );		
		$query = $this->db->where($where)->get($this->_table)->row_array();				
		if ($query) {
				unset($query['password']);
				return $query;		
		}else{
			return false;
		}
	}
}
