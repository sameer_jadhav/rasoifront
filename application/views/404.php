
<!DOCTYPE html>
<html>

<?php $this->load->view('include/head.php'); ?>

<body>
    <div>
        
        <?php $this->load->view('include/header.php'); ?>
        
        <main>
            <div class="bs-main">
                <div class="bs-message">
                    <div class="msg-wrap">
                        <div class="ty-head">
                            <span class="icon icon-404"></span>
                            <h2 class="ty-title">oops! 404</h2>
                            <p class="ty-desc">Something went wrong</p>
                        </div>
                        <div class="ty-cont">
                            <p>We could’nt find the page you are <span class="cm-line-break">looking for.</span></p>
                            <a href="<?php echo base_url();?>" type="button" class="btn btn-default">back to home</a>
                        </div>
                    </div>
                </div>
            </div>
        </main>

    </div>

    <!-- js group start -->
    <!--#include virtual="views/common/include_js.html" -->
    <!-- js group end -->

</body>

</html>