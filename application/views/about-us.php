
<!DOCTYPE html>
<html>

<?php $this->load->view('include/head.php'); ?>

<body class="pg-banner">
    
    <?php $this->load->view('include/header.php'); ?>

    <main>
        <div class="bs-main">
            <!-- banner start -->
            <div class="bs-banner typ-sm">
                <div class="banner-media js-addto-img">
                    <img src="assets/images/banner/about-banner1.jpg" data-mbsrc="assets/images/banner/about-banner1-mb.jpg" alt="banner" class="addto-img">
                </div>
                <div class="banner-cont typ-center">
                    <h2 class="title">
                        <span class="cm-line-break cm-cursive">Welcome to</span>
                        <span class="cm-line-break">Rasoi Tatva Spices</span>
                    </h2>
                </div>
            </div>
            <!-- banner end -->
            <section>
                <div class="bs-sec sec-first-child">
                    <div class="container">
                        <div class="sec-cont">
                            <div class="bs-info cm-text-center typ-sm wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.3s">
                                <h2 class="title">
                                    <span class="cm-cursive cm-line-break">Story behind</span>
                                    <span class="cm-line-break">Our Tradition</span>
                                </h2>
                                <div class="desc">
                                    <p>TATVA – principle, truth, reality, the element. Our bodies are made up of PANCHATATVA – the 5 essential elements. To nourish this body, we need food. And food is made delicious and healthy with spices. Spices then, make the sixth essential element. The beginning of all delicious meals, conversations and memories….This sixth element is found in our kitchens, the Rasoi – in the Indian context. And therefore, we named this sixth element Rasoi Tatva.</p>
                                    <p>What began as a small business a century ago in 1905, has now grown into a name of trust. Having catered to global clients for over 40 years, we have learnt a lot from experience and observation. We provide ‘just out of farm’ freshness and flavour to your meals along with all the nutritional benefits that pure spices provide. Today, you can buy premium Indian spices online from us, from the comfort of your homes. Rasoi Tatva – best Indian spices, from us to you.</p>
                                </div>
                                <!-- <button type="button" class="btn btn-link" data-toggle="modal" data-target="#traditionModal">read more</button> -->
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="bs-sec">
                    <div class="container">
                        <div class="sec-cont">
                            <div class="bs-img-info wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.3s">
                                <div class="img-wrap">
                                    <img src="assets/images/dummy-quality.jpg" alt="buy Indian spices">
                                </div>
                                <div class="info-wrap">
                                    <div class="bs-info">
                                        <h3 class="title">
                                            <span class="cm-line-break">Quality Process</span>
                                        </h3>
                                        <div class="desc">
                                            <p>Spices deserve respect. They need to be treated just right. Our sophisticated machines and methodology ensure this is taken care of. All processes are in house – which gives us control over the quality from the time the spices reach our facility. We harness low temperature grinding techniques to retain precious micro oils. This ensures that our premium indian spices remain flavourful and aromatic without having to use any synthetic fillers, colours, flavours or preservatives. A short and efficient supply chain allows the spice to reach your home in a short span of time. Also, a multi-level quality check for every process guarantees that every gram of spice that reaches you is of the finest quality.</p>
                                        </div>
                                        <strong class="meta-info">Rasoi Tatva – best Indian spices, for the best taste and flavour.</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="bs-video-block wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.5s">
                <div class="video-info typ-about">
                    <div class="img-wrap js-addto-img">
                        <img src="assets/images/about-video.jpg" data-mbsrc="assets/images/about-video-mg.jpg" alt="banner" class="addto-img">
                    </div>
                    <div class="info-wrap cm-hidden">
                        <h3 class="title"><span class="cm-line-break cm-cursive">Our finest</span>Process</h3>
                        <button class="play-btn js-btn-video" data-src="https://www.youtube.com/embed/bLw4NbJa_Rs" data-toggle="modal" data-target="#videoModal"></button>
                        <ul class="meta">
                            <li class="item">Drying & Harvesting spices</li>
                            <li class="item">Grinding & Roasting</li>
                            <li class="item">Grading & Seiving</li>
                        </ul>
                    </div>
                </div>
            </div>
            <section>
                <div class="bs-sec sec-last-child">
                    <div class="container">
                        <div class="sec-cont">
                            <div class="bs-img-info typ-right wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.3s">
                                <div class="img-wrap">
                                    <img src="assets/images/dummy-responsibility.jpg" alt="Natural Indian Spices">
                                </div>
                                <div class="info-wrap">
                                    <div class="bs-info">
                                        <h3 class="title">
                                            <span class="cm-line-break">Our Social</span>
                                            <span class="cm-line-break">Responsibility</span>
                                        </h3>
                                        <div class="desc">
                                            <p>We respect the environment and are mindful about keeping it as less distressed as possible. Our grinding machines are acoustically designed to reduce noise pollution. Japanese technology adopted for treating sewage makes waste management comfortable and safe. Water is recycled as necessary to cut down wastage. Dust arising from the various processes within the facility is disposed of systematically and not released carelessly in the open.</p>
                                            <p>In order to keep up with modernization and technology, we constantly update our competencies and skills with various certifications, automations and documentation as and when necessary.</p>
                                            <p>This is one of the prime reasons that we have been a preferred partner to homemakers through our semi centennial journey!</p>
                                            <p>Rasoi Tatva – best Indian spices, manufactured with respect.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
    <?php $this->load->view('include/footer.php'); ?>
    <?php $this->load->view('include/footer_2.php'); ?>
    <?php $this->load->view('include/footer_js.php'); ?>
    </body>

</html>