    
<section class="content-header">
    <h1>
        Faq | 
            <button type="button"  class="btn btn-primary  m-b-20" onclick="add()">Add</button>
            <button type="button" class="btn btn-warning  m-b-20" onclick="reload_table()"> Refresh</button>       

    </h1>
</section>
<section class="content">
    <div id="table_div"  >
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <div class="box">                 
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                               <tr class="table-primary">
                                <th style="width: 10px">Id</th>
                                <th style="width: 20px">Question</th>
                                <th style="width: 20px">Answer</th>
                                <th style="width: 20px">Category</th>
                                <th style="width: 20px">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div><!-- tablediv end -->
</section>
     
        <script type="text/javascript">
            var save_method; //for save method string
            var table;
            $(document).ready(function () {
                //datatables
                table = $('#table').DataTable({
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [], //Initial no order.
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo admin_url('faq/data_list') ?>",
                        "type": "POST"
                    },
                    //Set column definition initialisation properties.
                    "columnDefs": [
                        {
                            "targets": [2, 3], //last column
                            "orderable": false, //set not orderable
                        },
                    ],
                });
            
              
            });
            function add()
            {
                save_method = 'add';
                $('#form')[0].reset(); // reset form on modals
                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                    CKEDITOR.instances[instance].setData('');
                }
                $('.form-group').removeClass('has-error'); // clear error class
                $('.help-block').empty(); // clear error string
                $('#modal_form').modal('show'); // show bootstrap modal
                $('.modal-title').text('Add'); // Set Title to Bootstrap modal title
                $('.image_label').text('Upload image'); // Set Title to Bootstrap modal title
                $('.video').hide();
                $('.image').hide();
            }
            function edit(id)
            {
                save_method = 'update';
                $('#form')[0].reset(); // reset form on modals
                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                    CKEDITOR.instances[instance].setData('');
                }
                $('.form-group').removeClass('has-error'); // clear error class
                $('.help-block').empty(); // clear error string
                $('.image_label').text('Change image'); // Set Title to Bootstrap modal title
                $('.video').hide();
                $('.image').hide();
                //Ajax Load data from ajax
                $.ajax({
                    url: "<?php echo admin_url('faq/edit/') ?>/" + id,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data)
                    {
                        $('[name="id"]').val(data.id);
                        $('[name="question"]').val(data.question);                                            
                        $('[name="category"]').val(data.category);                                            
                        $('[name="answer"]').val(data.answer);                                            
                        CKEDITOR.instances['answer'].setData(data.answer);

                        $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                        $('.modal-title').text('Edit'); // Set title to Bootstrap modal title
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error get data from ajax');
                    }
                });
            }
            function reload_table()
            {
                table.ajax.reload(null, false); //reload datatable ajax 
            }
            function save()
            {
                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                    // CKEDITOR.instances[instance].setData('');
                }
                $('#btnSave').text('saving...'); //change button text
                $('#btnSave').attr('disabled', true); //set button disable 
                var url;
                if (save_method == 'add') {
                    url = "<?php echo admin_url('faq/add') ?>";
                } else {
                    url = "<?php echo admin_url('faq/update') ?>";
                }
                // ajax adding data to database
                var formData = new FormData($('#form')[0]);
                $.ajax({
                    url: url,
                    type: "POST",
                    data: formData,
                    contentType: false,
                    processData: false,
                    dataType: "JSON",
                    success: function (data)
                    {
                        if (data.status) //if success close modal and reload ajax table
                        {
                            $('#modal_form').modal('hide');
                            reload_table();
                        } else
                        {
                            for (var i = 0; i < data.inputerror.length; i++)
                            {
                                $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                                $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
                            }
                        }
                        $('#btnSave').text('save'); //change button text
                        $('#btnSave').attr('disabled', false); //set button enable 
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error adding / update data');
                        $('#btnSave').text('save'); //change button text
                        $('#btnSave').attr('disabled', false); //set button enable 
                    }
                });
            }
            function delete_value(id)
            {
                if (confirm('Are you sure delete this data?'))
                {
                    // ajax delete data to database
                    $.ajax({
                        url: "<?php echo admin_url('faq/delete') ?>/" + id,
                        type: "POST",
                        dataType: "JSON",
                        success: function (data)
                        {
                            //if success reload ajax table
                            $('#modal_form').modal('hide');
                            reload_table();
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error deleting data');
                        }
                    });
                }
            }


function off(id, column) {
        //alert(srno);
        $.ajax({
            url: "<?php echo admin_url('faq/off') ?>/" + id +'/'+column,
            type: "POST",          
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function (data)
            {

                if (data.status) //if success close modal and reload ajax table
                {
                    reload_table();

                } else
                {
                    alert('Error adding / update data');

                }
            }
        });
    }


    function on(id, column) {
        // alert(srno);       
        $.ajax({
            url: "<?php echo admin_url('faq/on') ?>/" + id +'/'+column,
            type: "POST",            
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function (data)
            {
                if (data.status) //if success close modal and reload ajax table
                {
                    reload_table();

                } else
                {
                    alert('Error adding / update data');
                }

            }
        });
    }
</script>

<!-- Bootstrap modal -->
<div class="modal fade " id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal form-signin">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Category</label>
                            <div class="col-md-9">
                                <select class="form-control" name="category">   
                                <option value="">Select</option>
                                <option value="product">Product</option>
                                <option value="payment">Payment</option>
                                <option value="shipping">Shipping</option>
                                <option value="others">Others</option>                                                            
                                </select>                                
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Question</label>
                            <div class="col-md-9">
                                <input name="question"  class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Answer</label>
                            <div class="col-md-9">
                                <textarea name="answer"  class="form-control" ></textarea>
                                <input type="hidden" name="answer_error">
                                <span class="help-block"></span>
                            </div>
                        </div>                        
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>  
<script type="text/javascript">
CKEDITOR.replace('answer');
</script>