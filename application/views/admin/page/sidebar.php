
  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">      
          <li><a href="<?= admin_url('slider'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Banner Slider</span></a></li>
          <li><a href="<?= admin_url('master'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Master</span></a></li>
          <li><a href="<?= admin_url('testimonial'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Testimonial</span></a></li>          
          <li><a href="<?= admin_url('faq'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>FAQ</span></a></li>
          <li><a href="<?= admin_url('termsandcondition'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Terms and Condition</span></a></li>
          <li><a href="<?= admin_url('privacypolicy'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Privacy Policy</span></a></li>
          <li><a href="<?= admin_url('shippingpolicy'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Shipping Policy</span></a></li>
          <li><a href="<?= admin_url('cancelpolicy'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Cancellation Policy</span></a></li>
          <li><a href="<?= admin_url('disclaimer'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Disclaimer</span></a></li>
          <li><a href="<?= admin_url('payments'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Payments</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
