


<section class="content">
    <?php
    if ($this->session->flashdata('msg')) {
        # code...
        echo $this->session->flashdata('msg');
    }
    ?>
    <div class="row">
        <form id="add_form" method="post"  action="<?php echo admin_url('profile/password_save'); ?>" class="form-horizontal">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Password Change</h3>
                    </div>
                    <br>
                    <div class="form-body  row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <label class="control-label col-md-6">Old Password</label>
                                <div class="typediv col-md-6">
                                    <input type="password" name="password" id="password" value="" class="form-control " required>
                                    <input type="hidden" name="id" value="<?php echo $result->id ?>" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-6">New Password</label>
                                <div class="typediv col-md-6">
                                    <input type="password" name="new_password" id="new_password" value="" class="form-control " required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-6">Confirm Password</label>
                                <div class="typediv col-md-6">
                                    <input type="password" name="confirm_password" id="confirm_password"  value="" class="form-control " required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    </form>
                </div>
            </div>
    </div>
</section>






<script type="text/javascript">

   var password = document.getElementById("new_password")
  , confirm_password = document.getElementById("confirm_password");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;

</script>