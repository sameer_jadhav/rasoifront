
<section class="content-header">
    <h1>
        Slider | 
            <button type="button"  class="btn btn-primary  m-b-20" onclick="add()">Add</button>
            <button type="button" class="btn btn-warning  m-b-20" onclick="reload_table()"> Refresh</button>       

    </h1>
</section>
<section class="content">
    <div id="table_div"  >
        <div class="row">
            <div class="col-xs-6">
                <!-- /.box -->
                <div class="box">                 
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                               <tr class="table-primary">
                                <th style="width: 10px">Id</th>
                                <th style="width: 20px">Title</th>
                                <th style="width: 20px">Order</th>
                                <th style="width: 20px">Content</th>
                                <th style="width: 20px">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div><!-- tablediv end -->
</section>
     
        <script type="text/javascript">
            var save_method; //for save method string
            var table;
            $(document).ready(function () {
                //datatables
                table = $('#table').DataTable({
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [], //Initial no order.
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo admin_url('slider/data_list') ?>",
                        "type": "POST"
                    },
                    //Set column definition initialisation properties.
                    "columnDefs": [
                        {
                            "targets": [3], //last column
                            "orderable": false, //set not orderable
                        },
                    ],
                });
            
              
            });
            function add()
            {
                save_method = 'add';
                $('#form')[0].reset(); // reset form on modals
                $('.form-group').removeClass('has-error'); // clear error class
                $('.help-block').empty(); // clear error string
                $('#modal_form').modal('show'); // show bootstrap modal
                $('.modal-title').text('Add'); // Set Title to Bootstrap modal title
                $('.image_label').text('Upload image'); // Set Title to Bootstrap modal title
                $('.image_label_mobile').text('Upload mobile image');
                $('.video').hide();
                $('.image').hide();
            }
            function edit(id)
            {
                save_method = 'update';
                $('#form')[0].reset(); // reset form on modals
                $('.form-group').removeClass('has-error'); // clear error class
                $('.help-block').empty(); // clear error string
                $('.image_label').text('Change image'); // Set Title to Bootstrap modal title
                $('.image_label_mobile').text('Change mobile image');
                $('.video').hide();
                $('.image').hide();
                //Ajax Load data from ajax
                $.ajax({
                    url: "<?php echo admin_url('slider/edit/') ?>/" + id,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data)
                    {
                        $('[name="id"]').val(data.id);
                        $('[name="sequence"]').val(data.sequence);
                        $('[name="title"]').val(data.title);
                        //$('[name="video"]').val(data.video);
                        $('[name="content_type"]').val(data.content_type);
                        if (data.content_type==1) {
                            //$('.image').show();
                            $('.video').hide();
                            $('[name="video"]').val('');
                            $('[name="video_mobile"]').val('');
                       }else if (data.content_type==2) {
                            $('[name="video"]').val(data.video);
                            $('[name="video_mobile"]').val(data.video_mobile);
                            $('.video').show();
                            //$('.image').hide();                            
                       }else{
                            $('.video').hide();
                            //$('.image').hide();
                       }
                        $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                        $('.modal-title').text('Edit'); // Set title to Bootstrap modal title
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error get data from ajax');
                    }
                });
            }
            function reload_table()
            {
                table.ajax.reload(null, false); //reload datatable ajax 
            }
            function save()
            {
                $('#btnSave').text('saving...'); //change button text
                $('#btnSave').attr('disabled', true); //set button disable 
                var url;
                if (save_method == 'add') {
                    url = "<?php echo admin_url('slider/add') ?>";
                } else {
                    url = "<?php echo admin_url('slider/update') ?>";
                }
                // ajax adding data to database
                var formData = new FormData($('#form')[0]);
                $.ajax({
                    url: url,
                    type: "POST",
                    data: formData,
                    contentType: false,
                    processData: false,
                    dataType: "JSON",
                    success: function (data)
                    {
                        if (data.status) //if success close modal and reload ajax table
                        {
                            $('#modal_form').modal('hide');
                            reload_table();
                        } else
                        {
                            for (var i = 0; i < data.inputerror.length; i++)
                            {
                                $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                                $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
                            }
                        }
                        $('#btnSave').text('save'); //change button text
                        $('#btnSave').attr('disabled', false); //set button enable 
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error adding / update data');
                        $('#btnSave').text('save'); //change button text
                        $('#btnSave').attr('disabled', false); //set button enable 
                    }
                });
            }
            function delete_value(id)
            {
                if (confirm('Are you sure delete this data?'))
                {
                    // ajax delete data to database
                    $.ajax({
                        url: "<?php echo admin_url('slider/delete') ?>/" + id,
                        type: "POST",
                        dataType: "JSON",
                        success: function (data)
                        {
                            //if success reload ajax table
                            $('#modal_form').modal('hide');
                            reload_table();
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error deleting data');
                        }
                    });
                }
            }



</script>

<!-- Bootstrap modal -->
<div class="modal fade " id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal form-signin">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Title</label>
                            <div class="col-md-9">
                                <input name="title"  class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Sequence</label>
                            <div class="col-md-9">
                                <input name="sequence"  class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Content Type</label>
                            <div class="col-md-9">
                                <select class="form-control" name="content_type">
                                    <option value="">Select</option>
                                    <option value="1">Image</option>
                                    <option value="2">Video</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="control-label col-md-3 image_label">File</label>
                            <div class="col-md-9">
                                <input name="image"  class="form-control" type="file">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label class="control-label col-md-3 image_label_mobile">Mobile File</label>
                            <div class="col-md-9">
                                <input name="mobile_image"  class="form-control" type="file">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group video"  style="display: none;">
                            <label class="control-label col-md-3">Video url:</label>
                            <div class="col-md-9">
                                <textarea name="video" class="form-control"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group video"  style="display: none;">
                            <label class="control-label col-md-3">Video Mobile url:</label>
                            <div class="col-md-9">
                                <textarea name="video_mobile" class="form-control"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>  

<script type="text/javascript">
    $('select[name="content_type"]').on('change', function (argument) {
       var id = $(this).val();
       if (id==1) {
            
            $('.video').hide();
       }else if (id==2) {
            $('.video').show();
            
       }else{
            $('.video').hide();            
       }
    });



</script>