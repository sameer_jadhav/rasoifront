<div class="cart-head">
        <h2 class="title">In Cart</h2>
        <span class="count"  id="cart_fly_count">(<?php echo count($cart);?>)</span>
        <button class="close-btn" id="close_cart_fly"><span class="icon icon-cross"></span></button>
    </div>
    <div class="cart-body">
        <?php if(count($cart)>0){?>
        <ul class="cart-list-fly">
        <?php 
            $p = 0;
            foreach($cart as $cart){
            ?>
            <li class="cart-item" id="cart_item_fly_<?php echo $cart['key'];?>">
                <div class="mod-cart-pd">
                    <div class="pd-img">
                        <img src="<?php echo $product_fly[$p]['images'][0]['src'];?>" alt="<?php echo $product_fly[$p]['name'];?>">
                    </div>
                    <div class="pd-detail">
                        <div class="title-wrap">
                            <h3 class="pd-title"><?php echo $product_fly[$p]['name'];?></h3>
                            <div class="bs-amount">
                            <?php  $product_price = $cart['line_total']/$cart['quantity']; ?>
                                <strong class="amount-val">&#8377; <?php echo number_format($product_price,2);?></strong>
                            </div>
                        </div>
                       
                        <div class="act-wrap">
                            <div class="bs-custom-select typ-box">
                                <div class="bs-amount">
                                <?php foreach($product_fly[$p]['attributes'][0]['options'] as $key=>$value) { 
                                    if($product_fly[$p]['variations'][$key]==$cart['variation_id']){
                                    ?>
                                    <strong class="amount-qty"><?php echo $value;?> gm</strong>
                                <?php } 
                                    } ?>
                                </div>
                            </div> 
                            
                            <div class="bs-val-variation">
                                <div class="input-group">
                                    <button class="decre-btn input-btn js-minus" onclick="update_to_cart(this)" data-weight="<?php echo $cart['weight'];?>" data-product-id="<?php echo $product_fly[$p]['id'];?>" data-var-id="<?php echo $cart['variation_id'];?>"><span class="icon icon-minus-c"></span></button>
                                    <input type="text" id="count" class="form-control count js-count" value="<?php echo $cart['quantity'];?>" min="1" max="5" readonly>
                                    <button class="incre-btn input-btn js-plus" onclick="update_to_cart(this)" data-weight="<?php echo $cart['weight'];?>" data-product-id="<?php echo $product_fly[$p]['id'];?>" data-var-id="<?php echo $cart['variation_id'];?>" data-max="5"><span class="icon icon-plus-c"></span></button>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-link del-btn js-product-remove" onclick="remove_cart_item('<?php echo $cart['key'];?>')" id="<?php echo $cart['key'];?>"><span class="icon icon-trash"></span></button>
                    </div>
                </div>
            </li>
            <?php 
                $p++;
            } ?>
        </ul>
        <?php }else{ ?>
            
            <div class="bs-message">
                <div class="msg-wrap">
                    <div class="ty-head">
                        <span class="icon icon-bag"></span>
                    </div>
                    <div class="ty-cont">
                        <p>No Products in shop</p>
                        <a href="<?php echo base_url();?>shop" type="button" class="btn btn-default">return to shop</a>
                    </div>
                </div>
            </div>
            <?php } ?>
       
    </div>
    <?php if(count($cart)>0){?>
    <div class="cart-foot">
        <div class="mod-data-list">
            <dl>
                <dt class="data-label">Cart subtotal</dt>
                <dd class="data-value"><span class="amount-val" >&#8377; <span id="cart_fly_subtotal"><?php echo $cart_total['subtotal'];?></span></span></dd>
            </dl>
        </div>
        <a href="<?php echo base_url();?>cart" class="btn btn-default">Checkout</a>
    </div>
    <?php } ?>
