<?php 
                                        $count = 0;
                                        foreach($cart as $cart){
                                        
                                        ?>
                                            <li class="cart-item" id="cart_item_<?php echo $cart['key'];?>">
                                                <div class="mod-cart-pd">
                                                    <div class="pd-img">
                                                        <img src="<?php echo $product[$count]['images'][0]['src'];?>" alt="<?php echo $product[$count]['name'];?>">
                                                    </div>
                                                    <div class="pd-detail">
                                                        <div class="title-wrap">
                                                            <h3 class="pd-title"><?php echo $product[$count]['name'];?></h3>
                                                            <div class="bs-amount">
                                                                <?php  $product_price = $cart['line_total']/$cart['quantity']; ?>
                                                                <strong class="amount-val">&#8377;<?php echo number_format($product_price,2);?></strong>
                                                            </div>
                                                            <?php 
                                                             $weight = "";
                                                            foreach($product[$count]['variations'] as $key=>$value){
                                                                   
                                                                if($value==$cart['variation_id']){
                                                                    $weight = $product[$count]['attributes'][0]['options'][$key];
                                                                }
                                                            }
                                                            ?>
                                                            <span class="net-quantity"><?php echo $weight;?> gm</span>
                                                        </div>
                                                        <div class="act-wrap">
                                                            <div class="bs-val-variation">
                                                                <div class="input-group">
                                                                    <button onclick="update_to_cart(this)" data-product-id="<?php echo $product[$count]['id'];?>" data-weight="<?php echo $cart['weight'];?>" data-var-id="<?php echo $cart['variation_id'];?>"  class="decre-btn input-btn js-minus" ><span class="icon icon-minus-c"></span></button>
                                                                    <input type="text" class="form-control count js-count"  id="count" min="1" max="5" value="<?php echo $cart['quantity'];?>" readonly>
                                                                    <button class="incre-btn input-btn js-trigger-toaster js-plus" onclick="update_to_cart(this)" data-product-id="<?php echo $product[$count]['id'];?>" data-var-id="<?php echo $cart['variation_id'];?>" data-weight="<?php echo $cart['weight'];?>" data-max="5"><span class="icon icon-plus-c" ></span></button>
                                                                </div>
                                                            </div>

                                                            
                                                        </div>
                                                        
                                                        <!--button class="btn btn-link del-btn" onclick="remove_cart_item_modal('<?php //echo $cart['key'];?>')" id="<?php //echo $cart['key'];?>">Remove</button-->
                                                         <button class="btn btn-link del-btn js-product-remove" onclick="remove_cart_item_modal('<?php echo $cart['key'];?>')" id="<?php echo $cart['key'];?>"><span class="icon icon-trash"></span></button>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php 
                                                $count++;
                                            } ?>