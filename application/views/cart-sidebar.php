<div class="order-wrap">
                                        <div class="order-head">
                                            <h3 class="title">Order Summary</h3>
                                        </div>
                                        <div class="order-cont">
                                            <div class="mod-data-list">
                                                <dl>
                                                    <dt class="data-label">Sub Total</dt>
                                                    <dd class="data-value">&#8377;<span id="cart_subtotal"> <?php echo $cart_total['cart_subtotal'];?></span></dd>
                                                </dl>
                                                <dl>
                                                    <dt class="data-label">Delivery Charges</dt>
                                                    <dd class="data-value">&#8377;<span id="cart_shipping"><?php echo $cart_total['shipping_total'];?></span></dd>
                                                </dl>
                                                <dl>
                                                    <dt class="data-label">Tax (5% GST)</dt>
                                                    <dd class="data-value">&#8377;<span id="cart_taxes"><?php echo $cart_total['cart_tax'];?></span></dd>
                                                </dl>
                                                
                                            </div>
                                        </div>
                                        <div class="order-foot">
                                            <div class="mod-data-list">
                                                <dl>
                                                    <dt class="data-label">Total Amount</dt>
                                                    <dd class="data-value"><span class="amount-val">&#8377; <span id="cart_total"><?php echo $cart_total['total'];?></span></span></dd>
                                                </dl>
                                            </div>
                                            <!-- <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Apply Coupon Code">
                                                <button class="btn btn-link input-btn">Apply</button>
                                            </div> -->
                                            <div class="input-group delivery-input">
                                                <input type="text" id="shipping_pincode" value="" class="form-control" placeholder="Delivery Pincode">
                                                <button class="btn btn-link input-btn" onclick="getShipping()">Check</button>
                                                 <span class="error-msg show" id="delivery_error"></span>
                                            </div>
                                           
                                            <btn onclick="move_to_shipping()"  class="btn btn-default hidden-xs continue_shipping">Proceed</btn>
                                            <div class="visible-xs cm-fly-action-wrap js-targetEle">
                                                <btn onclick="move_to_shipping()" class="btn btn-default continue_shipping">Proceed</btn>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" id="pincode_status" value="invalid"/>