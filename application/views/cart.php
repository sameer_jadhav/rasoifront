
<!DOCTYPE html>
<html>

<?php $this->load->view('include/head.php'); ?>

<body class="pg-banner js-pg-cart">
    <?php $this->load->view('include/header.php'); ?>

    <main>
        <div class="bs-main">
            <div class="container">
                <ul class="mod-breadcrumbs">
                    <!--li><a href="<?php echo base_url();?>">Home</a></li>
                    <li><a href="#" class="active">Cart</a></li-->
                </ul>
                <section>
                    <div class="bs-progress-tab js-element">
                        <ul class="progress-list">
                            <li class="progress-item">
                                <a href="<?php echo base_url();?>cart" class="progress-link active"><span class="icon icon-cart"></span>In Cart <span class="count" id="cart_count">(<?php echo count($cart);?>)</span></a>
                            </li>
                            <li class="progress-item">
                                <a href="#" class="progress-link"><span class="icon icon-track"></span>Shipping Details</a>
                            </li>
                            <li class="progress-item">
                                <a href="#" class="progress-link"><span class="icon icon-pay"></span>Payment</a>
                            </li>
                        </ul>
                        <div class="progress-cont show">
                            <div class="row">
                            <?php 
                            $cart_class = "";
                            $cart_empty_class = "";
                            if(empty($cart)){
                                $cart_class = "hide";
                                $cart_empty_class = "show";
                            }else{
                                $cart_class = "show";
                                $cart_empty_class = "hide";
                            }
                            ?>
                            <?php //if(empty($cart)){?>
                            <div class="bs-message typ-static cart_empty <?php echo $cart_empty_class;?>">
                                <div class="msg-wrap">
                                    <div class="ty-head">
                                        <span class="icon icon-bag"></span>
                                    </div>
                                    <div class="ty-cont">
                                        <p>Empty Cart, No Product</p>
                                        <a href="<?php echo base_url();?>shop" type="button" class="btn btn-default">Add Product</a>
                                    </div>
                                </div>
                            </div>
                            <?php //}else{ ?>
                                <div class="col-md-8 cart_fill <?php echo $cart_class;?>">
                                    <div class="cart-wrap">
                                        <ul class="cart-list">
                                        <?php 
                                        $count = 0;
                                        foreach($cart as $cart){
                                        
                                        ?>
                                            <li class="cart-item" id="cart_item_<?php echo $cart['key'];?>">
                                                <div class="mod-cart-pd">
                                                    <div class="pd-img">
                                                        <img src="<?php echo $product[$count]['images'][0]['src'];?>" alt="<?php echo $product[$count]['name'];?>">
                                                    </div>
                                                    <div class="pd-detail">
                                                        <div class="title-wrap">
                                                            <h3 class="pd-title"><?php echo $product[$count]['name'];?></h3>
                                                            <div class="bs-amount">
                                                                <?php  $product_price = $cart['line_total']/$cart['quantity']; ?>
                                                                <strong class="amount-val">&#8377;<?php echo number_format($product_price,2);?></strong>
                                                            </div>
                                                            <?php 
                                                             $weight = "";
                                                            foreach($product[$count]['variations'] as $key=>$value){
                                                                   
                                                                if($value==$cart['variation_id']){
                                                                    $weight = $product[$count]['attributes'][0]['options'][$key];
                                                                }
                                                            }
                                                            ?>
                                                            <span class="net-quantity"><?php echo $weight;?> gm</span>
                                                        </div>
                                                        <div class="act-wrap">
                                                            <div class="bs-val-variation">
                                                                <div class="input-group">
                                                                    <button onclick="update_to_cart(this)" data-product-id="<?php echo $product[$count]['id'];?>" data-var-id="<?php echo $cart['variation_id'];?>" 
                                                                    data-weight="<?php echo $cart['weight'];?>" class="decre-btn input-btn js-minus" ><span class="icon icon-minus-c"></span></button>
                                                                    <input type="text" class="form-control count js-count"  id="count" min="1" max="5" value="<?php echo $cart['quantity'];?>" readonly>
                                                                    <button class="incre-btn input-btn js-trigger-toaster js-plus" onclick="update_to_cart(this)" data-product-id="<?php echo $product[$count]['id'];?>" data-weight="<?php echo $cart['weight'];?>" data-var-id="<?php echo $cart['variation_id'];?>" data-max="5"><span class="icon icon-plus-c" ></span></button>
                                                                </div>
                                                            </div>

                                                            
                                                        </div>
                                                        
                                                        <button class="btn btn-link del-btn" onclick="remove_cart_item_modal('<?php echo $cart['key'];?>')" id="<?php echo $cart['key'];?>"><span class="icon icon-trash"></span></button>

                                                    </div>
                                                </div>
                                            </li>
                                        <?php 
                                                $count++;
                                            } ?>  
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-4 cart_fill cart_sidebar <?php echo $cart_class;?>">
                                    <div class="order-wrap">
                                        <div class="order-head">
                                            <h3 class="title">Order Summary</h3>
                                        </div>
                                        <div class="order-cont">
                                            <div class="mod-data-list">
                                                <dl>
                                                    <dt class="data-label">Sub Total</dt>
                                                    <dd class="data-value">&#8377; <span id="cart_subtotal"> <?php echo $cart_total['cart_subtotal'];?></span></dd>
                                                </dl>
                                                <dl>
                                                    <dt class="data-label">Delivery Charges</dt>
                                                    <dd class="data-value">&#8377; <span id="cart_shipping"><?php echo $cart_total['shipping_total'];?></span></dd>
                                                </dl>
                                                <dl>
                                                    <dt class="data-label">Tax (5% GST)</dt>
                                                    <dd class="data-value">&#8377; <span id="cart_taxes"><?php echo $cart_total['cart_tax'];?></span></dd>
                                                </dl>
                                                
                                            </div>
                                        </div>
                                        <div class="order-foot">
                                            <div class="mod-data-list">
                                                <dl>
                                                    <dt class="data-label">Total Amount</dt>
                                                    <dd class="data-value"><span class="amount-val">&#8377; <span id="cart_total"><?php echo $cart_total['total'];?></span></span></dd>
                                                </dl>
                                            </div>
                                            <!-- <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Apply Coupon Code">
                                                <button class="btn btn-link input-btn">Apply</button>
                                            </div> -->
                                            <div class="input-group delivery-input">
                                                <input type="text" id="shipping_pincode" value="" class="form-control" placeholder="Delivery Pincode">
                                                <button class="btn btn-link input-btn" onclick="getShipping()">Check</button>
                                                 <span class="error-msg show" id="delivery_error"></span>
                                            </div>
                                           
                                            <btn onclick="move_to_shipping()"  class="btn btn-default hidden-xs continue_shipping">Proceed</btn>
                                            <div class="visible-xs cm-fly-action-wrap js-targetEle">
                                                <btn onclick="move_to_shipping()" class="btn btn-default continue_shipping">Proceed</btn>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" id="pincode_status" value="invalid"/>
                                </div>
                            </div>
                            <?php //} ?>
                        </div>
                    </div>
                </section>
                
                <section>
                    <div class="bs-sec bs-related-products sec-first-child">
                        <div class="sec-head">
                            <h2 class="sec-title">Recommended Products</h2>
                        </div>
                        <div class="sec-cont" id="moreProductsSwiper">
                            <div class="lyt-product-listing swiper-container">
                                <ul class="list-wrap swiper-wrapper">
                                <?php  
                                                    $featured_count = count($featured);
                                                    for($m=0;$m<$featured_count;$m++){ ?>
                                    <li class="item swiper-slide">
                                        <div class="bs-product">
                                            <div class="pd-head">
                                                <figure class="pd-img-wrap">
                                                    <img src="<?php echo $featured[$m]['images'][0]['src'];?>" alt="<?php echo $featured[$m]['name'];?>">
                                                </figure>
                                                <div class="pd-desc">
                                                    <p>Garam masala is a blend of ground spices used extensively in Indian cuisine.</p>
                                                    <p>The spices for garam masala are usually toasted to bring out more flavor and aroma, and then ground</p>
                                                </div>
                                            </div>
                                            <div class="pd-cont">
                                                <h3 class="pd-title"><?php echo $featured[$m]['name'];?></h3>
                                                <div class="bs-amount">
                                                    <!-- <strong class="amount-val strike">Rs. 350</strong> -->
                                                    <strong class="amount-val">&#8377; <?php echo $featured[$m]['variations_price'][0];?></strong>
                                                    <strong class="amount-qty">/ <?php echo $featured[$m]['attributes'][0]['options'][0];?> gm</strong>
                                                </div>
                                                <div class="pd-action">
                                                    <button type="button" class="btn btn-icon btn-outline"  data-toggle="modal" onclick="product_summary(<?php echo $featured[$m]['id'];?>);"><span class="icon icon-view"></span></button>
                                                    <button type="button" onclick="add_to_cart(this)" data-product-id="<?php echo $featured[$m]['id'];?>" data-var-id="<?php echo $featured[$m]['variations'][0];?>" data-weight="<?php echo $featured[$m]['attributes'][0]['options'][0];?>" id="add_to_cart" class="btn btn-default">add to cart</button>
                                                </div>
                                            </div>
                                            <a href="<?php echo base_url();?>shop/<?php echo $featured[$m]['categories'][0]['slug'];?>/<?php echo $featured[$m]['slug'];?>" class="pd-link"></a>
                                            <button type="button" class="btn pd-btn-like js-like"></button>
                                            <?php if(!empty($featured[$m]['tags'])){ ?>
                                            <label class="pd-tag"><?php echo $featured[$m]['tags'][0]['name'];?></label>
                                            <?php } ?>
                                            <div class="pd-action">
                                                <div class="action-wrap">

                                                    <?php 
                                                            $like_class=''; 
                                                             if (in_array($featured[$m]['id'], $stored_cookie)) {
                                                                $like_class='active';
                                                            
                                                            ?>
                                                    <button type="button" class="btn btn-icon btn-outline" id="btnRemoveWishlist<?php echo $featured[$m]['id'];?>" onclick="remove_wishlist_modal('<?php echo $featured[$m]['id'];?>')"><span class="icon icon-like <?php echo $like_class; ?>" ></span></button>
                                                     <?php }else{ ?>
                                                        <button type="button" class="btn btn-icon btn-outline" id="btnWishlist<?php echo $featured[$m]['id'];?>" onclick="add_wishlist('<?php echo $featured[$m]['id'];?>')" ><span class="icon icon-like <?php echo $like_class; ?>" ></span></button>
                                                    <?php } ?>

                                                    <button type="button" class="btn btn-icon btn-outline" data-toggle="modal" onclick="product_summary(<?php echo $featured[$m]['id'];?>);"><span class="icon icon-view"></span></button>
                                                    <button type="button" class="btn btn-icon btn-default"  onclick="add_to_cart(this)" data-weight="<?php echo $featured[$m]['attributes'][0]['options'][0];?>" data-product-id="<?php echo $featured[$m]['id'];?>" data-var-id="<?php echo $featured[$m]['variations'][0];?>" id="add_to_cart"><span class="icon icon-cart"></span></button>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                <?php } ?>
                                    
                                </ul>
                            </div>
                            <div class="swiper-pagination"></div>
                            <div class="swiper-btn swiper-button-prev hidden-xs"><span class="icon icon-left-arrow-c"></span></div>
                            <div class="swiper-btn swiper-button-next hidden-xs"> <span class="icon icon-right-arrow-c"></span></div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </main>
    <?php $this->load->view('include/footer.php'); ?>
    
    
    
    <div class="bs-pd-fly cm-loader">
        <div class="head-wrap">
            <div class="filter-wrap">
                <span class="filter-label">Category</span>
                <div class="bs-custom-select typ-box">
                    <select id="pdSelect">
                        <option value="trending">Trending</option>
                        <?php foreach ($category as $key => $value): ?>
                        <option value="<?php echo $value['slug'];?>"><?php echo $value['name'];?></option>
                        <?php endforeach ?> 
                    </select>
                </div>
            </div>
        </div>
        <div class="cont-wrap">
            <div class="pd-tab-content js-tab-content active" id="trending">
                <ul class="pd-list">
                <?php foreach ($featured as $product):
                   // print_r($product);exit;
                 ?>
                    <li class="pd-item">
                        <div class="mod-cart-pd">
                            <div class="pd-img">
                                <img src="<?php echo $product['images'][0]['src'];?>" alt="product">
                            </div>
                            <div class="pd-detail">
                                <div class="title-wrap">
                                    <h3 class="pd-title"><?php echo $product['name'];?></h3>
                                    <div class="bs-amount">
                                       <strong class="amount-val">&#8377; <span id="amt_val_trend<?php echo $product['id'];?>" ><?php echo $product['variations_price'][0];?></span></strong>
                                        <strong class="amount-qty">/ <span id="amt_qty_trend<?php echo $product['id'];?>" ><?php echo $product['attributes'][0]['options'][0];?> gm</span></strong>
                                    </div>
                                </div>
                                <div class="act-wrap">
                                    <div class="bs-custom-select typ-box">
                                        <select id="product_qty_dd_feat">
                                                <?php foreach($product['attributes'][0]['options'] as $opt_key=>$opt_value) {
                                                        $sel_wt = "";
                                                        if($opt_key==0){
                                                            $sel_wt = "selected='selected'";
                                                        } ?>
                                                <option value="<?php echo $product['variations_price'][$opt_key].";".$opt_value.";".$product['variations'][$opt_key].";".$product['id'];?>"       <?php echo $sel_wt;?>><?php echo $opt_value;?>gm</option>
                                                <?php } ?>
                                            </select>
                                    </div>
                                     <button type="button"  onclick="add_to_cart(this)" data-product-id="<?php echo $product['id'];?>" data-var-id="<?php echo $product['variations'][0];?>" data-weight="<?php echo $product['attributes'][0]['options'][0];?>" id="add_to_cart_trend<?php echo $product['id'];?>" class="btn btn-icon btn-default"><span class="icon icon-cart"></span></button>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php endforeach ?>
                </ul>
            </div>

            <?php foreach ($category as $key => $value): ?>

            <div class="pd-tab-content js-tab-content" id="<?php echo $value['slug'];?>">
                <ul class="pd-list">
                <?php foreach ($products as $product):
                    if(in_array($value['id'], array_column($product['categories'], 'id'))) { 
                 ?>
                    <li class="pd-item">
                        <div class="mod-cart-pd">
                            <div class="pd-img">
                                <img src="<?php echo $product['images'][0]['src'];?>" alt="product">
                            </div>
                            <div class="pd-detail">
                                <div class="title-wrap">
                                    <h3 class="pd-title"><?php echo $product['name'];?></h3>
                                    <div class="bs-amount">
                                        <strong class="amount-val">&#8377; <span id="amt_val_cat<?php echo $product['id'];?>" ><?php echo $product['variations_price'][0];?></span></strong>
                                        <strong class="amount-qty">/ <span id="amt_qty_cat<?php echo $product['id'];?>" ><?php echo $product['attributes'][0]['options'][0];?> gm</span></strong>
                                    </div>
                                </div>
                                <div class="act-wrap">
                                    <div class="bs-custom-select typ-box">
                                        <select id="product_qty_dd">
                                                <?php foreach($product['attributes'][0]['options'] as $opt_key=>$opt_value) {
                                                        $sel_wt = "";
                                                        if($opt_key==0){
                                                            $sel_wt = "selected='selected'";
                                                        } ?>
                                                <option value="<?php echo $product['variations_price'][$opt_key].";".$opt_value.";".$product['variations'][$opt_key].";".$product['id'];?>"       <?php echo $sel_wt;?>><?php echo $opt_value;?>gm</option>
                                                <?php } ?>
                                            </select>
                                    </div>
                                    <button type="button"  onclick="add_to_cart(this)" data-product-id="<?php echo $product['id'];?>" data-weight="<?php echo $product['attributes'][0]['options'][0];?>"  data-var-id="<?php echo $product['variations'][0];?>" id="add_to_cart<?php echo $product['id'];?>" class="btn btn-icon btn-default"><span class="icon icon-cart"></span></button>
                                </div>
                            </div>
                        </div>
                    </li>
                    <?php 
                    }
                endforeach ?>

                </ul>
            </div>

             <?php endforeach ?>
            
        </div>
        <button class="btn-icon btn-default fly-btn"><span class="icon icon-square"></span><span class="icon icon-cross"></span></button>
    </div>
 
<?php $this->load->view('include/footer_2.php'); ?>
<?php $this->load->view('include/footer_js.php'); ?>
<?php $this->load->view('include/commonsite_js.php'); ?>
<script type="text/javascript">
    $( document ).ready(function() {
            var pincode = $("#shipping_pincode").val()
           if(pincode!=""){
            $("#shipping_pincode").val(pincode)
                getShipping()
           }

           
        });

    function move_to_shipping(){
        getShipping();
        var pincode_status = $("#pincode_status").val()
        if(pincode_status=="invalid"){

                                setTimeout(function(){
                                    window.location.href= "<?php echo base_url();?>shipping";
                                    return false;
                                }, 2500);
            
        }else{
                                setTimeout(function(){
                                    window.location.href= "<?php echo base_url();?>shipping";
                                    return false;
                                }, 2500);        
        }
        
    }
</script>

</body>

</html>