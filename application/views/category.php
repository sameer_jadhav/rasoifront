<!DOCTYPE html>
<html>
<?php $this->load->view('include/head.php'); ?>
<body class="pg-home">
    
    	<?php $this->load->view('include/header.php'); ?>

    	    <main>
        <div class="bs-main">
            <!-- banner start -->
            <div class="bs-banner typ-sm">
    <div class="banner-media js-addto-img">
        <img src="assets/images/banner/listing-banner.jpg" data-mbsrc="assets/images/banner/listing-banner-mb.jpg" alt="banner" class="addto-img">
    </div>
    <div class="banner-cont">
        <h2 class="title">
            <span class="cm-line-break cm-cursive">checkout</span>
            <span class="cm-line-break">All Variety Spices</span>
        </h2>
    </div>
</div>
            <!-- banner end -->
            <section>
                <div class="bs-sec bs-tabs sec-tab sec-first-child js-nav-target">
                    <div class="sec-head">
                        <ul class="nav nav-tabs wow fadeInUp" id="js-product-list" data-wow-duration="0.4s" data-wow-delay="0.2s">
                            <li class="active"><a data-toggle="tab" href="#all-products"><span class="icon icon-square"></span>All</a></li>
                            <?php foreach ($category as $key => $value): 
                            	switch ($value['slug']) {
                            		case 'blends':
                            			$class_icon = "icon-seasoning";
                            			break;

                            		case 'ground-spice':
                            			$class_icon = "icon-spa";
                            			break;

                            		case 'whole-spice':
                            			$class_icon = "icon-kitchen";
                            			break;
                            		
                            		default:
                            			# code...
                            			break;
                            	}
                            ?>
                            <li><a data-toggle="tab" href="#<?php echo $value['slug'];?>"><span class="icon <?php echo $class_icon;?>"></span><?php echo $value['name'];?></a></li>
                            <?php endforeach ?>  
                        </ul>
                        <div class="tab-select bs-custom-select typ-box">
                            <select id="tabSelect">
                                <option value="all" data-href="#all-products">all</option>

                                <?php foreach ($category as $key => $value): ?>
                                	<option value="<?php echo $value['slug'];?>" data-href="#<?php echo $value['slug'];?>"><?php echo $value['name'];?></option>
                                 <?php endforeach ?> 
                                
                            </select>
                        </div>
                    </div>
                    <div class="sec-cont">
                        <div class="container">
                            <div class="tab-content">
                                <div id="all-products" class="tab-pane fade in active">
                                    <div class="lyt-product-listing wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.3s">
                                        <ul class="list-wrap row">
                                        	<?php 
                                        	$product_count = count($products);
                                        	for($j=0;$j<$product_count;$j++){ ?>

                                            <li class="item col-md-4 col-xs-6">
                                                <div class="bs-product typ-tag">
                                                    <div class="pd-head">
                                                        <figure class="pd-img-wrap">
                                                            <img src="<?php echo $products[$j]['images'][0]['src'];?>" alt="<?php echo $products[$j]['images'][0]['alt'];?>">
                                                        </figure>
                                                        <div class="pd-desc">
                                                            <p>Garam masala is a blend of ground spices used extensively in Indian cuisine.</p>
                                                            <p>The spices for garam masala are usually toasted to bring out more flavor and aroma, and then ground</p>
                                                        </div>
                                                    </div>
                                                    <div class="pd-cont">
                                                        <h3 class="pd-title"><?php echo $products[$j]['name'];?></h3>
                                                        <div class="bs-amount">
                                                            <!-- <strong class="amount-val strike">Rs. 350</strong> -->
                                                            <strong class="amount-val">&#8377; <?php echo $products[$j]['variations_price'][0];?></strong>
                                                            <strong class="amount-qty">/ <?php echo $products[$j]['attributes'][0]['options'][0];?> gm</strong>
                                                        </div>
                                                        <div class="pd-action">

                                                            <!--button type="button" class="btn btn-icon btn-outline js-btn-modal" data-toggle="modal" onclick="product_summary(<?php echo $products[$j]['id'];?>);"><span class="icon icon-eye"></span></button-->
							    <a href="<?php echo base_url();?>shop/<?php echo $products[$j]['categories'][0]['slug'];?>/<?php echo $products[$j]['slug'];?>" class="btn btn-icon btn-outline"><span class="icon icon-eye"></span></a>

                                                            <!--button type="button" class="btn btn-default js-add-cart">add to cart</button-->
                                                            <button onclick="add_to_cart(this)" data-product-id="<?php echo $products[$j]['id'];?>" data-weight="<?php echo $products[$j]['attributes'][0]['options'][0];?>" data-var-id="<?php echo $products[$j]['variations'][0];?>" id="add_to_cart"  type="button" class="btn btn-default js-add-cart">add to cart</button>

                                                        </div>
                                                    </div>
                                                    <!-- <a href="#" class="pd-link"></a> -->
                                                    <button type="button" class="btn pd-btn-like js-like icon-wishlist-o" onclick="add_wishlist('<?php echo $products[$j]['id'];?>')"></button>
                                                    <?php if(!empty($products[$j]['tags'])){ ?>
                                                    <label class="pd-tag"><?php echo $products[$j]['tags'][0]['name'];?></label>
                                                     <?php } ?>
                                                    <div class="pd-action">
                                                        <div class="action-wrap">
                                                            <?php 
                                                            $like_class=''; 
                                                             if (in_array($products[$j]['id'], $stored_cookie)) {
                                                                $like_class='active';
                                                             ?>
                                                            <button type="button" class="btn btn-icon btn-outline js-like-btn hidden-xs" id="btnRemoveWishlist<?php echo $products[$j]['id'];?>" onclick="remove_wishlist_modal('<?php echo $products[$j]['id'];?>')" ><span class="icon icon-wishlist-o <?php echo $like_class; ?>"></span></button>
                                                            <?php }else{ ?>
                                                            <button type="button" class="btn btn-icon btn-outline js-like-btn hidden-xs" id="btnWishlist<?php echo $products[$j]['id'];?>" onclick="add_wishlist('<?php echo $products[$j]['id'];?>')" ><span class="icon icon-wishlist-o <?php echo $like_class; ?>"></span></button>
                                                            <?php } ?>

                                                            <!--button type="button" class="btn btn-icon btn-outline js-btn-modal" data-toggle="modal" onclick="product_summary(<?php echo $products[$j]['id'];?>);"><span class="icon icon-eye"></span></button-->
                                                            <a href="<?php echo base_url();?>shop/<?php echo $products[$j]['categories'][0]['slug'];?>/<?php echo $products[$j]['slug'];?>" class="btn btn-icon btn-outline"><span class="icon icon-eye"></span></a>
								<button type="button" class="btn btn-icon btn-default js-add-cart" onclick="add_to_cart(this)" data-product-id="<?php echo $products[$j]['id'];?>" data-weight="<?php echo $products[$j]['attributes'][0]['options'][0];?>" data-var-id="<?php echo $products[$j]['variations'][0];?>" id="add_to_cart"><span class="icon icon-cart"></span></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>

                                <?php foreach ($category as $key => $value): ?>
                                <div id="<?php echo $value['slug'];?>" class="tab-pane fade">
                                    <div class="lyt-product-listing">
                                        <ul class="list-wrap row">
                                        	<?php 
                                        	$product_count = count($products);
                                        	for($k=0;$k<$product_count;$k++){
                                        		//$products = $value['products'];
                                        		if(in_array($value['id'], array_column($products[$k]['categories'], 'id'))) { // search value in the array
                                        	 ?>
                                            <li class="item col-md-4 col-xs-6">
                                                <div class="bs-product typ-tag">
                                                    <div class="pd-head">
                                                        <figure class="pd-img-wrap">
                                                            <img src="<?php echo $products[$k]['images'][0]['src'];?>" alt="<?php echo $products[$k]['images'][0]['alt'];?>">
                                                        </figure>
                                                        <div class="pd-desc">
                                                            <p>Garam masala is a blend of ground spices used extensively in Indian cuisine.</p>
                                                            <p>The spices for garam masala are usually toasted to bring out more flavor and aroma, and then ground</p>
                                                        </div>
                                                    </div>
                                                    <div class="pd-cont">
                                                        <h3 class="pd-title"><?php echo $products[$k]['name'];?></h3>
                                                        <div class="bs-amount">
                                                            <!-- <strong class="amount-val strike">Rs. 350</strong> -->
                                                            
                                                            <strong class="amount-val">&#8377; <?php echo $products[$k]['variations_price'][0];?></strong>
                                                            <strong class="amount-qty">/ <?php echo $products[$k]['attributes'][0]['options'][0];?> gm</strong>
                                                        </div>
                                                        <div class="pd-action">
                                                            <!--button type="button" class="btn btn-icon btn-outline js-btn-modal" data-toggle="modal" onclick="product_summary(<?php echo $products[$k]['id'];?>);"><span class="icon icon-eye"></span></button-->
							    <a href="<?php echo base_url();?>shop/<?php echo $products[$k]['categories'][0]['slug'];?>/<?php echo $products[$k]['slug'];?>" class="btn btn-icon btn-outline"><span class="icon icon-eye"></span></a>
                                                            <!--button type="button" class="btn btn-default js-add-cart">add to cart</button-->
                                                            <button onclick="add_to_cart(this)" data-product-id="<?php echo $products[$k]['id'];?>" data-weight="<?php echo $products[$k]['attributes'][0]['options'][0];?>" data-var-id="<?php echo $products[$k]['variations'][0];?>" id="add_to_cart"  type="button" class="btn btn-default js-add-cart">add to cart</button>

                                                        </div>
                                                    </div>
                                                    <!-- <a href="#" class="pd-link"></a> -->
                                                    <button type="button" class="btn pd-btn-like js-like icon-wishlist-o" onclick="add_wishlist('<?php echo $products[$k]['id'];?>')"></button>
                                                    <?php if(!empty($products[$k]['tags'])){ ?>
                                                    <label class="pd-tag"><?php echo $products[$k]['tags'][0]['name'];?></label>
                                                    <?php } ?>
                                                    <div class="pd-action">
                                                        <div class="action-wrap">
                                                            <?php 
                                                            $like_class_2=''; 
                                                             if (in_array($products[$k]['id'], $stored_cookie)) {
                                                                $like_class_2 ='active';
                                                             ?>
                                                            <button type="button" class="btn btn-icon btn-outline js-like-btn hidden-xs" id="btnRemoveWishlistcat<?php echo $products[$k]['id'];?>" onclick="remove_wishlist_modal('<?php echo $products[$k]['id'];?>')" ><span class="icon icon-wishlist-o <?php echo $like_class_2; ?>"></span></button>
                                                            <?php }else{ ?>
                                                            <button type="button" class="btn btn-icon btn-outline js-like-btn hidden-xs" id="btnWishlistcat<?php echo $products[$k]['id'];?>" onclick="add_wishlist('<?php echo $products[$k]['id'];?>')" ><span class="icon icon-wishlist-o <?php echo $like_class_2; ?>"></span></button>
                                                            <?php } ?>
                                                            <!--button type="button" class="btn btn-icon btn-outline js-btn-modal" data-toggle="modal" onclick="product_summary(<?php echo $products[$k]['id'];?>);"><span class="icon icon-eye"></span></button-->
							    <a href="<?php echo base_url();?>shop/<?php echo $products[$k]['categories'][0]['slug'];?>/<?php echo $products[$k]['slug'];?>" class="btn btn-icon btn-outline"><span class="icon icon-eye"></span></a>
                                                            
                                                             <button onclick="add_to_cart(this)" data-product-id="<?php echo $products[$k]['id'];?>" data-weight="<?php echo $products[$k]['attributes'][0]['options'][0];?>" data-var-id="<?php echo $products[$k]['variations'][0];?>" id="add_to_cart"  type="button" class="btn btn-icon btn-default js-add-cart"><span class="icon icon-cart"></span></button>

                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <?php 
                                            	}
                                        	} ?>
                                        </ul>
                                    </div>
                                </div>
                                <?php endforeach ?>  
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="container">
                    <div class="bs-sec typ-horizon sec-last-child">
                        <div class="sec-head">
                            <h2 class="sec-title">
                                <span class="cm-cursive cm-line-break">Our most</span>
                                <span class="cm-line-break">Popular Spice</span>
                            </h2>
                        </div>
                        <div class="sec-cont">
                            <div class="bs-product typ-hero typ-tag">
                                <div class="pd-head">
                                    <figure class="pd-img-wrap">
                                        <img src="<?php echo $featured[0]['images'][0]['src'];?>" alt="<?php echo $featured[0]['images'][0]['alt'];?>">
                                    </figure>
                                    <div class="pd-desc">
                                        <?php echo $featured[0]['short_description'];?>
                                    </div>
                                </div>
                                <div class="pd-cont">
                                    <h3 class="pd-title"><?php echo $featured[0]['name'];?></h3>
                                    <div class="bs-amount">
                                        <strong class="amount-val">&#8377; <?php echo $featured[0]['variations_price'][0];?></strong>
                                        <strong class="amount-qty">/ <?php echo $featured[0]['attributes'][0]['options'][0];?> gm</strong>
                                    </div>
                                    <div class="pd-action">
                                        <button type="button" class="btn btn-icon btn-outline js-btn-modal" onclick="product_summary(<?php echo $featured[0]['id'];?>);" data-toggle="modal" data-target="#myModal"><span class="icon icon-eye"></span></button>
                                        <!-- <button type="button" class="btn btn-default js-add-cart">add to cart</button> -->

                                         <button onclick="add_to_cart(this)" data-product-id="<?php echo $featured[0]['id'];?>" data-weight="<?php echo $featured[0]['attributes'][0]['options'][0];?>" data-var-id="<?php echo $featured[0]['variations'][0];?>" id="add_to_cart"  type="button" class="btn btn-default js-add-cart">add to cart</button>

                                    </div>
                                </div>
                                <a href="#" class="pd-link"></a>
                                <button type="button" class="btn pd-btn-like js-like icon-wishlist-o"></button>
                                <label class="pd-tag">Best Whole Spice</label>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>

    <?php $this->load->view('include/footer.php'); ?>
    <?php $this->load->view('include/footer_2.php'); ?>
    <?php $this->load->view('include/footer_js.php'); ?>
    <?php $this->load->view('include/commonsite_js.php'); ?>

    </body>

</html>
