
<!DOCTYPE html>
<html>
<?php 
$userData = $this->session->userdata('user');
$strNo = rand(1,1000000);
$strCurDate = date('d-m-Y');
$txnid = time();
$surl = $surl;
$furl = $furl;  
$key_id = rzp_test_KEY_ID;
$currency_code = $this->session->userdata('currency');
//print_r($cart_data);exit;
$total = ( $cart_data['total'])* 100; 
$amount =  $cart_data['total'] ;
$merchant_order_id = $strNo;
$card_holder_name = $userData['user_display_name'];
$email = $userData['email'];
$phone = $phone_no;
$name = "RasoitatvA LLP";
$return_url = site_url().'checkout/callback';
$area_code =  $this->session->userdata('shipping_area_code');
$this->load->view('include/head.php'); ?>

<body class="pg-banner">

   <?php $this->load->view('include/header.php'); ?>

    <main>
        <div class="bs-main">
            <div class="container">
                <!--ul class="mod-breadcrumbs">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Cart</a></li>
                    <li><a href="#">Shipping</a></li>
                    <li><a href="#" class="active">Payment</a></li>
                </ul-->
                <section>
                    <div class="bs-progress-tab">
                        <ul class="progress-list">
                            <li class="progress-item">
                                <a href="<?php echo base_url();?>cart" class="progress-link completed"><span class="icon icon-cart"></span>In Cart <span class="count">(<?php echo count($cart);?>)</span></a>
                            </li>
                            <li class="progress-item">
                                <a href="<?php echo base_url();?>shipping" class="progress-link completed"><span class="icon icon-track"></span>Shipping Details</a>
                            </li>
                            <li class="progress-item">
                                <a href="<?php echo base_url();?>checkout" class="progress-link active"><span class="icon icon-pay"></span>Payment</a>
                            </li>
                        </ul>
                        <form method="post" action="<?php echo $return_url; ?>" id="checkout_form" name="checkout_form">
                         
                        <div class="lyt-payment">
                            <div class="head-wrap">
                                <h2 class="p-title">Total amount payable <strong>&#8377; <?php echo $amount;?></strong></h2>
                            </div>
                            <ul class="cont-wrap">

                               
                                <!-- <li class="p-item">
                                    <div class="bs-payment">
                                        <span class="icon icon-cod"></span>
                                        <h3 class="payment-title">Cash On Delivery</h3>
                                        <div class="payment-desc">
                                            <p>Kindly, confirm your Cash on Delivery as a payment mode.</p>
                                        </div>
                                        
                                        <button type="button" id="checkout_submit" onclick="codSubmit(this);" class="btn btn-default" >Place COD</button>
                                    </div>
                                </li-->

                                <li class="p-item">
                                    <div class="bs-payment">
                                        <span class="icon icon-online"></span>
                                        <h3 class="payment-title">Online Payment</h3>
                                        <div class="payment-desc">
                                            <p>Kindly, confirm your Online Payment as a payment mode.</p>
                                        </div>
                                        
                                        <button type="button" id="checkout_submit" onclick="razorpaySubmit(this);" class="btn btn-default">Pay Online</button>
                                    </div>
                                </li>
                            </ul>
                            <div class="foot-wrap">
                                <div class="mod-note">
                                    <p>If a confirmed order is cancelled more than 48 hrs before the scheduled delivery of the product, flat cancellation charges shall be deducted 8% of order value.</p>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id" />
                        <input type="hidden" name="merchant_order_id" id="merchant_order_id" value=""/>
                        <input type="hidden" name="merchant_trans_id" id="merchant_trans_id" value="<?php echo $txnid; ?>"/>
                        <input type="hidden" name="merchant_product_info_id" id="merchant_product_info_id" value="Beauty Product"/>

                        <input type="hidden" name="merchant_surl_id" id="merchant_surl_id" value="<?php echo $surl; ?>"/>
                        <input type="hidden" name="merchant_furl_id" id="merchant_furl_id" value="<?php echo $furl; ?>"/>
                        <input type="hidden" name="card_holder_name_id" id="card_holder_name_id" value="<?php echo $card_holder_name; ?>"/>
                        <input type="hidden" name="payment" id="payment_type" value="COD"/>
                        <input type="hidden" name="merchant_total" id="merchant_total" value="<?php echo $total; ?>"/>
                        <input type="hidden" name="merchant_amount" id="merchant_amount" value="<?php echo $amount; ?>"/>
                        <input type="hidden" name="address_id" value="<?php echo  $address_data['data']['address_id'];?>">
                         
                        
                        </form>

                    </div>
                </section>
            </div>
        </div>
    </main>
    <div class="bs-modal typ-message modal fade" id="otpModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bs-otp">
                        <h2 class="otp-title">Enter OTP</h2>
                        <form class="bs-form">
                            <div class="input-group">
                                <input type="text" class="form-control" maxlength="1">
                                <input type="text" class="form-control" maxlength="1">
                                <input type="text" class="form-control" maxlength="1">
                                <input type="text" class="form-control" maxlength="1">
                            </div>
                            <div class="form-action">
                                <button type="button" class="btn btn-default">Resend OTP</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('include/footer.php'); ?>
    <?php $this->load->view('include/footer_2.php'); ?>
    <?php $this->load->view('include/footer_js.php'); ?>
    <?php $this->load->view('include/commonsite_js.php'); ?>
    <!-- js group start -->

    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
  var razorpay_options = {
    key: "<?php echo $key_id; ?>",
    amount: "<?php echo $total; ?>",
    name: "<?php echo $name; ?>",
    description: "",
   /* method: {
        netbanking: true,
        card: true,
        wallet: true,
        upi: true
    },*/
    //netbanking: true,
    currency: "<?php echo $currency_code; ?>",
    prefill: {
      name:"<?php echo $card_holder_name; ?>",
      email: "<?php echo $email; ?>",
      contact: "<?php echo $phone; ?>",
      //method : "netbanking"
    },
     image : "<?php echo base_url();?>assets/images/logo-og.png",
    notes: {
      soolegal_order_id: "",
    },
    handler: function (transaction) {
        document.getElementById('razorpay_payment_id').value = transaction.razorpay_payment_id;
        document.getElementById('checkout_form').submit();
    },
    "modal": {
        "ondismiss": function(){
            location.reload()
        }
    }
  };
  //console.log(razorpay_options.prefill.method)
  var razorpay_submit_btn, razorpay_instance;

  function codSubmit(el){
        $("#payment_type").val("COD")
        $('#checkout_form').attr("action", "<?php echo base_url();?>checkout/process");
         event.preventDefault();
         $( "#checkout_form" ).submit();
  }

  function razorpaySubmit(el){
        $("#payment_type").val("Online Payment")
        if(typeof Razorpay == 'undefined'){
          setTimeout(razorpaySubmit, 200);
          if(!razorpay_submit_btn && el){
            razorpay_submit_btn = el;
            el.disabled = true;
            el.value = 'Please wait...';  
          }
        } else {
          if(!razorpay_instance){
            //razorpay_options.prefill.method = radioValue
            razorpay_instance = new Razorpay(razorpay_options);
            if(razorpay_submit_btn){
              razorpay_submit_btn.disabled = false;
              razorpay_submit_btn.value = "Pay Now";
            }
          }
          razorpay_instance.open();
        }    
    
  }  
</script>

</body>

</html>
