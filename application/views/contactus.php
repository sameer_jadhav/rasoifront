
<!DOCTYPE html>
<html>

<?php $this->load->view('include/head.php'); ?>

<body class="pg-banner">

    <?php $this->load->view('include/header.php'); ?>

    <main>
        <div class="bs-main">
            <div class="container">
                <ul class="mod-breadcrumbs">
                    <li><a href="<?php echo base_url();?>">home</a></li>
                    <li><a href="<?php echo base_url();?>contact-us" class="active">contact us</a></li>
                </ul>
                <section>
                    <div class="bs-sec sec-first-child ">
                        <div class="sec-head">
                            <h1 class="sec-title">Contact us</h1>
                        </div>
                        <div class="sec-cont">
                            <div class="bs-contact bs-pos-ele">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="mod-reach-us">
                                            <span class="icon icon-office"></span>
                                            <div class="cont-wrap">
                                                <h3 class="title">Our Office</h3>
                                                <address>
                                                    <strong>Spicemantra Private Limited</strong>
                                                    <span>3rd floor, Plot No. C 95 & C95 Part,</span>
                                                    <span>Turbhe MIDC, TTC Industrial Area,</span>
                                                    <span>Navi Mumbai - 400705</span>
                                                </address>
                                                <a href="tel:+917538993333" class="mod-icon-text typ-inline"><span class="icon icon-call"></span> <span class="text">+91-7538993333</span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="mod-reach-us">
                                            <span class="icon icon-support"></span>
                                            <div class="cont-wrap">
                                                <ul class="contact-list">
                                                    <li class="contact-item">
                                                        <a href="mailto:customercare@rasoitatva.com" class="mod-icon-text typ-inline"><span class="icon icon-mail"></span><span class="text">customercare@rasoitatva.com</span></a>
                                                    </li>
                                                    <li class="contact-item">
                                                        <a href="tel:+917538993333" class="mod-icon-text typ-inline"><span class="icon icon-call"></span> <span class="text">+91-7538993333</span></a>
                                                    </li>
                                                    <li class="contact-item cm-hidden">
                                                        <a href="mailto:contact@rasoitatva.com" class="mod-icon-text typ-inline"><span class="icon icon-mail"></span><span class="text"><strong>For Bulk Orders</strong> contact@rasoitatva.com</span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="bs-form">
                                        <span id ="error" class="cm-error-success-text"></span>
                                            <form id="form" action="#" class="form">
                                                <div class="form-head">
                                                    <h3 class="form-title">For More Queries</h3>
                                                </div>
                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label for="" class="input-label">Enter Name</label>
                                                        <input type="text" class="form-control"  name="enquiry_name" required="" placeholder="Enter your full name">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="" class="input-label">Email ID</label>
                                                        <input type="text" name="enquiry_email" required="" class="form-control" placeholder="abc@gmail.com">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="" class="input-label">Enter Comments</label>
                                                        <textarea name="enquiry_comment" id="" rows="4" class="form-control"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-foot">
                                                    <button class="btn btn-default" id="btnEnq" type="submit">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <span class="b-l element ele13"></span>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </main>
    <?php $this->load->view('include/footer.php'); ?>
    <?php $this->load->view('include/footer_2.php'); ?>
    <?php $this->load->view('include/footer_js.php'); ?>
    <script type="text/javascript">

    $.validator.setDefaults({
        submitHandler: function() {
            //$('#btnLogin').text('processing...'); //change button text
            $('#btnEnq').addClass("btn-loader"); //change button text
            $('#btnEnq').attr('disabled', true); //set button disable 
            $('#error').html('');
              $.ajax({
                    url: '<?php echo site_url("contactus/submitEnquiry") ?>',
                    type: "POST",
                    data: $('#form').serialize(),
                    dataType: "html",
                    success: function (data)
                    {
                        //console.log(data)
                        if ($.trim(data)=="success") //if success close modal and reload ajax table
                        {
                           $('#form').trigger("reset");
                           //$('#error').html("Enquery sent Successfully");
                           $("#toaster-text").text("Your query has been successfully submitted");
                                 $("#normal_toaster").addClass("active");
                                setTimeout(function(){
                                    $("#normal_toaster").removeClass("active");
                                }, 5000);
                        } else
                        {                    
                           // $('#error').html("Enquery sending Failed");
                           $("#toaster-text").text("Some error occured,Please try again later");
                                 $("#normal_toaster").addClass("active");
                                setTimeout(function(){
                                    $("#normal_toaster").removeClass("active");
                                }, 5000);
                        }
                        $("#form").trigger('reset');

                        $('#btnEnq').removeClass("btn-loader"); //change button text
                        //$('#btnLogin').text('submit'); //change button text
                        $('#btnEnq').attr('disabled', false); //set button enable 


                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('error');

                    }
                });
        }
    });

$(document).ready(function () {

     jQuery("#form").validate({
       rules: {
                enquiry_name: "required", 
                enquiry_comment: "required",              
               enquiry_email: {
                   required: true,
                   email: true
               }
           },
           errorElement: "span",
                        errorClass: "error-msg",
                        errorPlacement: function(error, element) {
                             error.appendTo($(element).parent());
                        },
                        highlight: function (element) {
                            $(element).parents(".form-group").addClass("error");
                        },
                        unhighlight: function (element) {
                            $(element).parents(".form-group").removeClass("error");
                        },
   });

    });
    </script>

</body>

</html>