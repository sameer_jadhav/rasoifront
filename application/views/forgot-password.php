
<!DOCTYPE html>
<html>

<?php $this->load->view('include/head.php'); ?>

<body>

    <?php $this->load->view('include/header.php'); ?>

    <main>
        <div class="bs-main lyt-login">
            <div class="bs-login typ-no-img">
                <div class="head-wrap">
                    <h2 class="title">Forgot Password</h2>
                </div>
                <div class="cont-wrap">
                    <form method="post" id="form" action="<?php echo base_url('forgot_password/send_mail_forget_password') ?>">
                        <div class="bs-form">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="input-label">Email</label>
                                        <input type="text" name="email" required="" class="form-control" placeholder="Enter your Email Address">
                                        <!-- <span class="error-msg">Lorem Ipsum</span> -->
                                    </div>
                                </div>
                            </div>
                            <div class="form-action">
                                <button type="submit" id="btnForget" class="btn btn-default">Submit<span class="loader"></span></button>
                            </div>

                            <!-- <div class="form-action">
                                 
                                <a href="<?php echo base_url();?>signup" class="btn btn-outline">Create Account</a>
                                <button type="submit" id="btnLogin" type="button" class="btn btn-default hidden-xs">Sign In<span class="loader"></span></button>
                            </div> -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
    <?php $this->load->view('include/footer.php'); ?>
    <span class="cm-overlay"></span>
 <?php $this->load->view('include/footer_2.php'); ?>
<?php $this->load->view('include/footer_js.php'); ?>

<script type="text/javascript">
        $(document).ready(function () {
            jQuery("#form").validate({
                rules: {
                    password: "required",              
                email: {
                    required: true,
                    email: true
                }
            },
           errorElement: "span",
                        errorClass: "error-msg",
                        errorPlacement: function(error, element) {
                             error.appendTo($(element).parent());
                        },
                        highlight: function (element) {
                            $(element).parents(".form-group").addClass("error");
                        },
                        unhighlight: function (element) {
                            $(element).parents(".form-group").removeClass("error");
                        },
            });
             var message = "<?php echo $this->session->flashdata('msg'); ?>";
             if(message=="forgot-pass-mail-sent"){
                $('#forgot-message').modal('show');

             }

             if(message=="not-valid-user"){
                $('#forgot-message-error').modal('show');

             }
        });
    </script>
</body>

</html>