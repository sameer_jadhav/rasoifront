<!DOCTYPE html>
<html>
<?php $this->load->view('include/head.php'); ?>
<body class="pg-home">
   
    	<?php $this->load->view('include/header.php'); ?>
    	<main>
        <div class="bs-main">
            <!-- banner start -->
            <div class="bs-banner typ-slider js-banner bs-pos-ele">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide js-responsive">
                            <div class="banner-media js-addto-img">
                                <img src="../../assets/images/banner/banner-1.jpg" data-mbsrc="../../assets/images/banner/banner-1-mb.jpg" alt="Indian spices online shopping" class="addto-img">
                            </div>
                            <div class="banner-cont typ-hero cm-hidden">
                                <h2 class="title">
                                    <span class="cm-line-break cm-cursive">Best Indian spices</span>
                                    <span class="cm-line-break">from the best Indian farms.</span>
                                </h2>
                                <div class="banner-desc">
                                    <p>Whole spices - ground spices - select blends</p>
                                </div>
                                <a href="product-listing.html" class="btn btn-default">Explore Products</a>
                            </div>
				<a href="<?php echo base_url();?>shop" class="swiper-link"></a>
                        </div>
                        <div class="swiper-slide js-responsive">
                            <div class="banner-media js-addto-img">
                                <img src="../../assets/images/banner/banner-2.jpg" data-mbsrc="../../assets/images/banner/banner-2-mb.jpg" alt="Indian masala online shopping
" class="addto-img">
                            </div>
                            <div class="banner-cont cm-hidden">
                                <h2 class="title">
                                    <span class="cm-line-break">Intense flavour and aroma</span>
                                    <span class="cm-line-break">to the last pinch.</span>
                                </h2>
                                <div class="banner-desc">
                                    <p>Whole spices - ground spices - select blends</p>
                                </div>
                                <a href="product-listing.html" class="btn btn-default">Shop Products</a>
                            </div>
			    <a href="<?php echo base_url();?>shop" class="swiper-link"></a>
                        </div>
                        <div class="swiper-slide js-responsive">
                            <div class="banner-media js-addto-img">
                                <img src="../../assets/images/banner/banner-3.jpg" data-mbsrc="../../assets/images/banner/banner-3-mb.jpg" alt="banner" class="addto-img">
                            </div>
                            <div class="banner-cont typ-black cm-hidden">
                                <h2 class="title">
                                    <span class="cm-line-break">Make mouth</span>
                                    <span class="cm-line-break">watering meals, every day!</span>
                                </h2>
                                <div class="banner-desc">
                                    <p>Whole spices - ground spices - select blends</p>
                                </div>
                                <a href="product-listing.html" class="btn btn-default">Shop Products</a>
                            </div>

			    <a href="<?php echo base_url();?>shop" class="swiper-link"></a>
                        </div>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
                <span class="b-r element ele-taj"></span>
            </div>
            <!-- banner end -->
            <section>
                <div class="bs-sec bs-tabs sec-tab sec-first-child bs-pos-ele">
                    <div class="sec-head">
                        <h2 class="sec-title">Product Range</h2>
                        <ul class="nav nav-tabs wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.3s">
                            <li class="active"><a data-toggle="tab" href="#trending"><span class="icon icon-square"></span>Trending</a></li>

                            <?php foreach ($category as $key => $value): 
                            	switch ($value['slug']) {
                            		case 'blends':
                            			$class_icon = "icon-seasoning";
                            			break;

                            		case 'ground-spice':
                            			$class_icon = "icon-spa";
                            			break;

                            		case 'whole-spice':
                            			$class_icon = "icon-kitchen";
                            			break;
                            		
                            		default:
                            			# code...
                            			break;
                            	}
                            ?>
                            <li><a data-toggle="tab" href="#<?php echo $value['slug'];?>"><span class="icon <?php echo $class_icon;?>"></span><?php echo $value['name'];?></a></li>
                             <?php endforeach ?>  
                            
                        </ul>
                        <div class="tab-select bs-custom-select typ-box">
                            <select id="tabSelect">
                            	<option value="all" data-href="#trending">Trending</option>
                            	<?php foreach ($category as $key => $value): ?>
                                	<option value="<?php echo $value['slug'];?>" data-href="#<?php echo $value['slug'];?>"><?php echo $value['name'];?></option>
                                 <?php endforeach ?>  
                            </select>
                        </div>
                    </div>
                    <div class="sec-cont wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.4s">
                        <div class="container">
                            <div class="tab-content">
                                <div id="trending" class="tab-pane fade in active">
                                    <div class="lyt-product-grid">
                                        <ul class="row">
                                            <li class="col-md-6 col-xs-12 pg-tile">
                                                <div class="bs-product typ-hero typ-tag">
                                                    <div class="pd-head">
                                                        <figure class="pd-img-wrap">
                                                            <img src="<?php echo $trending[0]['images'][0]['src'];?>" alt="<?php echo $trending[0]['images'][0]['alt'];?>">
                                                        </figure>
                                                        <div class="pd-desc">
                                                            <?php echo $trending[0]['short_description'];?>
                                                        </div>
                                                    </div>
                                                    <div class="pd-cont">
                                                        <h3 class="pd-title"><?php echo $trending[0]['name'];?></h3>
                                                        <div class="bs-amount">
                                                            <!-- <strong class="amount-val strike">Rs. 350</strong> -->
                                                            <strong class="amount-val">&#8377; <?php echo $trending[0]['variations_price'][0];?></strong>
                                                            <strong class="amount-qty">/ <?php echo $trending[0]['attributes'][0]['options'][0];?> gm</strong>
                                                        </div>
                                                        <div class="pd-action">
                                                            <button type="button" class="btn btn-icon btn-outline js-btn-modal" data-toggle="modal" onclick="product_summary(<?php echo $trending[0]['id'];?>);"><span class="icon icon-eye"></span></button>
                                                            <button onclick="add_to_cart(this)" data-product-id="<?php echo $trending[0]['id'];?>" data-var-id="<?php echo $trending[0]['variations'][0];?>" id="add_to_cart_home" data-weight="<?php echo $trending[0]['attributes'][0]['options'][0];?>"  type="button" class="btn btn-default js-add-cart">add to cart</button>
                                                        </div>
                                                    </div>
                                                    <a href="<?php echo base_url();?>shop/<?php echo $trending[0]['categories'][0]['slug'];?>/<?php echo $trending[0]['slug'];?>" class="pd-link"></a>

                                                    <?php 
                                                    $like_class=''; 
                                                     if (in_array($trending[0]['id'], $stored_cookie)) {
                                                        $like_class='active';
                                                     ?>
                                                        <button type="button" id="btnRemoveWishlist<?php echo $trending[0]['id'];?>" onclick="remove_wishlist_modal('<?php echo $trending[0]['id'];?>')" class="btn pd-btn-like js-like <?php echo $like_class ?> icon-wishlist-o">
                                                            
                                                        </button>
                                                    <?php }else{ ?>
                                                        <button type="button" id="btnWishlist<?php echo $trending[0]['id'];?>" onclick="add_wishlist('<?php echo $trending[0]['id'];?>')" class="btn pd-btn-like js-like <?php echo $like_class ?> icon-wishlist-o">
                                                            
                                                        </button>
                                                    <?php } ?>
                                                    <?Php if($trending[0]['tags'][0]['name']!=""){?>
                                                    <label class="pd-tag"><?php echo $trending[0]['tags'][0]['name'];?></label>
                                                    <?Php } ?>
                                                    <!-- <div class="pd-action">
                                                        <div class="action-wrap">
                                                            <button type="button" class="btn btn-icon btn-outline"><span class="icon icon-wishlist-o"></span></button>
                                                            <button type="button" class="btn btn-icon btn-outline js-btn-modal" data-toggle="modal" data-target="#myModal"><span class="icon icon-eye"></span></button>
                                                            <button type="button" class="btn btn-icon btn-default js-add-cart"><span class="icon icon-cart"></span></button>
                                                        </div>
                                                    </div> -->
                                                </div>
                                            </li>
                                            <li class="col-md-6 col-xs-12 pg-tile-wrap">
                                                <div class="row">
                                                	<?php  
                                                	$trending_count = count($trending);
                                                	for($i=1;$i<$trending_count;$i++){ ?>
                                                    <div class="col-md-6 col-xs-6 pg-tile">
                                                        <div class="bs-product">
                                                            <div class="pd-head">
                                                                <figure class="pd-img-wrap">
                                                                    <img src="<?php echo $trending[$i]['images'][0]['src'];?>" alt="<?php echo $trending[$i]['images'][0]['alt'];?>">
                                                                </figure>
                                                                <div class="pd-desc">
                                                                    <p>Garam masala is a blend of ground spices used extensively in Indian cuisine.</p>
                                                                    <p>The spices for garam masala are usually toasted to bring out more flavor and aroma, and then ground</p>
                                                                </div>
                                                            </div>
                                                            <div class="pd-cont">
                                                                <h3 class="pd-title"><?php echo $trending[$i]['name'];?></h3>
                                                                <div class="bs-amount">
                                                                    <!-- <strong class="amount-val strike">Rs. 350</strong> -->
                                                                    <strong class="amount-val">&#8377; <?php echo $trending[$i]['variations_price'][0];?></strong>
                                                                    <strong class="amount-qty">/ <?php echo $trending[$i]['attributes'][0]['options'][0];?> gm</strong>
                                                                </div>
                                                                <div class="pd-action">
                                                                    <button type="button" class="btn btn-icon btn-outline js-btn-modal" data-toggle="modal" onclick="product_summary(<?php echo $trending[$i]['id'];?>);"><span class="icon icon-eye"></span></button>

                                                                    <button onclick="add_to_cart(this)" data-product-id="<?php echo $trending[$i]['id'];?>" data-var-id="<?php echo $trending[$i]['variations'][0];?>" id="add_to_cart" data-weight="<?php echo $trending[$i]['attributes'][0]['options'][0];?>"  type="button" class="btn btn-default js-add-cart">add to cart</button>
                                                                    
                                                                </div>
                                                            </div>
                                                            <a href="<?php echo base_url();?>shop/<?php echo $trending[$i]['categories'][0]['slug'];?>/<?php echo $trending[$i]['slug'];?>" class="pd-link"></a>
                                                            <?php 
                                                            $like_class_2=''; 
                                                             if (in_array($trending[$i]['id'], $stored_cookie)) {
                                                                $like_class_2='active';
                                                            
                                                            ?>

                                                             <button type="button" id="btnRemoveWishlist<?php echo $trending[$i]['id'];?>" onclick="remove_wishlist_modal('<?php echo $trending[$i]['id'];?>')" class="btn pd-btn-like js-like <?php echo $like_class_2 ?> icon-wishlist-o">
                                                            
                                                                </button>
                                                            <?php }else{ ?>
                                                                <button type="button" id="btnWishlist<?php echo $trending[$i]['id'];?>" onclick="add_wishlist('<?php echo $trending[$i]['id'];?>')" class="btn pd-btn-like js-like <?php echo $like_class_2 ?> icon-wishlist-o">
                                                                    
                                                                </button>
                                                            <?php } ?>
                                                            
                                                            <label class="pd-tag">Greatest Blend</label>
                                                            <div class="pd-action">
                                                                <div class="action-wrap">
                                                                    <button type="button" class="btn btn-icon btn-outline"><span class="icon icon-wishlist-o"></span></button>
                                                                    <button type="button" class="btn btn-icon btn-outline js-btn-modal" data-toggle="modal" onclick="product_summary(<?php echo $trending[$i]['id'];?>);"><span class="icon icon-eye"></span></button>
                                                                    <button type="button" onclick="add_to_cart(this)" data-product-id="<?php echo $trending[$i]['id'];?>" data-var-id="<?php echo $trending[$i]['variations'][0];?>" data-weight="<?php echo $trending[$i]['attributes'][0]['options'][0];?>" id="add_to_cart"  class="btn btn-icon btn-default js-add-cart"><span class="icon icon-cart"></span></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <?php foreach ($category as $key => $value): ?>
                                <div id="<?php echo $value['slug'];?>" class="tab-pane fade">
                                    <div class="lyt-product-listing typ-grid4">
                                        <ul class="list-wrap row">
                                        	<?php 
                                        	$product_count = count($products);
                                        	for($j=0;$j<$product_count;$j++){
                                        		//$products = $value['products'];
                                        		if(in_array($value['id'], array_column($products[$j]['categories'], 'id'))) { // search value in the array
                                        	 ?>
                                            <li class="item col-md-3 col-xs-6">
                                                <div class="bs-product typ-tag">
                                                    <div class="pd-head">
                                                        <figure class="pd-img-wrap">
                                                            <img src="<?php echo $products[$j]['images'][0]['src'];?>" alt="<?php echo $products[$j]['images'][0]['alt'];?>">
                                                        </figure>
                                                        <div class="pd-desc">
                                                            <p>Garam masala is a blend of ground spices used extensively in Indian cuisine.</p>
                                                            <p>The spices for garam masala are usually toasted to bring out more flavor and aroma, and then ground</p>
                                                        </div>
                                                    </div>
                                                    <div class="pd-cont">
                                                        <h3 class="pd-title"><?php echo $products[$j]['name'];?></h3>
                                                        <div class="bs-amount">
                                                            <!-- <strong class="amount-val strike">Rs. 350</strong> -->
                                                            <strong class="amount-val">&#8377; <?php echo $products[$j]['variations_price'][0];?></strong>
                                                            <strong class="amount-qty">/ <?php echo $products[$j]['attributes'][0]['options'][0];?>  gm</strong>
                                                        </div>
                                                        <div class="pd-action">
                                                            <button type="button" class="btn btn-icon btn-outline js-btn-modal" data-toggle="modal" onclick="product_summary(<?php echo $products[$j]['id'];?>);"><span class="icon icon-eye"></span></button>

                                                            <button onclick="add_to_cart(this)" data-product-id="<?php echo $products[$j]['id'];?>" data-var-id="<?php echo $products[$j]['variations'][0];?>" data-weight="<?php echo $products[$j]['attributes'][0]['options'][0];?>" id="add_to_cart"  type="button" class="btn btn-default js-add-cart">add to cart</button>
                                                            
                                                        </div>
                                                    </div>
                                                    <!-- <a href="#" class="pd-link"></a> -->
                                                    <button type="button" class="btn pd-btn-like js-like icon-wishlist-o"></button>
                                                    <?php if(!empty($products[$j]['tags'])){ ?>
                                                    <label class="pd-tag"><?php echo $products[$j]['tags'][0]['name'];?></label>
                                                    <?php } ?>
                                                    <div class="pd-action">
                                                        <div class="action-wrap">
                                                            <?php 
                                                            $like_class_2=''; 
                                                             if (in_array($products[$j]['id'], $stored_cookie)) {
                                                                $like_class_2='active';
                                                            
                                                            ?>

                                                             

                                                                <button type="button" class="btn btn-icon btn-outline js-like-btn hidden-xs" id="btnRemoveWishlistcat<?php echo $products[$j]['id'];?>" onclick="remove_wishlist_modal('<?php echo $products[$j]['id'];?>')" ><span class="icon icon-wishlist-o <?php echo $like_class_2 ?>"></span></button>
                                                            <?php }else{ ?>
                                                               
                                                                <button type="button" class="btn btn-icon btn-outline js-like-btn hidden-xs" id="btnWishlistcat<?php echo $products[$j]['id'];?>" onclick="add_wishlist('<?php echo $products[$j]['id'];?>')" ><span class="icon icon-wishlist-o <?php echo $like_class_2 ?>"></span></button>
                                                                    
                                                                </button>
                                                            <?php } ?>

                                                            <button type="button" class="btn btn-icon btn-outline js-btn-modal" data-toggle="modal" onclick="product_summary(<?php echo $products[$j]['id'];?>);"><span class="icon icon-eye"></span></button>
                                                            <button type="button" class="btn btn-icon btn-default js-add-cart" onclick="add_to_cart(this)" data-product-id="<?php echo $products[$j]['id'];?>" data-weight="<?php echo $products[$j]['attributes'][0]['options'][0];?>" data-var-id="<?php echo $products[$j]['variations'][0];?>" id="add_to_cart" ><span class="icon icon-cart"></span></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <?php 
                                            	}
                                        	} ?>
                                        </ul>
                                    </div>
                                </div>
                                <?php endforeach ?>  
                            </div>
                        </div>
                    </div>
                    <span class="t-l element ele2"></span>
                </div>
            </section>
            <section>
                <div class="bs-sec wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.5s">
                    <div class="container">
                        <div class="sec-cont">
                            <div class="bs-img-info typ-right typ-vertical-center">
                                <div class="img-wrap bs-pos-ele">
                                    <img src="assets/images/truth-every-bite.jpg" alt="xyz">
                                    <span class="b-l element ele3"></span>
                                </div>
                                <div class="info-wrap" data-aos="fade-right" data-aos-duration="500">
                                    <div class="bs-info cm-text-center typ-fixed bs-pos-ele">
                                        <h3 class="title">
                                            <span class="cm-line-break cm-cursive">A pinch of </span>
                                            <span class="cm-line-break">Truth In Every Bite</span>
                                        </h3>
                                        <div class="desc">
                                            <p>We bring to you over 100 years of legacy, passion and in-depth knowledge of spices. Every care is taken in selection, processing, packing and storage of our spices. It starts with selection of right variety
                                                of Spice, perfect time, multiple cleaning processes and chilled air grinding to preserve vital spice oils for every pack. Our robust food safety system ensures only pure and safe spice is packed. RASOI TATVA
                                                brings to your table a select range of Premium Spices that you would need to cook wholesome and tasty food for your family. Every spice at RASOI TATVA goes through stringent quality check that would meet
                                                the expectations of a connoisseur. You deserve the best!</p>
                                        </div>
                                        <a href="<?php echo base_url();?>aboutus" type="button" class="btn btn-default">know more</a>
                                        <span class="b-l element ele4"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="bs-img-block bs-pos-ele wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.5s">
                <div class="img-wrap js-addto-img">
                    <img src="assets/images/finest-spice.jpg" data-mbsrc="assets/images/finest-spice-mb.jpg" alt="banner" class="addto-img">
                </div>

                <div class="info-wrap cm-hidden">
                    <h3 class="title">
                        <span class="cm-line-break cm-cursive">Finest spices for</span> Connoisseur In You!
                    </h3>
                </div>

                <span class="t-l element ele5"></span>
                <span class="b-r element ele6"></span>
            </div>
            <section>
                <div class="bs-sec">
                    <div class="sec-cont">
                        <div class="container">
                            <div class="bs-usp wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.4s">
                                <ul class="usp-list row">
                                    <li class="item col-md-4">
                                        <div class="mod-usp">
                                            <div class="img-wrap">
                                                <img src="assets/images/dummy-usp-1.jpg" alt="usp">
                                            </div>
                                            <div class="info-wrap">
                                                <h3 class="title">Best quality spices</h3>
                                                <p class="desc">Spice that is not fit for our own consumption is not meant for sale.</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item col-md-4">
                                        <div class="mod-usp">
                                            <div class="img-wrap">
                                                <img src="assets/images/dummy-usp-2.jpg" alt="usp">
                                            </div>
                                            <div class="info-wrap">
                                                <h3 class="title">International standards</h3>
                                                <p class="desc">Exporting worldwide since 1997 </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item col-md-4">
                                        <div class="mod-usp">
                                            <div class="img-wrap">
                                                <img src="assets/images/dummy-usp-3.jpg" alt="usp">
                                            </div>
                                            <div class="info-wrap">
                                                <h3 class="title">Farm to Fork Approach
                                                </h3>
                                                <p class="desc">For every major spice a complete traceability can be established from farm till our point of sale.</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <div class="quality-wrap wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">
                                    <div class="row">
                                        <div class="col-md-5" data-aos="fade-right" data-aos-duration="500">
                                            <div class="title-wrap">
                                                <h3 class="title"><span class="cm-line-break cm-cursive">Experience the</span>Taste of purity</h3>
                                                <a href="<?php echo base_url();?>shop" class="btn btn-default hidden-xs">Buy products</a>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <ul class="quality-list row">
                                                <li class="item col-md-4" data-aos="fade-up" data-aos-duration="500" data-aos-delay="500">
                                                    <div class="mod-icon-text">
                                                        <span class="icon icon-award"></span>
                                                        <span class="text">Real Flavour Real Taste</span>
                                                    </div>
                                                </li>
                                                <li class="item col-md-4" data-aos="fade-up" data-aos-duration="500" data-aos-delay="600">
                                                    <div class="mod-icon-text">
                                                        <span class="icon icon-nowater"></span>
                                                        <span class="text">No added colours</span>
                                                    </div>
                                                </li>
                                                <li class="item col-md-4" data-aos="fade-up" data-aos-duration="500" data-aos-delay="700">
                                                    <div class="mod-icon-text">
                                                        <span class="icon icon-lab"></span>
                                                        <span class="text">Lab Tested</span>
                                                    </div>
                                                </li>
                                            </ul>
                                            <div class="quality-action-wrap visible-xs">
                                                <a href="<?php echo base_url();?>shop" class="btn btn-default ">Buy products</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="bs-video-block bs-pos-ele wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.5s">
                <div class="video-info typ-home">
                    <div class="img-wrap js-addto-img">
                        <img src="assets/images/home-video.jpg" data-mbsrc="assets/images/home-video-mb.jpg" alt="banner" class="addto-img">
                    </div>
                    <div class="info-wrap cm-hidden">
                        <h3 class="title cm-cursive">The process</h3>
                        <button class="play-btn js-btn-video" data-src="https://www.youtube.com/embed/l1dDw4wk1E8" data-toggle="modal" data-target="#videoModal"></button>
                        <span class="meta">Watch the process to make great spice</span>
                    </div>
                </div>
                <span class="t-l element ele7"></span>
                <span class="b-r element ele8"></span>
            </div>
            <section>
                <div class="bs-sec bs-pos-ele wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.4s">
                    <div class="container">
                        <div class="sec-cont">
                            <div class="bs-testimonials">
                                <div class="row">
                                    <div class="col-md-4 col-md-offset-1">
                                        <h2 class="sec-title">
                                            <span class="cm-line-break">Customer</span>
                                            <span class="cm-line-break">Speaks</span>
                                        </h2>
                                    </div>
                                    <div class="col-md-66">
                                        <div class="swiper-container">
                                            <div class="swiper-wrapper">
                                            	
                                                <div class="swiper-slide">
                                                    <!-- <div class="img-wrap js-addto-img">
                                                        <img src="assets/images/testimonial1.png" alt="testi1" class="addto-img">
                                                    </div> -->
                                                    <div class="desc">
                                                        <p>I don’t know much about spices. I would just pick up any packet and use it in my cooking. Then a friend gifted me a few packets from Rasoi Tatva. My food instantly tasted so good! I would recommend it to everyone.</p>
                                                    </div>
                                                    <strong class="meta-info">Saisha B. (Housewife)</strong>
                                                </div>

                                                <div class="swiper-slide">
                                                    <!-- <div class="img-wrap js-addto-img">
                                                        <img src="assets/images/testimonial1.png" alt="testi1" class="addto-img">
                                                    </div> -->
                                                    <div class="desc">
                                                        <p>The red chilli powder from Rasoi Tatva is superb. Has a lot of flavour and gives a lovely colour as well. And it is free flowing so very easy to use!</p>
                                                    </div>
                                                </div>

                                                <div class="swiper-slide">
                                                    <!-- <div class="img-wrap js-addto-img">
                                                        <img src="assets/images/testimonial1.png" alt="testi1" class="addto-img">
                                                    </div> -->
                                                    <div class="desc">
                                                        <p>I ordered the garam masala to try it. It blends with everything and makes the food so yum! Cannot wait to order more stuff.</p>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="swiper-pagination visible-xs"></div>
                                    </div>
                                </div>
                                <div class="swiper-btn swiper-button-prev hidden-xs"></div>
                                <div class="swiper-btn swiper-button-next hidden-xs"></div>
                            </div>
                        </div>
                    </div>
                    <span class="b-l element ele9"></span>
                </div>
            </section>
            <section>
                <div class="bs-sec wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.3s">
                    <div class="container">
                        <div class="sec-head">
                            <h2 class="sec-title">
                                <span class="cm-cursive cm-line-break">Indian various</span>
                                <span class="cm-line-break">World Of Spices</span>
                            </h2>
                        </div>
                        <div class="sec-cont">
                            <div class="bs-world-spice" id="wrspiceSlider">
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <div class="img-wrap">
                                                <img src="assets/images/white-pepper.png" alt="White Pepper" width="230px" height="230px">
                                            </div>
                                            <h3 class="title">White Pepper</h3>
                                            <p class="desc">Used in preparations where a lighter flavour of pepper is preferred, while keeping the health benefits intact</p>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="img-wrap">
                                                <img src="assets/images/turmeric-powder.png" alt="Turmeric Powder" width="230px" height="230px">
                                            </div>
                                            <h3 class="title">Turmeric Powder</h3>
                                            <p class="desc">An essential spice across India. Used in cooking as well as most auspicious occasions. No puja is complete without offering this golden root to the Almighty.</p>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="img-wrap">
                                                <img src="assets/images/mustard-seeds.png" alt="Mustard Seeds" width="230px" height="230px">
                                            </div>
                                            <h3 class="title">Mustard Seeds</h3>
                                            <p class="desc">A must for most pickle preparations. Help in the growth of thick, lustrous hair as well. Used most commonly in the Indian Tadka!</p>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="img-wrap">
                                                <img src="assets/images/meethi-seeds.png" alt="Methi Seeds" width="230px" height="230px">
                                            </div>
                                            <h3 class="title">Methi Seeds</h3>
                                            <p class="desc">Loaded with medicinal properties. Has been used for years for lustrous hair and healthy bones.</p>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="img-wrap">
                                                <img src="assets/images/kashmiri-chillipowder.png" alt="Kashmiri Chilli Powder" width="230px" height="230px">
                                            </div>
                                            <h3 class="title">Kashmiri Chili Powder</h3>
                                            <p class="desc">Those who like their food brightly coloured, prefer this variety. It gives vibrance with a lot less heat.</p>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="img-wrap">
                                                <img src="assets/images/ginger-powder.png" alt="Ginger Powder" width="230px" height="230px">
                                            </div>
                                            <h3 class="title">Ginger Powder</h3>
                                            <p class="desc">Versatile and convenient in its use. Used in most herbal preparation and great for immunity building and digestion.</p>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="img-wrap">
                                                <img src="assets/images/fennel-seeds.png" alt="Fennel Seeds" width="230px" height="230px">
                                            </div>
                                            <h3 class="title">Fennel Seeds</h3>
                                            <p class="desc">Used in food preparation and as a mouth freshner. Excellent digestive with a fresh flavour.</p>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="img-wrap">
                                                <img src="assets/images/cumin-seeds.png" alt="Cumin Seeds" width="230px" height="230px">
                                            </div>
                                            <h3 class="title">Cumin Seeds</h3>
                                            <p class="desc">Another potent coolant that is widely used across the globe. Temper your food with cumin seeds and see it notch up in terms of aroma and taste.</p>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="img-wrap">
                                                <img src="assets/images/cardamom.png" alt="Cardamom" width="230px" height="230px">
                                            </div>
                                            <h3 class="title">Cardamom</h3>
                                            <p class="desc">Queen of spices! Unmistakable aroma and flavour. Used to prepare a variety of sweet and savory dishes.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-navigation">
                                    <div class="swiper-button-prev"><span class="icon icon-left-arrow-c"></span></div>
                                    <div class="swiper-button-next"> <span class="icon icon-right-arrow-c"></span></div>
                                </div>
                                <a href="<?php echo base_url();?>shop" class="btn btn-default">Explore All Spices</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="bs-sec bs-social-grid sec-last-child wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.5s">
                    <div class="container">
                        <div class="sec-head">
                            <h2 class="sec-title">
                                <span class="cm-line-break text-sm">Follow us on instagram</span>
                                <span class="cm-line-break">@rasoitatva</span>
                            </h2>
                        </div>
                        <div class="sec-cont">
                            <ul class="row sg-wrap">
                            <?php 
                            //print_r($insta_data);exit;
                            for($b=0;$b<count($insta_data['data']);$b++){
                                        $post = $insta_data['data'][$b];
                                        $pic_src=str_replace("http://", "https://", $post['media_url']);
                                         $pic_text=$post['caption'];
                                         $pic_link=$post['permalink'];
                                        ?>
                                <li class="col-md-4 sg-item">
                                    <a href="<?php echo $pic_link;?>" target="_blank" class="sg-link">
                                        <img src="<?php echo $pic_src;?>" alt="<?php echo $pic_text;?>" class="sg-img" width="370px" height="370px">
                                    </a>
                                </li>
                                <?php } ?>
                                 <!-- <li class="col-md-4 js-addto-img sg-item">
                                    <a href="#" class="sg-link">
                                        <img src="assets/images/dummy-insta2.jpg" alt="wall 2" class="addto-img">
                                    </a>
                                </li>
                                <li class="col-md-4 js-addto-img sg-item">
                                    <a href="#" class="sg-link">
                                        <img src="assets/images/dummy-insta2.jpg" alt="wall 2" class="addto-img">
                                    </a>
                                </li>
                                <li class="col-md-4 js-addto-img sg-item">
                                    <a href="#" class="sg-link">
                                        <img src="assets/images/dummy-insta3.jpg" alt="wall 3" class="addto-img">
                                    </a>
                                </li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>

    <div class="bs-loader active">
        <div class="loader-wrap">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 54 40">
                <defs>
                    <path id="prefix__a" d="M0 54L54 54 54 0 0 0z"/>
                </defs>
                <g fill="none" fill-rule="evenodd">
                    <path class="fill" fill="#000" d="M5.42 36.934l-.14-.386-.362.195c-.594.32-1.165.352-1.732.098-.72-.562-1.052-1.342-1.015-2.395V30.07h2.656v-1.941H2.16V24H0v10.684c-.014.876.191 1.69.611 2.428.701 1.183 1.678 1.818 2.909 1.885.044.002.088.003.133.003.596 0 1.272-.142 2.01-.422l.337-.13-.136-.334c-.111-.277-.261-.673-.444-1.18M13.981 29.03c1.063 0 1.944.382 2.692 1.164.29.306.523.632.69.968.233.77.327 1.587.276 2.427-1.01 2.652-2.627 3.683-4.965 3.184-.928-.462-1.593-1.023-1.976-1.67-.393-.661-.591-1.356-.591-2.07 0-1.118.376-2.045 1.15-2.834.767-.787 1.66-1.17 2.724-1.17m4.399-.128C17.219 27.64 15.739 27 13.981 27s-3.239.643-4.401 1.913C8.53 30.07 8 31.448 8 33.015c0 1.575.56 2.97 1.667 4.15 1.111 1.184 2.56 1.785 4.337 1.785h.006c1.668-.139 2.95-.694 3.825-1.653V39h2.086l.06-.264c.016-.065.029-.125.01-5.721 0-1.582-.542-2.965-1.611-4.114M28.419 36.935l-.14-.387-.362.195c-.595.32-1.165.352-1.73.098-.72-.562-1.052-1.342-1.016-2.395V30.07h2.656v-1.941H25.16V24H23v10.683c-.014.877.19 1.691.612 2.429.7 1.183 1.676 1.818 2.907 1.885.044.002.088.003.133.003.597 0 1.273-.142 2.008-.422l.34-.13-.136-.335c-.114-.276-.264-.673-.445-1.178M34.009 35.71L30.384 28 28 28 33.651 40 34.383 40 40 28 37.607 28zM47.98 29.03c1.064 0 1.945.382 2.693 1.164.29.306.523.632.69.968.233.77.326 1.587.276 2.427-1.01 2.652-2.628 3.683-4.966 3.184-.928-.462-1.594-1.023-1.977-1.67-.39-.661-.589-1.357-.589-2.07 0-1.118.375-2.045 1.147-2.834.77-.787 1.662-1.17 2.726-1.17m6.01 3.986c0-1.582-.543-2.965-1.61-4.114C51.218 27.64 49.738 27 47.98 27c-1.759 0-3.238.643-4.403 1.913C42.532 30.07 42 31.448 42 33.015c0 1.576.56 2.972 1.667 4.15 1.11 1.184 2.56 1.785 4.336 1.785h.004c1.67-.139 2.952-.692 3.827-1.653V39h2.087l.06-.264c.015-.065.029-.125.01-5.721M6.699 5.532C6.156 5.18 5.633 5 5.144 5c-.656 0-1.434.353-2.38 1.08-.931.714-1.68 1.844-2.217 3.338-.236.554-.419 1.275-.543 2.142L0 20h2.285v-5.077c0-2.112.094-3.564.273-4.294.244-.824.691-1.598 1.33-2.302.583-.64 1.142-.844 1.71-.61l.259.106L7 5.728l-.301-.196zM15.474 7.445c1.356 0 2.479.485 3.432 1.485.37.389.666.804.881 1.235.296.98.415 2.018.352 3.087-1.287 3.374-3.345 4.69-6.328 4.048-1.182-.583-2.029-1.298-2.518-2.124-.498-.842-.751-1.728-.751-2.634 0-1.423.478-2.603 1.46-3.61.98-1 2.117-1.487 3.472-1.487m-5.39 10.26c1.386 1.48 3.2 2.231 5.417 2.231h.004c2.15-.179 3.784-.91 4.878-2.172V20h2.527l.068-.29c.017-.076.035-.153.008-7.192 0-1.976-.676-3.709-2.011-5.142C19.522 5.8 17.673 5 15.475 5c-2.197 0-4.048.804-5.503 2.39C8.664 8.833 8 10.56 8 12.518c0 1.97.701 3.714 2.084 5.187"/>
                    <path fill="#000" class="fill" d="M31.86 15.654c0-.867-.251-1.66-.747-2.363-.494-.68-1.392-1.35-2.745-2.053-1-.516-1.654-.97-1.955-1.357-.277-.329-.413-.654-.413-.993 0-.406.158-.75.48-1.05.332-.308.723-.457 1.194-.457.768 0 1.59.423 2.446 1.256l.267.26L32 7.194l-.267-.26C30.412 5.652 29.07 5 27.748 5c-1.145 0-2.116.378-2.882 1.125-.768.75-1.158 1.704-1.158 2.837 0 .869.244 1.651.727 2.316.47.685 1.325 1.336 2.605 1.988 1.338.695 1.853 1.126 2.049 1.364.264.323.392.683.392 1.098 0 .498-.2.923-.608 1.302-.418.388-.912.576-1.505.576-.872 0-1.72-.464-2.523-1.383l-.278-.317L23 17.705l.184.243c.491.648 1.124 1.157 1.88 1.516.75.355 1.552.536 2.377.536 1.254 0 2.314-.426 3.153-1.265.84-.84 1.266-1.877 1.266-3.08"/>
                    <mask id="prefix__b" fill="#fff">
                        <use xlink:href="#prefix__a"/>
                    </mask>
                    <path fill="#000" class="fill" d="M51 20L53 20 53 6 51 6zM51.994 4c.547 0 1.023-.198 1.415-.59.393-.392.591-.867.591-1.417 0-.548-.198-1.023-.593-1.41C53.016.195 52.54 0 51.994 0c-.547 0-1.022.197-1.41.586-.387.386-.584.86-.584 1.407s.196 1.022.583 1.414c.387.394.862.593 1.411.593M46.402 9.075h.001l-.105.172c.63.928.997 2.047.997 3.253 0 3.2-2.595 5.795-5.794 5.795-3.201 0-5.797-2.595-5.797-5.795 0-3.201 2.596-5.796 5.797-5.796.637 0 1.25.104 1.825.294.395-.461.785-.921 1.174-1.374-.919-.402-1.933-.624-3-.624-4.142 0-7.5 3.357-7.5 7.5s3.358 7.5 7.5 7.5c4.141 0 7.5-3.357 7.5-7.5 0-1.834-.659-3.513-1.751-4.818-.284.47-.567.933-.847 1.393" mask="url(#prefix__b)"/>
                    <path fill="#000" class="fill" d="M43.485 15.596l.024-.015.023.013.015-.001.133.094-.034.038.002.023-.022.015c-1.225.886-3.03.912-4.202.063l-.105-.08.053-.082c1.399.743 2.782.72 4.113-.068zM38.01 11.22l-.012.027-.025.003-.01.01-.167.003.006-.05-.015-.019.009-.025c.471-1.437 1.916-2.52 3.362-2.52.046 0 .09.002.135.003l.005.097c-1.57.221-2.676 1.054-3.288 2.471zm6.89-.352l-.018.028-.51.834c-.04.075-.09.16-.147.243l-.857 1.403-.082.14-.085.135c-.053.082-.101.162-.145.243-.283.536-.764.81-1.43.818h-.023c-.924 0-1.73-.564-2.056-1.436-.14-.379-.164-.737-.068-1.068.007-.044.022-.09.045-.134l.053-.12.043-.087.026.003c.45-.72 1.088-1.347 1.16-1.415l.147-.175c.293-.35.599-.711.9-1.066l.888-1.042C42.346 8.061 41.929 8 41.5 8c-2.486 0-4.5 2.014-4.5 4.5 0 2.484 2.014 4.5 4.5 4.5 2.485 0 4.5-2.016 4.5-4.5 0-.862-.243-1.665-.662-2.35l-.438.719z" mask="url(#prefix__b)"/>
                    <path fill="#000" class="fill" d="M47.69 5.227c-.018-.017-.034-.034-.03-.058l-.002-.001v-.003c-.04 0-.054-.028-.065-.056h-.003v-.005c-.362-.36-.8-.622-1.282-.828-.138-.06-.22-.06-.356-.004.073-.122.202-.157.401-.095.41.126.743.354 1.023.645.083.085.197.154.213.282l.003.001.001.004c.045-.007.05.028.064.056h.003v.004c.01.019.019.04.029.056l.001.002.004.003-.004-.003zM41.65 9.756c1.313-1.576 2.744-3.071 4.217-4.538-1.28 1.598-2.707 3.095-4.217 4.538zm6.286-4.674c-.176-.4-.522-.656-.912-.864-.26-.14-.544-.235-.857-.216-.239.015-.414.136-.41.327.003.203-.103.35-.235.49-.373.397-.746.79-1.12 1.186-.99 1.052-1.98 2.102-2.968 3.155-.359.381-.713.764-1.068 1.147-.017.015-.737.645-1.204 1.35l-.018-.002c-.018.036-.035.073-.051.109l-.001.002c-.02.032-.032.063-.035.092-.099.302-.06.606.063.906.312.76 1.078 1.244 1.97 1.236.569-.005 1.04-.197 1.322-.682.07-.122.157-.236.236-.355l.149-.225.806-1.2c.057-.068.102-.142.147-.217l.522-.775.016-.02.59-.881.025-.035c.748-1.116 1.497-2.232 2.244-3.35.154-.23.32-.443.588-.584.28-.149.324-.316.201-.594z" mask="url(#prefix__b)"/>
                </g>
            </svg>
        </div>
    </div>
    <?php $this->load->view('include/footer.php'); ?>
    <?php $this->load->view('include/footer_2.php'); ?>
    <?php $this->load->view('include/footer_js.php'); ?>
    <?php $this->load->view('include/commonsite_js.php'); ?>
    </body>

</html>
