<footer>
        <div class="bs-footer">
    <div class="footer-top">
        <ul class="grid">
            <li class="item">
                <ul class="quick-links">
                    <li><a href="<?php echo base_url();?>">home</a></li>
                    <li><a href="<?php echo base_url();?>about-us">about us</a></li>
                    <li><a href="<?php echo base_url();?>contact-us">contact us</a></li>
                </ul>
            </li>
            <li class="item">
                <ul class="quick-links">
                    <li><a href="<?php echo base_url();?>shop">shop all spices</a></li>
                    <li><a href="<?php echo base_url();?>shop#blends" class="js-product-type-link">Blends</a></li>
                    <li><a href="<?php echo base_url();?>shop#ground-spice" class="js-product-type-link">Ground Spices</a></li>
                    <li><a href="<?php echo base_url();?>shop#whole-spice" class="js-product-type-link">Whole Spices</a></li>
                </ul>
            </li>
            <li class="item">
                <ul class="quick-links">
                    <li><a href="<?php echo base_url();?>profile">My account</a></li>
                    <li><a href="<?php echo base_url();?>orders">my orders</a></li>
                    <li><a href="<?php echo base_url();?>orders">track order</a></li>
                </ul>
            </li>
            <li class="item">
                <ul class="quick-links">
                    <li><a href="<?php echo base_url();?>terms-conditions">terms &amp; conditions</a></li>
                    <li><a href="<?php echo base_url();?>privacy-policy">privacy policy</a></li>
                    <li><a href="<?php echo base_url();?>disclaimer">Disclaimer</a></li>
                    <!-- <li><a href="<?php echo base_url();?>faqs">FAQ's</a></li> -->
                </ul>
            </li>
            <li class="item">
                <ul class="quick-links">
                    <li>
                        <h3 class="title">contact</h3>
                    </li>
                    <li><a href="mailto:customercare@rasoitatva.com" class="cm-text-initial">customercare@rasoitatva.com</a></li>
                    <li><a href="tel:+917538993333">+91-7538993333</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="footer-mid">
        <!-- <div class="lhs">
        <form id="newsletter_form" action="#">
            <div class="bs-newsletter">
                <h3 class="nl-title">Subscribe to newsletter</h3>
                <div class="nl-input-group">
                    <input type="text" name="subscribe_email" class="form-control" placeholder="Email ID">
                    <button type="submit" id="btnSub" class="nl-btn">Submit</button>
                </div>
            </div>
            </form>
        </div> -->
        <div class="rhs">
            <ul class="social-media-wrap">
                <li class="item">
                    <a href="https://www.facebook.com/rasoitatva/" target="_blank" class="social-icon icon-fb"></a>
                </li>
                <li class="item">
                    <a href="https://www.instagram.com/rasoitatva/" target="_blank" class="social-icon icon-insta"></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="lhs">
            <div class="support-wrap">
                <ul class="img-list">
                    <li class="item">
                        <span class="icon icon-visa"></span>
                    </li>
                    <li class="item">
                        <span class="icon icon-mastercard"></span>
                    </li>
                    <li class="item">
                        <span class="icon icon-paytm"></span>
                    </li>
                    <li class="item">
                        <span class="icon icon-upi"></span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="rhs">
            <div class="copyrights">rasoi tatva &copy; <span class="js-current-year"></span>. All rights reserved.</div>
        </div>
    </div>
</div>
    </footer>