<span class="cm-overlay"></span>
    
    <!-- Toaster start -->
    <div class="bs-toaster">
    <p class="toaster-info">Item removed from bag. To add item back to yhe bag click on undo.</p>
    <!--button type="button" class="btn btn-link js-close-toaster">undo</button-->
</div>

<div class="bs-toaster typ-sm">
    <p class="toaster-info">Item added to cart</p>
    <!--button type="button" class="btn btn-link js-close-toaster">view cart</button-->
</div>
    <!-- Toaster end -->
    <!-- Cart flyout start -->
<div class="bs-cart-fly cm-loader">
    <div class="cart-head">
        <h2 class="title">In Cart</h2>
        <span class="count" id="cart_fly_count">(<?php echo count($cart);?>)</span>
        <button class="close-btn close_cart_fly" id="" ><span class="icon icon-cross" ></span></button>
    </div>
    <div class="cart-body">
    <?php if(count($cart)>0){?>
        <ul class="cart-list">
        <?php 

            $p = 0;
            foreach($cart as $cart){
            ?>
            <li class="cart-item" id="cart_item_fly_<?php echo $cart['key'];?>">
                <div class="mod-cart-pd">
                    <div class="pd-img">
                        <img src="<?php echo $product_fly[$p]['images'][0]['src'];?>" alt="<?php echo $product_fly[$p]['name'];?>">
                    </div>
                    <div class="pd-detail">
                        <div class="title-wrap">
                            <h3 class="pd-title"><?php echo $product_fly[$p]['name'];?></h3>
                            <div class="bs-amount">
                            <?php  $product_price = $cart['line_total']/$cart['quantity']; ?>
                                <strong class="amount-val">Rs.<?php echo number_format($product_price,2);?></strong>
                            </div>
                        </div>
                       
                        <div class="act-wrap">
                            <div class="bs-custom-select typ-box">
                                <?php foreach($product_fly[$p]['attributes'][0]['options'] as $key=>$value) { 
                                    if($product_fly[$p]['variations'][$key]==$cart['variation_id']){
                                    ?>
                                    <strong class="amount-qty"><?php echo $value;?> gm</strong>
                                <?php } 
                                    } ?>
                            </div>
                            <div class="bs-val-variation">
                                <div class="input-group">
                                    <button class="decre-btn input-btn js-minus" onclick="update_to_cart(this)" data-weight="<?php echo $cart['weight'];?>" data-product-id="<?php echo $product_fly[$p]['id'];?>" data-var-id="<?php echo $cart['variation_id'];?>"><span class="icon icon-minus-c"></span></button>
                                    <input type="text" id="count" class="form-control count js-count" value="<?php echo $cart['quantity'];?>" min="1" max="5" readonly>
                                    <button class="incre-btn input-btn js-plus" onclick="update_to_cart(this)" data-weight="<?php echo $cart['weight'];?>" data-product-id="<?php echo $product_fly[$p]['id'];?>" data-var-id="<?php echo $cart['variation_id'];?>" data-max="5"><span class="icon icon-plus-c"></span></button>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-link del-btn js-product-remove" onclick="remove_cart_item('<?php echo $cart['key'];?>')" id="<?php echo $cart['key'];?>"><span class="icon icon-trash"></span></button>
                    </div>
                </div>
            </li>
            <?php 
                $p++;
            } ?>
        </ul>
        <?php }else{ ?>
        <div class="bs-message">
            <div class="msg-wrap">
                <div class="ty-head">
                    <span class="icon icon-bag"></span>
                </div>
                <div class="ty-cont">
                    <p>No Products in shop</p>
                    <a href="product-listing.html" type="button" class="btn btn-default">return to shop</a>
                </div>
            </div>
        </div>
         <?php } ?>
    </div>
    <div class="cart-foot">
        <div class="mod-data-list">
            <dl>
                <dt class="data-label">Cart subtotal</dt>

                <dd class="data-value"><span class="amount-val" >&#8377; <span id="cart_fly_subtotal"><?php echo $cart_total['subtotal'];?></span></span></dd>

            </dl>
        </div>
        <a href="<?php echo base_url();?>cart" class="btn btn-default">Checkout</a>
    </div>
</div>
    <!-- Cart flyout end -->
    <div class="bs-modal modal fade mb-large" id="productModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body " id="product_summary_body">
                
            </div>
        </div>
    </div>
</div>
    <div class="bs-modal modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="bs-video">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <iframe class="js-video-frame" width="100%" height="400" src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="bs-toaster " id="normal_toaster" >
    <p class="toaster-info" id="toaster-text">Item removed from bag. To add item back to yhe bag click on undo.</p>
    <!--button type="button" class="btn btn-link js-close-toaster">Ok</button-->
</div>

<div class="bs-toaster " id="cart_toast" >
    <p class="toaster-info" id="toaster-text-cart">Product added to Cart</p>
    <!--button type="button" class="btn btn-link js-close-toaster">Ok</button-->
</div>



<!-- <div class="modal typ-message fade" role="dialog" id="confirm_remove" tabindex="-1" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Please confirm</h2>
                </div>
                <div class="modal-body">
                    <div class="bs-static-cont">
                        <p>
                           Are you sure you want to remove this item from wishlist?
                        </p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="cancel_btn" class="btn btn-secondary-clr" data-dismiss="modal">cancel</button>
                    <button type="button" id="proceed_btn" data-product-id="" class="btn btn-default">proceed</button>
                </div>
            </div>
        </div>
    </div> -->

    <!--div class="bs-modal typ-message modal fade" id="message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bs-video">
                        <div class="bs-message typ-static">
                            <div class="msg-wrap">
                                <div class="ty-head">
                                    <span class="icon icon-done"></span>
                                    <p class="ty-desc">You have registered successfully.</p>
                                </div>
                                <div class="ty-cont">
                                    <button type="button" id="cancel_btn" class="btn btn-outline" data-dismiss="modal">Ok</button>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div-->

    <div class="bs-modal typ-message modal fade" id="message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bs-video">
                        <div class="bs-message typ-static">
                            <div class="msg-wrap">
                                <div class="ty-head">
                                    <div class="img-wrap">
                                        <img src="assets/images/success.gif" alt="success">
                                    </div>
                                    <p class="ty-desc">Registration is Successful</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="bs-modal typ-message modal fade" id="forgot-message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bs-video">
                        <div class="bs-message typ-static">
                            <div class="msg-wrap">
                                <div class="ty-head">
                                    <div class="img-wrap">
                                        <img src="assets/images/success.gif" alt="success">
                                    </div>
                                    <p class="ty-desc">Password reset link sent successfully on registered email id.</p>
                                </div>
                                <!-- <div class="ty-cont">
                                    <button type="button" id="cancel_btn" class="btn btn-outline" data-dismiss="modal">Ok</button>
                                </div> -->
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>


    

    <div class="bs-modal typ-message modal fade" id="forgot-message-error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bs-video">
                        <div class="bs-message typ-static">
                            <div class="msg-wrap">
                                <div class="ty-head">
                                    <!-- <span class="icon icon-remove"></span> -->
                                    <span class="icon icon-address-error"></span>
                                    <p class="ty-desc">Email Id does not exist.</p>
                                </div>
                                <div class="ty-cont">
                                    <button type="button" id="cancel_btn" class="btn btn-outline" data-dismiss="modal">Ok</button>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="bs-modal typ-message modal fade" id="confirm_remove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bs-video">
                        <div class="bs-message typ-static">
                            <div class="msg-wrap">
                                <div class="ty-head">
                                    <span class="icon icon-remove"></span>
                                    <p class="ty-desc">Are you sure you want to remove this item from wishlist?</p>
                                </div>
                                <div class="ty-cont">
                                    <button type="button" id="cancel_btn" class="btn btn-outline" data-dismiss="modal">Cancel</button>
                                    <button type="button" id="proceed_btn" data-product-id="" class="btn btn-default">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bs-modal typ-message modal fade" id="confirm_remove_order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bs-video">
                        <div class="bs-message typ-static">
                            <div class="msg-wrap">
                                <div class="ty-head">
                                    <span class="icon icon-remove"></span>
                                    <p class="ty-desc">Are you sure you want to cancel this order?</p>
                                </div>
                                <div class="ty-cont">
                                    <button type="button" id="cancel_btn" class="btn btn-outline" data-dismiss="modal">Cancel</button>
                                    <button type="button" id="proceed_cancel_btn" data-product-id="" class="btn btn-default">Proceed</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bs-modal typ-message modal fade" id="confirm_remove_cart_item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bs-video">
                        <div class="bs-message typ-static">
                            <div class="msg-wrap">
                                <div class="ty-head">
                                    <span class="icon icon-remove"></span>
                                    <p class="ty-desc">Are you sure you want to remove this item?</p>
                                </div>
                                <div class="ty-cont">
                                    <button type="button" id="cancel_btn_cart_item" class="btn btn-outline" data-dismiss="modal">Cancel</button>
                                    <button type="button" id="proceed_btn_cart_item" data-refresh-state="" data-cart-id="" class="btn btn-default">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>