
<head>
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-CNCM8GGR1G"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-CNCM8GGR1G');
    </script>

    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MFS7M48');</script>


    <title><?php echo $meta_title;?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="<?php echo $meta_desc;?>">
    <meta name="keyword" content="<?php echo $meta_keyword;?>">

    <meta name="google-site-verification" content="5xE8Q5YZMhz-R8lYGHx69XiKA41zW6b6nGN2Q108T6M">
    <link rel="canonical" href="<?php echo "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>" />

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="<?php echo "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>">
    <meta property="og:title" content="<?php echo $meta_title;?>">
    <meta property="og:description" content="<?php echo $meta_desc;?>">
    <meta property="og:image" content="<?php echo base_url();?>assets/images/logo-og.png">
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="<?php echo "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>">
    <meta property="twitter:title" content="<?php echo $meta_title;?>">
    <meta property="twitter:description" content="<?php echo $meta_desc;?>">
    <meta property="twitter:image" content="<?php echo base_url();?>assets/images/logo-og.png">
    <!-- Schema -->
    <meta itemprop="name" content="">
    <html itemscope itemtype="<?php echo "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>">
    <meta itemprop="description" content="<?php echo $meta_desc;?>">
    <meta itemprop="image" content="<?php echo base_url();?>assets/images/logo-og.png">
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url();?>assets/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url();?>assets/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/images/favicon-16x16.png">

    

    <!-- css group start -->
    
    <!-- <link rel="icon" href="<?php //echo base_url();?>assets/images/favicon.ico" type="image/x-icon"> 
<link href="https://fonts.googleapis.com/css?family=Italianno|Nunito:300,400,600,700&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php // base_url();?>assets/css/libraries.css">
<link rel="stylesheet" type="text/css" href="<?php //echo base_url();?>assets/css/custom.css">-->
    <!-- css group end -->


    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
    <link rel="preconnect" href="https://www.instagram.com">
    <link rel="dns-prefetch" href="https://www.instagram.com">
    <link rel="preload" href="<?php echo base_url();?>assets/fonts/icomoon.woff" as="font" type="font/woff" crossorigin="anonymous">
    <link rel="preload" href="https://fonts.googleapis.com/css?family=Italianno|Nunito:300,400,600,700&display=swap" as="style">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Italianno|Nunito:300,400,600,700&display=swap">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/libraries.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/custom.css">

    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '151029989833019');
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=151029989833019&ev=PageView&noscript=1"
    /></noscript>

</head>
