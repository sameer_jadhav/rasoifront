<?php 

load_wishlist();
load_cart();
?>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MFS7M48"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<h1 class="cm-not-in-page"><?php echo $meta_h1;?></h1>
<header>
        <div class="bs-header">
    <a href="<?php echo base_url();?>" class="logo">
        <img src="<?php echo base_url();?>assets/images/logo-white.svg" alt="logo">
    </a>
    <button class="menu-btn">
        <span class="line"></span>
        <span class="line"></span>
        <span class="line"></span>
    </button>
    <div class="nav-wrap">
        <nav>
            <ul class="nav-list">
                <li class="nav-item">
                    <a href="<?php echo base_url();?>shop" class="nav-link">Shop</a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo base_url();?>about-us" class="nav-link">About Us</a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo base_url();?>contact-us" class="nav-link">Contact Us</a>
                </li>
                <?php 
            $user_sess = check_is_login(); 
            // print_r($user_sess);exit;
            if($user_sess==false){ ?>
                <li class="nav-item visible-xs">
                    <a href="<?php echo base_url();?>login" class="nav-link">Login</a>
                </li>
                <li class="nav-item visible-xs">
                    <a href="<?php echo base_url();?>signup" class="nav-link">Sign Up</a>
                </li>
                <?php }else{ ?>
                <li class="nav-item visible-xs">
                    <a href="<?php echo base_url()."profile";?>" class="nav-link">Profile</a>
                </li>
                <li class="nav-item visible-xs">
                    <a href="<?php echo base_url()."orders";?>" class="nav-link">My Orders</a>
                </li>
                <li class="nav-item visible-xs">
                    <a href="<?php echo base_url()."manage-address";?>" class="nav-link">Saved Addresses</a>
                </li>
                <li class="nav-item visible-xs">
                    <a href="<?php echo base_url()."wishlist";?>" class="nav-link">Wishlist</a>
                </li>
                <li class="nav-item visible-xs">
                    <a href="<?php echo base_url()."logout";?>" class="nav-link">Logout</a>
                </li>
                <?php } ?>
            </ul>
        </nav>
        <div class="mod-user">
        <?php 
            $user_sess = check_is_login(); 
            // print_r($user_sess);exit;
            if($user_sess==false){ ?>
                <div class="bs-dropdown hidden-xs">
                    <button class="nav-icon mod-cart">
                        <span class="icon icon-user"></span>
                    </button>
                    
                    <div class="dropdown-menu">
                        <h3 class="dropdown-title">Your account</h3>
                        <p class="dropdown-info">Access account and manage orders</p>
                        
                        <div class="dopdown-action-wrap">
                            <a href="<?php echo base_url();?>login" class="btn btn-default">login</a>
                            <a href="<?php echo base_url();?>signup" class="btn btn-default">sign up</a>
                        </div>
                        
                    </div>
                </div>
            <?php }else{ ?>

                <div class="bs-dropdown hidden-xs">
                    <a href="#" class="link">Hi, <span class="user"><?php echo $user_sess['user_display_name'];?></span></a>
                    <div class="dropdown-menu">
                        <ul class="user-nav">
                            <li class="user-item">
                                <a href="<?php echo base_url()."profile";?>" class="user-link">Profile</a>
                            </li>
                            <li class="user-item">
                                <a href="<?php echo base_url()."orders";?>" class="user-link">My Orders</a>
                            </li>
                            <li class="user-item">
                                <a href="<?php echo base_url()."manage-address";?>" class="user-link">Saved Addresses</a>
                            </li>
                            <li class="user-item">
                                <a href="<?php echo base_url()."wishlist";?>" class="user-link">Wishlist</a>
                            </li>
                            <li class="user-item">
                                <a href="<?php echo base_url()."logout";?>" class="user-link">Logout</a>
                            </li>
                        </ul>
                    </div>
                </div>

            <?php } ?>

        </div>
        <?php 
                    //$cart_count = get_cart_count
                     $class_name = "";
                    if(get_cart_count() >0){
                        $class_name = "add";
                    }
                     ?>
        <div class="nav-icon-list">
            <ul class="list">
                <li class="item">
                    <a href="<?php echo base_url();?>cart" class="nav-icon mod-cart <?php echo $class_name;?>">
                        <span class="icon icon-cart"></span>
                        <span class="count cart-count"><?php echo get_cart_count();?></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
</header>
