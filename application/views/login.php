
<!DOCTYPE html>
<html>

<?php $this->load->view('include/head.php'); ?>

<body>
    
<?php $this->load->view('include/header.php'); ?>

    <main>
        <div class="bs-main lyt-login">
            <div class="bs-login">
                <div class="head-wrap">
                    <h1 class="title">Log In</h1>
                    <p>Log into your account in just a few steps.</p>
                </div>
                <div class="cont-wrap">
                    <form id="form" action="#" class="">
                        <div class="bs-form">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group ">
                                        <label for="" class="input-label">Email / Mobile No.</label>
                                        <input type="text" name="username"  required=""class="form-control" placeholder="Enter your Mobile No./Email Id">
                                        <!-- <span class="error-msg">Lorem Ipsum</span> -->
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group ">
                                        <label for="" class="input-label">Password</label>
                                        <input type="password" name="password" required="" class="form-control" placeholder="Enter your Password">
                                        <!-- <span class="error-msg">Lorem Ipsum</span> -->
                                    </div>
                                </div>
                                <div class="col-md-12 hide" id="error-div">
                                    <span id="error-text" class="error-text cm-error-text"></span>
                                </div>
                            </div>
                            <div class="form-action">
                                <button type="submit" id="btnLogin" type="button" class="btn btn-default visible-xs btnLogin">Log In</button>
                                <button type="submit" id="btnLogin" type="button" class="btn btn-default hidden-xs btnLogin">Log In</button>
                                <a href="<?php echo base_url();?>signup" class="btn btn-outline">Create New Account</a>
                            </div>
                             <div class="form-action">
                                <a href="<?php echo base_url();?>forgot-password" class="btn btn-link cm-text-initial cm-text-left">Forgot Password</a>
                            </div>
                        </div>
                        <div class="login-method">
                            <h3 class="title">Log in with</h3>
                            <ul>
                                <li class="login-item">
                                    <a href="<?php echo $authUrl;?>" class="icon icon-facebook"></a>
                                </li>
                                <li class="login-item">
                                    <a href="<?php echo $google_login_url;?>" class="icon icon-google"></a>
                                </li>
                            </ul>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
    <?php $this->load->view('include/footer.php'); ?>
    <span class="cm-overlay"></span>

    <?php $this->load->view('include/footer_2.php'); ?>
    <!-- js group start -->
    <!-- <script type="text/javascript" src="assets/js/plugins/libraries.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script> -->

<!-- build:js assets/js/vendor.js -->
<!-- <script type="text/javascript" src="assets/js/plugins/libraries.js"></script> -->
<?php $this->load->view('include/footer_js.php'); ?>

<script type="text/javascript">

    $.validator.setDefaults({
        submitHandler: function() {
            //$('#btnLogin').text('processing...'); //change button text
            $('.btnLogin').addClass("btn-loader");
            $('.btnLogin').attr('disabled', true); //set button disable 
            $("#error-div").removeClass("show")
            $("#error-div").addClass("hide")   
            
            $('#error-text').html('');
              $.ajax({
                    url: '<?php echo site_url("login/doLogin") ?>',
                    type: "POST",
                    data: $('#form').serialize(),
                    dataType: "JSON",
                    success: function (data)
                    {
                        //console.log(data)  
                        if (data.token) //if success close modal and reload ajax table
                        {
                            //$('#btnLogin').text('processing...'); //change button text
                            //$('#btnLogin').addClass("btn-load");
                            //$('.btnLogin').addClass('btn-loader');
                            //return false;
                            //$('.btnLogin').attr('disabled', true); //set button disable 
                            if(data.redirect_url==""){
                                window.location.replace('<?php echo base_url() ?>');    
                            }else{
                                window.location.replace(data.redirect_url);
                            }
                            
                        } else
                        {   
                            $('.btnLogin').removeClass('btn-loader');
                            $('.btnLogin').attr('disabled', false); //set button enable 
                            $("#error-div").removeClass("hide")
                            $("#error-div").addClass("show")   
                            $('#error-text').html("Please enter correct credentials");
                        }
                        //$('#btnLogin').text('login'); //change button text
                        //$('#btnLogin').removeClass("btn-load");
                        


                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('error');

                    }
                });
        }
    });


    $(document).ready(function () {

     jQuery("#form").validate({
       rules: {
                 password: "required",              
               username: {
                   required: true
               }
           },
           errorElement: "span",
                        errorClass: "error-msg",
                        errorPlacement: function(error, element) {
                             error.appendTo($(element).parent());
                        },
                        highlight: function (element) {
                            $(element).parents(".form-group").addClass("error");
                        },
                        unhighlight: function (element) {
                            $(element).parents(".form-group").removeClass("error");
                        },
   });
     var message = "<?php echo $this->session->flashdata('message'); ?>";
     if(message=="registration-success"){
        $('#message').modal('show');
         setTimeout(function(){
                $('#message').modal('hide');
        }, 5000);
        
        //$("#productModal").modal('show');
     }

     var message_alert = "<?php echo $this->session->flashdata('msg'); ?>";
             if(message_alert !=""){
                $('#message-alert').modal('show');

             }
    });

</script>
<!-- endbuild -->
    <!-- js group end -->

</body>

</html>