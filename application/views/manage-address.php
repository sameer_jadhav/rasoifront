
<!DOCTYPE html>
<html>

<?php $this->load->view('include/head.php'); ?>

<body class="pg-banner">

   <?php $this->load->view('include/header.php'); ?>

    <main>
        <div class="bs-main">
            <div class="container">
                <ul class="mod-breadcrumbs">
                     <li><a href="<?php echo base_url();?>">home</a></li>
                    <li><a href="<?php echo base_url();?>profile">my account</a></li>
                    <li><a href="#" class="active">Address</a></li>
                </ul>
                <section>
                    <div class="bs-sec sec-first-child">
                        <div class="sec-head">
                            <h2 class="sec-title">
                                <span class="cm-cursive cm-line-break">Hi, <?php echo $userdata['user_display_name'] ?></span>
                            </h2>
                        </div>
                        <div class="sec-cont">
                            <div class="lyt-account">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="nav-wrap hidden-xs">
                                            <ul class="nav-list">
                                                <li class="nav-item">
                                                    <a href="<?php echo base_url();?>profile" class="nav-link "><span class="icon icon-user"></span> Personal Details</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="<?php echo base_url();?>orders" class="nav-link "><span class="icon icon-cart"></span> My Orders <span class="count">(<?php echo count($cust_orders);?>)</span></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="<?php echo base_url();?>manage-address" class="nav-link active"><span class="icon icon-agenda"></span> Saved Addresses</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="<?php echo base_url();?>wishlist" class="nav-link"><span class="icon icon-wishlist"></span> Wishlist</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="bs-custom-select typ-box visible-xs">
                                            <select id="tabSelect">
                                                <option value="profile" data-href="<?php echo base_url();?>profile"> Personal Details</option>
                                                <option value="orders" data-href="<?php echo base_url();?>orders"> My Orders <span class="count">(<?php echo count($cust_orders);?>)</span></option>
                                                <option value="manage-address" data-href="<?php echo base_url();?>manage-address" selected> Saved Addresses</option>
                                                <option value="wishlist" data-href="<?php echo base_url();?>wishlist"> Wishlist</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="cont-wrap">
                                            <div class="lyt-manage-address">
                                                <ul class="ma-list">
                                                <?php $i= 1; foreach ($user_address as $key => $value): ?>
                                                    <li class="ma-item" id="address_<?php echo $value['add_id']; ?>">
                                                        <div class="mod-add-card <?php if ($value['shipping_address_is_default']=="true"): ?>typ-default<?php endif ?>">
                                                            <div class="bs-radiobox typ-card">
                                                                <input type="radio" name="address" id="add3">
                                                                <label for="add3">
                                                                    <strong class="title"><?php echo $value['label'];?></strong>
                                                                    <address>
                                                                        <strong><?php echo $value['shipping_first_name'].' '.$value['shipping_last_name']; ?></strong>
                                                                        <span><?php echo $value['shipping_address_1']; ?>,</span>
                                                                        <span><?php echo $value['shipping_address_2'] ?> <?php echo $value['shipping_city'].' '.$value['shipping_postcode'] ?>,<?php echo $value['shipping_state']; ?>,<?php echo $value['shipping_country']; ?></span>
                                                                    </address>
                                                                </label>
                                                            </div>
                                                            <div class="action-wrap">
                                                            <?php if ($value['shipping_address_is_default']=="false" ||  $value['shipping_address_is_default']==""): ?>
                                                                <button class="btn btn-link btn-def" onclick="make_default('<?php echo $value['add_id']; ?>')">Make Default</button>
                                                                 <?php endif ?>

                                                                <button class="btn btn-link" onclick="edit_address('<?php echo $value['add_id']; ?>')">Edit</button>
                                                                <button class="btn btn-link"  data-add-id="<?php echo $value['add_id']; ?>" onclick="remove_add_modal(this)">Remove</button>
                                                            </div>
                                                            <?php if ($value['shipping_address_is_default']=="true"): ?>
                                                                <span class="tag">default</span>
                                                            <?php endif ?>
                                                        </div>
                                                    </li>
                                                    <?php $i++; endforeach ?>
                                                </ul>
                                                <button class="btn btn-add" onclick="add_address()" ><span class="icon icon-plus"></span> Add New Address</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </main>
    <?php $this->load->view('include/footer.php'); ?>

    <div class="bs-modal modal typ-form fade" id="addressModal" tabindex="-1" role="dialog" aria-labelledby="addressModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="bs-form">
                <form action="#" id="address_form_modal" class="form address_form">
                    <div class="modal-header">
                        <h2 class="form-title">Add new address</h2>
                    </div>
                    <input type="hidden" id="modal_label" name="add_id">
                    <div class="modal-body">
                    <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="input-label">Name</label>
                                    <input type="text" class="form-control" placeholder="Enter your full name" id="modal_shipping_address_2" name="shipping_first_name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="input-label">Mobile no.</label>
                                    <input type="text" class="form-control" placeholder="Enter 10 digit mobile no." id="modal_mobile_no" name="shipping_mobile_no">
                                </div>
                            </div>
                            <!-- <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="input-label">Email ID</label>
                                    <input type="text" class="form-control" name="shipping_email_id" placeholder="Enter Email Id">
                                </div>
                            </div> -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="input-label">Pin code</label>
                                    <input type="text" class="form-control" placeholder="Enter pincode" id="modal_shipping_postcode" name="shipping_postcode">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="input-label">State</label>
                                    <input type="text"  name="shipping_state" id="shipping_state" class="form-control" value="" readonly>
                                </div>
                            </div>
                             <input type="hidden" name="shipping_country" value="India" readonly>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="input-label">City</label>
                                    <input type="text" name="shipping_city" id="shipping_city" class="form-control" value="" readonly>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="" class="input-label">Address</label>
                                    <input type="text" class="form-control" placeholder="Enter address" id="modal_shipping_address_1" name="shipping_address_1">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="" class="input-label">Landmark</label>
                                    <input type="text"  required="" id="modal_shipping_address_2" name="shipping_address_2" class="form-control" placeholder="Enter address">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group radio-wrap last-child">
                                <strong class="input-label">type of address</strong>
                                    <div class="bs-radiobox">
                                        <input type="radio" id="home" value="home" name="label[]">
                                        <label for="home">home</label>
                                    </div>
                                    <div class="bs-radiobox">
                                        <input type="radio" id="office" value="office" name="label[]">
                                        <label for="office">office</label>
                                    </div>
                                    <div class="bs-radiobox">
                                        <input type="radio" id="other1" value="other" name="label[]">
                                        <label for="other1">other</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group last-child chk-wrap">
                                    <div class="bs-checkbox">
                                        <input type="checkbox" name="shipping_address_is_default"  value="true" id="addressDefault">
                                        <label for="addressDefault"><span class="check"></span>Make this my default address</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-outline" data-dismiss="modal" aria-label="Close">cancel</button>
                        <button type="submit"  id="btnSave" class="btn btn-default">save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="bs-modal typ-message modal fade" id="confirm_remove_add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bs-video">
                        <div class="bs-message typ-static">
                            <div class="msg-wrap">
                                <div class="ty-head">
                                    <span class="icon icon-reg-remove"></span>
                                    <p class="ty-desc">Are you sure you want to remove this address from your list? </p>
                                </div>
                                <div class="ty-cont">
                                    <button type="button" id="cancel_btn_remove_add" class="btn btn-outline" data-dismiss="modal">Cancel</button>
                                    <button type="button" id="proceed_btn_remove_add" class="btn btn-default" >proceed</button>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- js group start -->
    <!-- <script type="text/javascript" src="assets/js/plugins/libraries.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script> -->

<!-- build:js assets/js/vendor.js -->
<!-- <script type="text/javascript" src="assets/js/plugins/libraries.js"></script> -->
<?php $this->load->view('include/footer_js.php'); ?>
<!-- endbuild -->
    <!-- js group end -->
<script type="text/javascript">

            $("#modal_shipping_postcode").blur(function(){
                var id = $(this).val()
                $.ajax({
                    url: "<?php echo site_url('manageaddress/getpincodedata/') ?>/" + id,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data)
                    {
                        console.log(data.pindata[0])
                        $('#shipping_state').val(data.pindata[0].state);
                         $('#shipping_city').val(data.pindata[0].city);
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error get data from ajax');
                    }
                });
            });

            var save_method = '';            
            function edit_address(id) {
                $("#js-country-add").val(null)
                $("#js-city").val(null)
                $("#js-state").val(null)

                save_method = 'update';
                $('#address_form_modal')[0].reset(); // reset form on modals                
                $('.form-group').removeClass('has-error'); // clear error class
                $('.help-block').empty(); // clear error string

                //Ajax Load data from ajax
                $.ajax({
                    url: "<?php echo site_url('manageaddress/get_by_id/') ?>/" + id,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data)
                    {                     
                        //$('[name="label"]').val(data.meta_value.label);
                        //$("input[name=label[]][value=" + data.meta_value.label + "]").attr('checked', 'checked');
                        //$('[name="shipping_last_name"]').val(data.meta_value.shipping_last_name);
                        //$('[name="shipping_country"]').val(data.meta_value.shipping_country);
                        //console.log(data.data.meta_value)
                        $('[name="add_id"]').val(id);
                        $('[name="shipping_address_2"]').val(data.data.meta_value.shipping_address_2);
                        $('[name="shipping_postcode"]').val(data.data.meta_value.shipping_postcode);
                        //$('[name="shipping_email_id"]').val(data.data.meta_value.shipping_email_id);
                        //$('[name="shipping_address_is_default"]').val(data.data.meta_value.shipping_address_is_default);
                        $('[name="shipping_first_name"]').val(data.data.meta_value.shipping_first_name);
                        $('[name="shipping_address_1"]').val(data.data.meta_value.shipping_address_1);
                        $('[name="shipping_mobile_no"]').val(data.data.meta_value.shipping_mobile_no);
                        //$('[name="shipping_city"]').val(data.data.meta_value.shipping_city);
                        //$('[name="shipping_state"]').val(data.data.meta_value.shipping_state);
                         $('[name="shipping_country"]').val(data.data.meta_value.shipping_country);
                         $('#js-country-add').select2("val", data.data.meta_value.shipping_country);
                         if(data.data.meta_value.shipping_address_is_default=="true"){
                            $("input[name='shipping_address_is_default']").prop("checked","checked")
                        }
                        //get_state(data.data.meta_value.shipping_country , data.data.meta_value.shipping_state)
                        //$('#js-state').select2("val", data.data.meta_value.shipping_state);
                        $('[name="shipping_state"]').val(data.data.meta_value.shipping_state);
                        // get_city(data.data.meta_value.shipping_state,data.data.meta_value.shipping_city)
                        $('[name="shipping_city"]').val(data.data.meta_value.shipping_city);
                        
                        $('#'+data.data.meta_value.label).prop("checked", true);
                        $('#addressModal').modal('show'); // show bootstrap modal when complete loaded
                        $('.modal-title').text('Edit Address'); // Set title to Bootstrap modal title
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error get data from ajax');
                    }
                });

            }
            function add_address() {
                $("#js-country-add").val(null)
                $("#js-city").val(null)
                $("#js-state").val(null)


                 save_method = 'add';
                $('#address_form_modal')[0].reset(); // reset form on modals
                $('.form-group').removeClass('has-error'); // clear error class
                $('.help-block').empty(); // clear error string                
                $('#addressModal').modal('show'); // show bootstrap modal
                $('.modal-title').text('add new address'); // Set Title to Bootstrap modal title                
            }
             function save()
                {                  
                   // $('#btnSave').text('saving...'); //change button text
                    $('#btnSave').addClass("btn-load");
                    $('#btnSave').attr('disabled', true); //set button disable 
                    var url;

                    if (save_method=='add') {                        
                        url = "<?php echo site_url('manageaddress/add') ?>";
                    }else{                        
                        url = "<?php echo site_url('manageaddress/update') ?>";
                    }

                    // ajax adding data to database
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: $('.address_form').serialize(),//get data of all address from all div address
                        dataType: "JSON",
                        success: function (data)
                        {

                            if (data.status) //if success close modal and reload ajax table
                            {   
                                //$('#newAddress').modal('hide'); // show bootstrap modal                                
                                //alert('success');
                                location.reload();
                            } else
                            {
                                for (var i = 0; i < data.inputerror.length; i++)
                                {
                                    $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                                    $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
                                }
                            }
                            //$('#btnSave').text('save'); //change button text
                            $('#btnSave').removeClass("btn-load");
                            $('#btnSave').attr('disabled', false); //set button enable 


                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error adding / update data');
                            $('#btnSave').text('save'); //change button text
                            $('#btnSave').attr('disabled', false); //set button enable 

                        }
                    });
                }

            
            function remove_add_modal(btn_data){
                $("#confirm_remove_add").modal('show');
                var id = $(btn_data).attr('data-add-id');
                $("#proceed_btn_remove_add").attr("data-address-id",id)
                $('#cancel_btn_remove_add').on('click', function(e) {
                   $('#confirm_remove_add').modal('hide');
                    return false; 
                })
            }

            $("#proceed_btn_remove_add").on("click",function(){

                var id = $("#proceed_btn_remove_add").attr("data-address-id");
                //alert(id)
                var url = "<?php echo site_url('manageaddress/delete/') ?>/"+id
                // ajax adding data to database
                    $.ajax({
                        url: url,
                        type: "DELETE",
                        data: id,//get data of all address from all div address
                        dataType: "JSON",
                        success: function (data)
                        {

                            if (data.code=="success") //if success close modal and reload ajax table
                            {   
                                $('#confirm_remove_add').modal('hide');
                                $('#address_'+id).hide(); // show bootstrap modal  
                                 $('#add-message-remove').modal('show');                              
                                //alert(data.code);
                                //location.reload();
                            } else
                            {
                                for (var i = 0; i < data.inputerror.length; i++)
                                {
                                    $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                                    $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
                                }
                            }
                            


                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error adding / update data');
                            

                        }
                    });
            
            })

            function remove_add(btn_data){
                var id = $(btn_data).attr('data-add-id');
                //alert(id)
                var url = "<?php echo site_url('manageaddress/delete/') ?>/"+id
                // ajax adding data to database
                    $.ajax({
                        url: url,
                        type: "DELETE",
                        data: id,//get data of all address from all div address
                        dataType: "JSON",
                        success: function (data)
                        {

                            if (data.code=="success") //if success close modal and reload ajax table
                            {   
                                $('#address_'+id).hide(); // show bootstrap modal                                
                               // alert(data.code);
                                //location.reload();
                                 $('#add-message-remove').modal('show');
                            } else
                            {
                                for (var i = 0; i < data.inputerror.length; i++)
                                {
                                    $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                                    $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
                                }
                            }
                            


                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error adding / update data');
                            

                        }
                    });
            }  
            function make_default(add_id){
                
                //alert(id)
                var url = "<?php echo site_url('manageaddress/make_default/') ?>/"+add_id
                // ajax adding data to database
                    $.ajax({
                        url: url,
                        type: "GET",                        
                        dataType: "JSON",
                        success: function (data)
                        {

                            if (data.status=="success") //if success close modal and reload ajax table
                            {   
                                $("#toaster-text").text("Your address has been marked as default");
                                $("#normal_toaster").addClass("active");
                                setTimeout(function(){
                                    $("#normal_toaster").removeClass("active");
                                    location.reload();
                                }, 3000);                                
                                //location.reload();
                            } else
                            {
                                alert('error');
                            }
                            


                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error adding / update data');
                            

                        }
                    });
            }  

             function getShipping(pincode){
            var shipping_pin = pincode;
            if(shipping_pin!=""){
                $.ajax({
                    url: '<?php echo site_url("cart/calculate") ?>',
                    type: "POST",
                    data: { pincode : shipping_pin },
                    dataType: "JSON",
                    success: function (data)
                    {   
                        
                       var current_cart_subtotal =  $("#cart_subtotal").text();

                       $("#cart_shipping").text(data);
                       $("#cart_total").text(Number(data) + Number(current_cart_subtotal))

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('error');

                    }
                });
            }
        }


   

        function get_state(country_id,set_val=null){
            if(country_id!=""){
                $('#js-state option').remove();
                $.ajax({
                    url: '<?php echo site_url("shipping/get_state") ?>',
                    type: "GET",
                    data: { country_id : country_id },
                    dataType: "json",
                    success: function (data)
                    {   
                        
                       var indiaState = data;
                       
                        indiaState.forEach(function (item, index) {
                            //console.log(item);

                        $('#js-state').append(
                            '<option value="' + item.name + '">' + item.name + '</option>'
                        );
                    });

                    $("#js-state").select2("enable", true);
                     if(set_val){
                        $("#js-state").select2("val",set_val)
                         
                     }


                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('error');

                    }
                });
            }
        }
       
        $('#js-state').on('change',function(){

            var id=$(this).val();
             referVal = id + 'City';
             $('#js-city option').remove();
             if(id!=""){
                $.ajax({
                    url: '<?php echo site_url("shipping/get_city") ?>',
                    type: "GET",
                    data: { state_id : id },
                    dataType: "json",
                    success: function (data)
                    {   
                        
                        var indiaCity = data;
                        indiaCity.forEach(function (item, index) {
                            //console.log(item);
                        $('#js-city').append(
                            '<option value="' + item.name + '">' + item.name + '</option>'
                        );

                    });

                    $("#js-city").select2("enable", true);
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('error');

                    }
                });
            }
       
       })
      
        function get_city(state_id,set_val=null){
            if(state_id!=""){
                $.ajax({
                    url: '<?php echo site_url("shipping/get_city") ?>',
                    type: "GET",
                    data: { state_id : state_id },
                    dataType: "json",
                    success: function (data)
                    {   

                       var indiaCity = data;
                        indiaCity.forEach(function (item, index) {
                            //console.log(item);
                            $('#js-city').append(
                                '<option value="' + item.name + '">' + item.name + '</option>'
                            );
                        });  
                         $("#js-city").select2("enable", true);
                      if(set_val){
                         //$('[name="shipping_city"]').val(set_val)
                         $("#js-city").select2("val",set_val)
                     }

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('error');

                    }
                });
            }
        }

        jQuery("#address_form_modal").validate({
               rules: {
                        shipping_first_name: "required",
                        shipping_address_1: "required",
                        shipping_address_2: "required",
                        shipping_country: "required",
                        shipping_state: "required",
                        /*shipping_email_id: {
                            required: true,
                            email: true
                        },*/
                        shipping_city: "required",
                        shipping_postcode: "required",
                         shipping_mobile_no: {
                            required: true,
                            number: true,
                            minlength : 10,
                            maxlength : 15
                        },
                        'label[]': "required"
                   },
                   errorElement: "span",
                        errorClass: "error-msg",
                        errorPlacement: function(error, element) {
                             error.appendTo($(element).parent());
                        },
                        highlight: function (element) {
                            $(element).parents(".form-group").addClass("error");
                        },
                        unhighlight: function (element) {
                            $(element).parents(".form-group").removeClass("error");
                        },
                    
                submitHandler: function() {
                    //$('#btnSave').text('saving...'); //change button text
                    $('#btnSave').addClass("btn-load");
                    $('#btnSave').attr('disabled', true); //set button disable 
                    var url;

                    if (save_method=='add') {                        
                        url = "<?php echo site_url('manageaddress/add') ?>";
                    }else{                        
                        url = "<?php echo site_url('manageaddress/update') ?>";
                    }
              $.ajax({
                        url: url,
                        type: "POST",
                        data: $('.address_form').serialize(),//get data of all address from all div address
                        dataType: "JSON",
                        success: function (data)
                        {

                            if (data.status) //if success close modal and reload ajax table
                            {   
                                //$('#newAddress').modal('hide'); // show bootstrap modal                                
                                //alert('success');
                                location.reload();
                            } else
                            {
                                for (var i = 0; i < data.inputerror.length; i++)
                                {
                                    $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                                    $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
                                }
                            }
                            $('#btnSave').removeClass("btn-load");
                            //$('#btnSave').text('save'); //change button text
                            $('#btnSave').attr('disabled', false); //set button enable 


                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error adding / update data');
                            //$('#btnSave').text('save'); //change button text
                            $('#btnSave').removeClass("btn-load");
                            $('#btnSave').attr('disabled', false); //set button enable 

                        }
                    });
                }
        
           });
           
           var message = "<?php echo $this->session->flashdata('address-msg'); ?>";
             if(message=="address-ad-success"){
                $("#ad-msg-title").text("Address added Successfully")
                $("#ad-msg-content").text("Address has been added successfully.")
                $('#add-message').modal('show');
             } 

             if(message=="address-up-success"){
                $("#ad-msg-title").text("Address updated Successfully")
                $("#ad-msg-content").text("Address has been updated successfully.")
                $('#add-message').modal('show');
             }  

             if(message=="address-del-success"){
                $("#ad-msg-title").text("Address deleted Successfully")
                $("#ad-msg-content").text("Address has been deleted successfully.")
                //$('#add-message').modal('show');
             }  


    
             
         </script>


</body>

</html>