
<!DOCTYPE html>
<html>

<?php $this->load->view('include/head.php'); ?>

<body class="pg-banner">

    <?php $this->load->view('include/header.php'); ?>

    <main>
        <div class="bs-main">
            <div class="container">
                <ul class="mod-breadcrumbs">
                    <li><a href="<?php echo base_url();?>">home</a></li>
                    <li><a href="<?php echo base_url();?>profile">my account</a></li>
                    <li><a href="#" class="active">Orders</a></li>
                </ul>
                <section>
                    <div class="bs-sec sec-first-child">
                        <div class="sec-head">
                            <h2 class="sec-title">
                                <span class="cm-cursive cm-line-break">Hi, <?php echo $userdata['user_display_name'] ?></span>
                            </h2>
                        </div>
                        <div class="sec-cont">
                            <div class="lyt-account">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="nav-wrap hidden-xs">
                                            <ul class="nav-list">
                                                <li class="nav-item">
                                                    <a href="<?php echo base_url();?>profile" class="nav-link "><span class="icon icon-user"></span> Personal Details</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="<?php echo base_url();?>orders" class="nav-link active"><span class="icon icon-cart"></span> My Orders <span class="count">(<?php echo count($cust_orders);?>)</span></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="<?php echo base_url();?>manage-address" class="nav-link"><span class="icon icon-agenda"></span> Saved Addresses</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="<?php echo base_url();?>wishlist" class="nav-link"><span class="icon icon-wishlist"></span> Wishlist</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="bs-custom-select typ-box visible-xs">
                                            <select id="tabSelect">
                                                <option value="profile" data-href="<?php echo base_url();?>profile"> Personal Details</option>
                                                <option value="orders" data-href="<?php echo base_url();?>orders" selected> My Orders <span class="count">(<?php echo count($cust_orders);?>)</span></option>
                                                <option value="manage-address" data-href="<?php echo base_url();?>manage-address"> Saved Addresses</option>
                                                <option value="wishlist" data-href="<?php echo base_url();?>wishlist"> Wishlist</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="cont-wrap">
                                            <div class="bs-orders">
                                                <ul class="order-list">
                                                <?php 
                                                $order_count = count($cust_orders);
                                                if($order_count>0){ 
                                                 for($w=0;$w<$order_count;$w++){ ?>
                                                    <li class="ol-item">
                                                        <div class="order-item">
                                                            <div class="oi-head">
                                                                <ul class="head-details">
                                                                    <li>
                                                                        <p>Order ID : <span>#<?php echo $cust_orders[$w]['number']; ?></span></p>
                                                                    </li>
                                                                    <li>
                                                                        <p>Placed on <span><?php echo date('d M Y',strtotime($cust_orders[$w]['date_created'])); ?></span></p>
                                                                    </li>
                                                                    <li>
                                                                        <p>Total Amount Payable <span>&#8377; <?php echo $cust_orders[$w]['total']; ?></span></p>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="oi-cont">
                                                                <div class="row">
                                                                    <div class="col-md-5">
                                                                        <ul class="o-list">
                                                                        <?php for($p=0;$p<count($cust_orders[$w]['line_items']);$p++){?>  
                                                                            <li class="o-item">
                                                                                <div class="mod-cart-pd">
                                                                                    <div class="pd-img">
                                                                                        <img src="<?php echo $cust_orders[$w]['line_items'][$p]['img_url'];?>" alt="product">
                                                                                    </div>
                                                                                    <div class="pd-detail">
                                                                                        <div class="title-wrap">
                                                                                            <h3 class="pd-title"><?php echo $cust_orders[$w]['line_items'][$p]['name'];?></h3>
                                                                                            <div class="bs-amount">
                                                                                                <strong class="amount-val">&#8377;<?php echo $cust_orders[$w]['line_items'][$p]['price'];?></strong>
                                                                                            </div>
                                                                                            <span class="net-quantity"><?php echo $cust_orders[$w]['line_items'][$p]['meta_data'][0]['value'];?>gm X <?php echo $cust_orders[$w]['line_items'][$p]['quantity'];?></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        <?php } ?>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <ul class="mod-progress">
                                                                            <?php 
                                                                                $process_class = "";
                                                                                $shipped_class = "";
                                                                                $delivered_class = "";
                                                                             switch ($cust_orders[$w]['status']) {
                                                                                 case 'pending':
                                                                                        $process_class = "active";
                                                                                     break;

                                                                                case 'paid':
                                                                                        
                                                                                        $process_class = "active";
                                                                                     break;

                                                                                case 'processing':
                                                                                        $shipped_class = "active";
                                                                                        $process_class = "active";
                                                                                     break;

                                                                                case 'completed':
                                                                                        $shipped_class = "active";
                                                                                        $process_class = "active";
                                                                                        $delivered_class = "active";
                                                                                     break;
                                                                                 
                                                                                 default:
                                                                                     # code...
                                                                                     break;
                                                                             }
                                                                            ?>
                                                                            <li class="progress-item <?php echo $process_class;?>">Processed</li>
                                                                            <li class="progress-item <?php echo $shipped_class;?>">Shipped</li>
                                                                            <li class="progress-item <?php echo $delivered_class;?>">Delivered</li>
                                                                        </ul>
                                                                        <!--button type="button" class="btn btn-link btn-icon-link"><span class="icon icon-track"></span> track order</button-->
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="action-wrap">
                                                                            <!-- <button type="button" class="btn btn-outline visible-xs">cancel order</button> -->
                                                                            <button type="button" onclick="get_order_summary(<?php echo $cust_orders[$w]['id']; ?>);" class="btn btn-default" data-toggle="modal" data-target="#orderSummaryModal">view details</button>
                                                                            <?php if($cust_orders[$w]['status']=="pending" || $cust_orders[$w]['status']=="paid"){  ?>
                                                                            <!--button id="cancel_<?php echo $cust_orders[$w]['id']; ?>" type="button" class="btn btn-outline" onclick="cancel_order_modal(<?php //echo $cust_orders[$w]['id']; ?>)">cancel order</button-->
                                                                            <?php }else if($cust_orders[$w]['status']=="cancelled"){ ?>
                                                                                <button id="cancel_<?php echo $cust_orders[$w]['id']; ?>" type="button" class="btn btn-outline" >cancelled</button>
                                                                            <?php } ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                 <?php } 
                                             }else { ?>
                                                <div class="bs-message typ-static">
                                                            <div class="msg-wrap">
                                                                <div class="ty-head">
                                                                    <span class="icon icon-bag"></span>
                                                                </div>
                                                                <div class="ty-cont">
                                                                    <p>No Orders Yet</p>
                                                                    <a href="<?php echo base_url();?>shop" type="button" class="btn btn-default">return to shop</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                            <?php  } ?>   
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </main>
    <?php $this->load->view('include/footer.php'); ?>
    <?php $this->load->view('include/footer_2.php'); ?>

    <div class="bs-modal typ-message modal fade" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="bs-video">
                    <div class="bs-message typ-static">
                        <div class="msg-wrap">
                            <div class="ty-head">
                                <span class="icon icon-remove"></span>
                                <p class="ty-desc">Are you sure you want to remove this item?</p>
                            </div>
                            <div class="ty-cont">
                                <button type="button" class="btn btn-outline" data-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-default">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <div class="bs-modal modal typ-message fade" id="orderSummaryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="bs-order-details">
                    <ul class="od-list">
                        <li>
                            <h3 class="title">order summary</h3>
                            <div class="mod-data-list">
                                <dl>
                                    <dt class="data-label">Sub Total</dt>
                                    <dd  class="data-value">&#8377; <span id="ord_sum_subtotal">45</span></dd>
                                </dl>
                                <dl>
                                    <dt class="data-label">Delivery Charges</dt>
                                    <dd  class="data-value">&#8377; <span id="ord_sum_shipping" >100</span></dd>
                                </dl>
                                <dl>
                                    <dt class="data-label">Taxes (5% GST)</dt>
                                    <dd  class="data-value">&#8377; <span id="ord_sum_tax">50</span></dd>
                                </dl>

                                <dl>
                                    <dt class="data-label">Total</dt>
                                    <dd  class="data-value">&#8377; <span id="ord_sum_total">550</span></dd>
                                </dl>
                                
                                
                                
                                <dl>
                                    <dt class="data-label">Total Amount Payable</dt>
                                    <dd  class="data-value">&#8377; <span id="ord_sum_pay_total" >650</span></dd>
                                </dl>
                            </div>
                        </li>
                        <li>
                            <h3 class="title">shipping details</h3>
                            <div class="mod-add-card">
                                <address>
                                    <strong id="ord_sum_customer_name">Shashank Kumar</strong>
                                    <span id="ord_sum_address">14, Gemini bulding, Dr kankal hospital,</span>
                                   
                                </address>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- js group start -->
    <!-- <script type="text/javascript" src="assets/js/plugins/libraries.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script> -->

<!-- build:js assets/js/vendor.js -->
<!-- <script type="text/javascript" src="assets/js/plugins/libraries.js"></script> -->
<?php $this->load->view('include/footer_js.php'); ?>
<?php $this->load->view('include/commonsite_js.php'); ?>
<!-- endbuild -->
    <!-- js group end -->
<script type="text/javascript">
    
    function get_order_summary(order_id){
    //alert(order_id);
    $.ajax({
                    url: '<?php echo site_url("orders/order_summary") ?>',
                    type: "GET",
                    data: {order_id : order_id},
                    dataType: "JSON",
                    success: function (data)
                    {

                       $("#ord_sum_total").text(data.total);
                       $("#ord_sum_tax").text(data.total_tax);
                       $("#ord_sum_subtotal").text(data.subtotal);
                       $("#ord_sum_shipping").text(data.shipping_total);
                       $("#ord_sum_pay_total").text(data.total);
                       $("#ord_sum_customer_name").text(data.first_name+' '+data.last_name);
                       $("#ord_sum_address").text(data.first_name+' '+data.last_name+','+data.address_1+','+data.address_2+','+data.city+','+data.state+','+data.postcode+','+data.country);
                        //  $('#order').modal('show'); 
                        setTimeout(() => {
                            tooltip(); 
                        }, 500);
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('error');

                    }
                });
}
</script>

</body>

</html>