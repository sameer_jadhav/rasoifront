<button type="button" class="close visible-xs" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="bs-product-details typ-sm">
                    <div class="pd-view-wrap">
                        <div class="bs-view">
                            <div class="swiper-container gallery-top view-wrap">
                                <div class="swiper-wrapper">
                                     <?php foreach ($product['images'] as $key => $value):  ?>
                                    <div class="  swiper-slide">
                                        <img src="<?php echo $value['src'];?>"  alt="<?php echo $value['alt'];?>">
                                    </div>
                                    <?php endforeach ?> 
                                </div>
                            </div>
                            <!--div class="view-item-list swiper-container gallery-thumbs">
                                <ul class="swiper-wrapper">

                                <?php
                                //$i = 0;
                                //foreach ($product['images'] as $key => $value):
                                    $class_active = "";
                                   // if($i==0){ $class_active = "active"; }
                                ?>

                                    <li class="view-item swiper-slide">
                                        <img src="<?php //echo $value['src'];?>">
                                        <a href="#" class="view-link <?php //echo $class_active;?>"></a>
                                    </li>
                                
                                    <?php 
                                //$i++;
                                //endforeach ?>  

                                </ul>
                            </div-->



                             <?php 
                                                    $like_class=''; 
                                                     if (in_array($product['id'], $stored_cookie)) {
                                                        $like_class='active';
                                                     ?>
                                                        <button type="button" id="btnRemoveWishlistmod<?php echo $product['id'];?>" onclick="remove_wishlist_modal('<?php echo $product['id'];?>')" class="btn pd-btn-like js-like icon-wishlist-o visible-xs <?php echo $like_class ?>">
                                                            
                                                        </button>
                                                    <?php }else{ ?>
                                                        <button type="button" id="btnWishlistmod<?php echo $product['id'];?>" onclick="add_wishlist('<?php echo $product['id'];?>')" class="btn pd-btn-like js-like icon-wishlist-o visible-xs <?php echo $like_class ?> ">
                                                            
                                                        </button>
                                                    <?php } ?>
                           
                           
                           

                        </div>
                    </div>
                    <div class="pd-info">
                        <h2 class="pd-title"><?php echo $product['name'];?></h2>
                        <div class="bs-amount">
                            <strong class="amount-val" >&#8377; <span id="product_price"><?php echo $product['variations_price'][0];?></span></strong>
                            <strong class="amount-qty" id="product_weight">/ <?php echo $product['attributes'][0]['options'][0];?> gm</strong>
                        </div>
                        <div class="bs-attribute">
                            <ul>
                                <li>
                                    <div class="attr-item">
                                        <ul class="dd-list typ-split">
                                        <?php 
                                                            switch ($product['categories'][0]['slug']) {
                                                                case 'blends':
                                                                    $class_icon = "icon-blend";
                                                                    break;

                                                                case 'ground-spice':
                                                                    $class_icon = "icon-spa";
                                                                    break;

                                                                case 'whole-spice':
                                                                    $class_icon = "icon-kitchen";
                                                                    break;
                                                                
                                                                default:
                                                                    # code...
                                                                    break;
                                                            }
                                                            ?>
                                            <li>
                                                <label class="dd-title">Category</label>
                                                <strong class="dd-value"><span class="icon <?php echo $class_icon;?>"></span><?php echo $product['categories'][0]['name'];?></strong>
                                            </li>
                                            <?php
                                                            foreach($product['meta_data'] as $key=>$value){ 
                                                                if($value['key']=="product_type"){
                                                                    $product_type =  $value['value'];
                                                                    }
                                                                 }
                                                            ?>
                                            <!--li>
                                                <label class="dd-title">Type</label>
                                                <strong class="dd-value"><?php //echo $product_type;?></strong>
                                            </li-->
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <div class="dd-list">
                                        <label class="dd-title">Select Size</label>
                                        <div class="radio-wrap">
                                        <?php 

                                                                foreach($product['attributes'][0]['options'] as $key=>$value) {
                                                                $checked = "";
                                                                if($key==0){
                                                                    $checked = "checked='checked'";
                                                                }
                                                             ?>
                                            <div class="bs-radiobox typ-pill">
                                                <input <?php echo $checked;?> id="radio<?php echo $key;?>" value="<?php echo $product['variations_price'][$key].";".$value;?>" type="radio" data-variation-id="<?php echo $product['variations'][$key];?>" class="form-control"  name="weight_class">
                                                                <label for="radio<?php echo $key;?>"><?php echo $value;?> gm</label>
                                            </div>
                                            <?Php } ?>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="dd-list">
                                        <label class="dd-title">Quantity</label>
                                        <div class="attr-action">
                                            <div class="bs-custom-select typ-box">
                                                <select id="count_summary">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                            </div>

                                             <button id="cart_summary_btn" onclick="add_to_cart(this)" data-product-id="<?php echo $product['id'];?>" data-var-id="<?php echo $product['variations'][0];?>" data-weight="<?php echo $product['attributes'][0]['options'][0];?>" id="add_to_cart"  type="button" class="btn btn-default">add to cart</button>

                                            <!-- <a href="cart.html" type="button" class="btn btn-default">add to cart</a> -->
                                        </div>
                                    </div>
                                </li>
                                <!--li>
                                    <div class="attr-item">
                                        <h3 class="attr-title">Description</h3>
                                        <div class="pd-desc">
                                            <p><?php //echo $product['short_description'];?></p>
                                        </div>
                                    </div>
                                </li-->
                            </ul>
                        </div>
                         <?php 
                                                    $like_class_2=''; 
                                                     if (in_array($product['id'], $stored_cookie)) {
                                                        $like_class_2='active';
                                                     ?>
                                                        <button type="button" id="btnRemoveWishlistmodad<?php echo $product['id'];?>" onclick="remove_wishlist_modal('<?php echo $product['id'];?>')" class="btn pd-btn-like js-like  icon-wishlist-o hidden-xs <?php echo $like_class_2 ?>">
                                                            
                                                        </button>
                                                    <?php }else{ ?>
                                                        <button type="button" id="btnWishlistmodad<?php echo $product['id'];?>" onclick="add_wishlist('<?php echo $product['id'];?>')" class="btn pd-btn-like js-like icon-wishlist-o hidden-xs <?php echo $like_class_2 ?>">
                                                            
                                                        </button>
                                                    <?php } ?>
                        
                        <!-- <div class="pd-action-wrap visible-xs cm-fly-action-wrap js-targetEle">
                            <a href="cart.html" class="btn btn-default">add to cart</a>
                        </div> -->
                    </div>
                </div>