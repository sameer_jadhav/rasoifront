
<!DOCTYPE html>
<html>
<?php $this->load->view('include/head.php'); ?>
<body>
    <div>
        
        <?php $this->load->view('include/header.php'); ?>
        <main>
            <div class="bs-main">
                <div class="container">
                    <ul class="mod-breadcrumbs">
                        <li><a href="<?php echo base_url();?>">Home</a></li>
                        <li><a href="<?php echo base_url();?>shop">Shop</a></li>
                        <li><a href="#" class="active"><?php echo $product[0]['name'];?></a></li>
                    </ul>
                    <section>
                        <div class="bs-sec sec-first-child lyt-product-details">
                            <div class="sec-cont">
                                <div class="bs-product-details">
                                    <div class="pd-view-wrap">
                                        <div class="bs-view">
                                            <div class="swiper-container gallery-top view-wrap">
                                                <div class="swiper-wrapper">
                                                    
                                                   <?php foreach ($product[0]['images'] as $key => $value):  ?>
                                                    <div class="swiper-slide">
                                                        <img src="<?php echo $value['src'];?>" alt="<?php echo $value['alt'];?>">
                                                    </div>
                                                    <?php endforeach ?>  
                                                </div>
                                            </div>
                                            <!--div class="view-item-list swiper-container gallery-thumbs">
                                                <ul class="swiper-wrapper">
                                                <?php
                                                //$i = 0;
                                                 //foreach ($product[0]['images'] as $key => $value):
                                                $class_active = "";
                                                //if($i==0){ $class_active = "active"; }
                                                  ?>
                                                    <li class="view-item swiper-slide">
                                                        <img src="<?php //echo $value['src'];?>">
                                                        <a href="#" class="view-link <?php //echo $class_active;?>"></a>
                                                    </li>
                                                    <?php 
                                                //$i++;
                                                //endforeach ?>  
                                                </ul>
                                            </div-->

                                             
                                            <!-- <button type="button" class="btn pd-btn-like js-like icon-wishlist-o visible-xs"></button> -->
                                        </div>
                                    </div>
                                    <div class="pd-info js-element">
                                        <h2 class="pd-title"><?php echo $product[0]['name'];?></h2>
                                        <div class="bs-amount">
                                            <strong class="amount-val" >&#8377; <span id="product_price"><?php echo $product[0]['variations_price'][0];?></span></strong>
                                            <strong class="amount-qty" id="product_weight">/ <?php echo $product[0]['attributes'][0]['options'][0];?> gm</strong>
                                        </div>
                                        <div class="pd-desc hidden-xs">
                                            <p><?php echo $product[0]['short_description'];?></p>
                                        </div>
                                        <div class="bs-attribute">
                                            <ul>
                                                <li class="hidden-xs">
                                                    <div class="attr-item">
                                                        <ul class="dd-list typ-split">
                                                            <li>
                                                            <?php 
                                                            switch ($product[0]['categories'][0]['slug']) {
                                                                case 'blends':
                                                                    $class_icon = "icon-blend";
                                                                    break;

                                                                case 'ground-spice':
                                                                    $class_icon = "icon-spa";
                                                                    break;

                                                                case 'whole-spice':
                                                                    $class_icon = "icon-kitchen";
                                                                    break;
                                                                
                                                                default:
                                                                    # code...
                                                                    break;
                                                            }
                                                            ?>
                                                                <label class="dd-title">Category</label>
                                                                <strong class="dd-value"><span class="icon <?php echo $class_icon;?>"></span><?php echo $product[0]['categories'][0]['name'];?></strong>
                                                            </li>

                                                            <?php
                                                            foreach($product[0]['meta_data'] as $key=>$value){ 
                                                                if($value['key']=="product_type"){
                                                                    $product_type =  $value['value'];
                                                                    }
                                                                 }
                                                            ?>
                                                            <!--li>
                                                                <label class="dd-title">Type</label>
                                                                <strong class="dd-value"><?php //echo $product_type;?></strong>
                                                            </li-->
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="dd-list">
                                                        <label class="dd-title">Select Size</label>
                                                        <div class="radio-wrap">
                                                            <?php 

                                                                foreach($product[0]['attributes'][0]['options'] as $key=>$value) {
                                                                $checked = "";
                                                                if($key==0){
                                                                    $checked = "checked='checked'";
                                                                }
                                                             ?>
                                                             <div class="bs-radiobox typ-pill">
                                                                <input <?php echo $checked;?> id="radio<?php echo $key;?>" value="<?php echo $product[0]['variations_price'][$key].";".$value;?>" type="radio" class="form-control"  name="weight_class" data-variation-id="<?php echo $product[0]['variations'][$key];?>">
                                                                <label for="radio<?php echo $key;?>"><?php echo $value;?> gm</label>
                                                            </div>
                                                            <?Php } ?>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="dd-list">
                                                        <label class="dd-title">Quantity</label>
                                                        <div class="attr-action">
                                                            <div class="bs-custom-select typ-box">
                                                                <select id="count_summary">
                                                                    <option value="1">01</option>
                                                                    <option value="2">02</option>
                                                                    <option value="3">03</option>
                                                                    <option value="4">04</option>
                                                                    <option value="5">05</option>
                                                                </select>
                                                            </div>
                                                           
                                                            <!-- <a href="cart.html" class="btn btn-default hidden-xs">add to cart</a> -->
                                                            <button onclick="add_to_cart(this)" data-product-id="<?php echo $product[0]['id'];?>" data-var-id="<?php echo $product[0]['variations'][0];?>" data-weight="<?php echo $product[0]['attributes'][0]['options'][0];?>" id="add_to_cart"  type="button" class="btn btn-default js-add-cart">add to cart</button>
                                                        </div>
                                                    </div>
                                                </li>
                                                <!--li class="visible-xs">
                                                    <div class="pd-desc ">
                                                        <p><?php echo $product[0]['description'];?>
                                                        </p>
                                                    </div>
                                                </li>
                                                <li class="visible-xs">
                                                    <div class="attr-item">
                                                        <ul class="dd-list typ-split">
                                                            <li>
                                                                <label class="dd-title">Category</label>
                                                                <strong class="dd-value"><span class="icon icon-blend"></span>Blends</strong>
                                                            </li>
                                                            <li>
                                                                <label class="dd-title">Type</label>
                                                                <strong class="dd-value"><?php echo $product_type;?></strong>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="attr-item">
                                                        <h3 class="attr-title">Description</h3>
                                                        <div class="pd-desc">
                                                            <p>Kerala’s black pepper powder is grown in the Southern part of the Western Ghats which melt into Idukki district. The rich and fertile hilly soil delivers delivers pepper that is spicy and has
                                                                a tangent smell. Black pepper or Kali Mirch apparently was used as currency in ancient Greece. This seemingly small black spice is power packed with nutrients to improve your health. These
                                                                include manganese, potassium, iron, dietary fiber.</p>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="attr-item">
                                                        <h3 class="attr-title">Additional Information</h3>
                                                        <ul class="dd-list">
                                                        
                                                            <li>
                                                                <label class="dd-title">Category</label>
                                                                <strong class="dd-value">Blend Spices</strong>
                                                            </li>
                                                            <li>
                                                                <label class="dd-title">Type</label>
                                                                <strong class="dd-value">Turmeric</strong>
                                                            </li>
                                                            <li>
                                                                <label class="dd-title">Weight</label>
                                                                <strong class="dd-value">500gm, 1Kg, 2Kg</strong>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li-->
                                            </ul>
                                        </div>
                                        <?php 
                                                    $like_class=''; 
                                                     if (in_array($product[0]['id'], $stored_cookie)) {
                                                        $like_class='active';
                                                     ?>
                                                        <button type="button" id="btnRemoveWishlist<?php echo $product[0]['id'];?>" onclick="remove_wishlist_modal('<?php echo $product[0]['id'];?>')" class="btn pd-btn-like js-like <?php echo $like_class ?> icon-wishlist-o">
                                                            
                                                        </button>
                                                    <?php }else{ ?>
                                                        <button type="button" id="btnWishlist<?php echo $product[0]['id'];?>" onclick="add_wishlist('<?php echo $product[0]['id'];?>')" class="btn pd-btn-like js-like <?php echo $like_class ?> icon-wishlist-o">
                                                            
                                                        </button>
                                                    <?php } ?>
                                        <div class="pd-action-wrap visible-xs cm-fly-action-wrap js-targetEle">
                                            <a href="javascript:void(0)" onclick="add_to_cart(this)" data-product-id="<?php echo $product[0]['id'];?>" data-var-id="<?php echo $product[0]['variations'][0];?>" id="add_to_cart"  type="button" class="btn btn-default js-add-cart">add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <?php 
                    $count_upsell_products = count($upsell_product);
                    if($count_upsell_products>0){ ?>
                    <section>
                        <div class="bs-sec bs-related-products sec-last-child">
                            <div class="sec-head">
                                <h2 class="sec-title">Related Products</h2>
                            </div>
                            <div class="sec-cont" id="moreProductsSwiper">
                                <div class="lyt-product-listing swiper-container">
                                    <ul class="list-wrap swiper-wrapper">
                                        <?php 
                                        for($m=0;$m<$count_upsell_products;$m++){ ?>
                                        <li class="item swiper-slide">
                                            <div class="bs-product">
                                                <div class="pd-head">
                                                    <figure class="pd-img-wrap">
                                                        <img src="<?php echo $upsell_product[$m]['images'][0]['src'];?>" alt="<?php echo $upsell_product[$m]['images'][0]['alt'];?>"> 
                                                    </figure>
                                                    <div class="pd-desc">
                                                        <p>Garam masala is a blend of ground spices used extensively in Indian cuisine.</p>
                                                        <p>The spices for garam masala are usually toasted to bring out more flavor and aroma, and then ground</p>
                                                    </div>
                                                </div>
                                                <div class="pd-cont">
                                                    <h3 class="pd-title"><?php echo $upsell_product[$m]['name'];?></h3>
                                                    <div class="bs-amount">
                                                        <!-- <strong class="amount-val strike">Rs. 350</strong> -->
                                                        <strong class="amount-val">&#8377; <?php echo $upsell_product[$m]['variations_price'][0];?></strong>
                                                        <strong class="amount-qty">/ <?php echo $upsell_product[$m]['attributes'][0]['options'][0];?> gm</strong>
                                                    </div>
                                                    <div class="pd-action">
                                                        <button type="button" class="btn btn-icon btn-outline"  data-toggle="modal" onclick="product_summary(<?php echo $upsell_product[$m]['id'];?>);"><span class="icon icon-view"></span></button>
                                                        <button type="button" class="btn btn-default" onclick="add_to_cart(this)" data-product-id="<?php echo $upsell_product[$m]['id'];?>" data-var-id="<?php echo $upsell_product[$m]['variations'][0];?>" id="add_to_cart">add to cart</button>
                                                    </div>
                                                </div>
                                                <a href="<?php echo base_url();?>shop/<?php echo $upsell_product[$m]['categories'][0]['slug'];?>/<?php echo $upsell_product[$m]['slug'];?>" class="pd-link"></a>
                                                <button type="button" class="btn pd-btn-like js-like"></button>
                                                <?php if(!empty($upsell_product[$m]['tags'])){ ?>
                                            <label class="pd-tag"><?php echo $upsell_product[$m]['tags'][0]['name'];?></label>
                                            <?php } ?>
                                                <div class="pd-action">
                                                    <div class="action-wrap">

                                                        <?php 
                                                            $like_class_2=''; 
                                                             if (in_array($upsell_product[$m]['id'], $stored_cookie)) {
                                                                $like_class_2='active';
                                                            
                                                            ?>
                                                    <button type="button" class="btn btn-icon btn-outline" id="btnRemoveWishlist<?php echo $upsell_product[$m]['id'];?>" onclick="remove_wishlist_modal('<?php echo $upsell_product[$m]['id'];?>')"><span class="icon icon-like <?php echo $like_class_2; ?>" ></span></button>
                                                     <?php }else{ ?>
                                                        <button type="button" class="btn btn-icon btn-outline" id="btnWishlist<?php echo $upsell_product[$m]['id'];?>" onclick="add_wishlist('<?php echo $upsell_product[$m]['id'];?>')" ><span class="icon icon-like <?php echo $like_class_2; ?>" ></span></button>
                                                    <?php } ?>

                                                        
                                                        <button type="button" class="btn btn-icon btn-outline" data-toggle="modal" onclick="product_summary(<?php echo $upsell_product[$m]['id'];?>);"><span class="icon icon-view"></span></button>
                                                         <button type="button" class="btn btn-icon btn-default"  onclick="add_to_cart(this)" data-product-id="<?php echo $upsell_product[$m]['id'];?>" data-var-id="<?php echo $upsell_product[$m]['variations'][0];?>" id="add_to_cart"><span class="icon icon-cart"></span></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                       <?php } ?>
                                    </ul>
                                </div>
                                <div class="swiper-pagination"></div>

                                <div class="swiper-btn swiper-button-prev hidden-xs"><span class="icon icon-left-arrow-c"></span></div>
                                <div class="swiper-btn swiper-button-next hidden-xs"> <span class="icon icon-right-arrow-c"></span></div>

                            </div>
                        </div>
                    </section>
                    <?php } ?>
                </div>
            </div>
        </main>
        <?php $this->load->view('include/footer.php'); ?>
    </div>

    <!-- js group start -->
    <!-- <script type="text/javascript" src="assets/js/plugins/libraries.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script> -->

<!-- build:js assets/js/vendor.js -->
<!-- <script type="text/javascript" src="assets/js/plugins/libraries.js"></script> -->
<?php $this->load->view('include/footer_js.php'); ?>
<?php $this->load->view('include/footer_2.php'); ?>
<?php $this->load->view('include/commonsite_js.php'); ?>
<!-- endbuild -->
    <!-- js group end -->

</body>

</html>