
<!DOCTYPE html>
<html>
<?php $this->load->view('include/head.php'); ?>

<body class="pg-banner">

    <?php $this->load->view('include/header.php'); ?>

    <main>
        <div class="bs-main">
            <div class="container">
                <ul class="mod-breadcrumbs">
                    <li><a href="<?php echo base_url();?>">home</a></li>
                    <li><a href="<?php echo base_url();?>profile">my account</a></li>
                    <li><a href="#" class="active">profile</a></li>
                </ul>
                <section>
                    <div class="bs-sec sec-first-child">
                        <div class="sec-head">
                            <h2 class="sec-title">
                                <span class="cm-cursive cm-line-break">Hi, <?php echo $user['first_name'] ?></span>
                            </h2>
                        </div>
                        <div class="sec-cont">
                            <div class="lyt-account">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="nav-wrap hidden-xs">
                                            <ul class="nav-list">
                                                <li class="nav-item">
                                                    <a href="<?php echo base_url();?>profile" class="nav-link active"><span class="icon icon-user"></span> Personal Details</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="<?php echo base_url();?>orders" class="nav-link"><span class="icon icon-cart"></span> My Orders <span class="count">(<?php echo count($cust_orders);?>)</span></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="<?php echo base_url();?>manage-address" class="nav-link"><span class="icon icon-agenda"></span> Saved Addresses</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="<?php echo base_url();?>wishlist" class="nav-link"><span class="icon icon-wishlist"></span> Wishlist</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="bs-custom-select typ-box visible-xs">
                                            <select id="tabSelect">
                                                <option value="profile" data-href="<?php echo base_url();?>profile" selected> Personal Details</option>
                                                <option value="orders" data-href="<?php echo base_url();?>orders"> My Orders <span class="count">(<?php echo count($cust_orders);?>)</span></option>
                                                <option value="manage-address" data-href="<?php echo base_url();?>manage-address"> Saved Addresses</option>
                                                <option value="wishlist" data-href="<?php echo base_url();?>wishlist"> Wishlist</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="cont-wrap">
                                            <form class="bs-form" name="profile_form" id="validation-form" method="post" action="<?php echo base_url();?>profile/save">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="" class="input-label">Name</label>
                                                            <input type="text" data-rule-required="true" name="customer_name" id="customer_name" required="" class="form-control" placeholder="Enter your full name " required="" value="<?php echo $user['first_name'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="" class="input-label">Email</label>
                                                            <input type="text" class="form-control" placeholder="Enter your full name"  value="<?php echo $user['email'] ?>" name="email" required="" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="" class="input-label">Mobile Number</label>
                                                            <input type="text" class="form-control" placeholder="Enter your Mobile number" name="phone_no" id="phone_no" value="<?php echo $user['billing']['phone']; ?>" data-rule-required="true" required="">
                                                        </div>
                                                    </div>
                                                    <?php if($user['oauthProvider']==""){ ?>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="" class="input-label">Password</label>
                                                            <div class="input-group">
                                                                <input type="password" class="form-control"  value="tetetetette"  name="user_password" id="user_password" required="" readonly>
                                                                <button type="button" class="btn btn-link input-btn" data-toggle="modal" data-target="#changePasswordModal">Change</button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <?php } ?>
                                                </div>
                                                <div class="form-foot">
                                                    <button type="submit" class="btn btn-default">save</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </main>
    <?php $this->load->view('include/footer.php'); ?>
    <?php $this->load->view('include/footer_2.php'); ?>

    <div class="bs-modal modal typ-form fade" id="changePasswordModal" tabindex="-1" role="dialog" aria-labelledby="addressModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="bs-form">
            <span id ="status_field_pass"></span>
                <form id="password_form" action="#" class="form">
                    <div class="modal-header">
                        <h2 class="form-title">Change Password</h2>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="" class="input-label">Existing Password</label>
                                    <input type="password" name="current_pass" id="current_pass" required="" class="form-control" placeholder="Enter Existing Password">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="" class="input-label">New Password</label>
                                    <input name="new_pass" id="user_new_password" required="" type="password" class="form-control" placeholder="Enter New Password">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="" class="input-label">Confirm new Password</label>
                                    <input type="password" name="confirm_pass" required="" class="form-control" placeholder="Re-enter new password">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-outline" data-dismiss="modal" aria-label="Close">cancel</button>
                        <button type="submit" id="btnChangePass" class="btn btn-default">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

    <!-- js group start -->
    <!-- <script type="text/javascript" src="assets/js/plugins/libraries.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script> -->

<!-- build:js assets/js/vendor.js -->
<!-- <script type="text/javascript" src="assets/js/plugins/libraries.js"></script> -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/vendor.js"></script>
<!-- endbuild -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/validation.js"></script>
<!-- build:js assets/js/custom.js -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $("#validation-form").validate({
        rules: {
                customer_name: "required",
                
                user_password: {
                    minlength: 5
                },
                user_confirm_password: {
                    minlength: 5,
                    equalTo: "#user_password"
                },
                phone_no: {
                    required: true,
                    number: true,
                    minlength:10,
                    maxlength:15
                }
            },
            errorElement: "span",
                        errorClass: "error-msg",
                        errorPlacement: function(error, element) {
                             error.appendTo($(element).parent());
                        },
                        highlight: function (element) {
                            $(element).parents(".form-group").addClass("error");
                        },
                        unhighlight: function (element) {
                            $(element).parents(".form-group").removeClass("error");
                        },
            submitHandler: function (form) {
                var url = $(form).attr('action');
                //alert(url)
                $.ajax({
                   type: "POST",
                   url: url,
                   data: $(form).serialize(), // serializes the form's elements.
                   dataType:"JSON",
                   success: function(data)
                   {
                        
                        $('#btnLogin').text('saving...'); //change button text
                        $('#btnLogin').attr('disabled', true);
                       if (data.status=="error") //if success close modal and reload ajax table
                       {
                         
                         //$("#status_field").attr("class","error");
                          $("#toaster-text").text("Some issue occured,Please try again later");
                                 $("#normal_toaster").addClass("active");
                                setTimeout(function(){
                                    $("#normal_toaster").removeClass("active");

                                }, 5000);
                        
                       } else
                       {
                        //$("#status_field").attr("class","success"); 
                        //$('#validation-form').trigger("reset");
                         $("#toaster-text").text("Your profile has been updated");
                                 $("#normal_toaster").addClass("active");
                                setTimeout(function(){
                                    $("#normal_toaster").removeClass("active");
                                    location.reload();
                                }, 5000);
  
                       }
                        $('#btnLogin').text('save'); //change button text
                        $('#btnLogin').attr('disabled', false); //set button enable 
                        //$("#status_field").html(data.message);
                   }
                 });
                return false; 
            }
    });
});
    
    $("#password_form").validate({
        rules: {
               
               current_pass : {
                required : true,
                remote : {
                    type: "POST",
                    url: '<?php echo site_url("profile/check_password") ?>',
                    data : {
                        current_pass : function() {
                            //alert(1111111)
                            return $( "#current_pass" ).val();
                          }
                    }
                }
               },
                new_pass: { minlength: 6 },
                confirm_pass: {
                    minlength: 6,
                    equalTo: "#user_new_password"
                },
                
            },
            errorElement: "span",
                        errorClass: "error-msg",
                        errorPlacement: function(error, element) {
                             error.appendTo($(element).parent());
                        },
                        highlight: function (element) {
                            $(element).parents(".form-group").addClass("error");
                        },
                        unhighlight: function (element) {
                            $(element).parents(".form-group").removeClass("error");
                        },
            messages : {
                current_pass : {
                    remote : jQuery.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                var url = $(form).attr('action');
                //alert(url)
                $.ajax({
                   type: "POST",
                   url: '<?php echo site_url("profile/changePassword") ?>',
                   data: $(form).serialize(), // serializes the form's elements.
                   dataType:"JSON",
                   success: function(data)
                   {
                        
                        $('#btnChangePass').text('updating...'); //change button text
                        $('#btnChangePass').attr('disabled', true);
                       if (data.status=="error") //if success close modal and reload ajax table
                       {
                         
                        $("#status_field_pass").attr("class","error");
                         //$("#status_field_pass").html(data.message);
                        
                       } else
                       {
                       // $("#status_field_pass").attr("class","success"); 
                        $('#password_form').trigger("reset");
                         $('#changePasswordModal').modal('hide');
                         $("#toaster-text").text("Your password has been updated");
                                 $("#normal_toaster").addClass("active");
                                setTimeout(function(){
                                    $("#normal_toaster").removeClass("active");
                                }, 5000);
                       

                        //$('#validation-form').trigger("reset");
  
                       }
                        $('#btnChangePass').text('update'); //change button text
                        $('#btnChangePass').attr('disabled', false); //set button enable 
                        //$("#status_field_pass").html(data.message);
                   }
                 });
                return false; 
            }
    });      
    
    </script>
<!-- endbuild -->
    <!-- js group end -->

</body>

</html>