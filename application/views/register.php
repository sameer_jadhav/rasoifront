
<!DOCTYPE html>
<html>

<?php $this->load->view('include/head.php'); ?>

<body>
    
    <?php $this->load->view('include/header.php'); ?>
    <main>
        <div class="bs-main lyt-login typ-signup">
            <div class="bs-login js-element">
                <div class="head-wrap">
                    <h1 class="title">Create Account</h1>
                    <p>Create account with us</p>
                </div>
                <div class="cont-wrap">
                    <form id="validation-form" method="post" action="<?php echo base_url();?>signup/user">
                        <div class="bs-form">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="input-label">Name</label>
                                        <input type="text" class="form-control" placeholder="Enter your Name" name="customer_name" id="customer_name" data-rule-required="true">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="input-label">Email</label>
                                        <input type="text" class="form-control" name="email_id" id="email_id" data-rule-required="true" placeholder="Enter your Email Address">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="input-label">Mobile No.</label>
                                        <input type="text" name="phone_no" id="phone_no" data-rule-required="true" class="form-control" placeholder="Enter your Mobile No.">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="input-label">New Password</label>
                                        <input type="password" name="user_password" id="user_password" data-rule-required="true" class="form-control" placeholder="Create your Password">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="input-label">Re-enter New Password</label>
                                        <input type="password"  id="user_confirm_password" name="user_confirm_password" data-rule-required="true" class="form-control" placeholder="Re-enter your New Password">
                                    </div>
                                </div>
                            </div>
                            <div class="form-action">
                                <button type="submit" class="btn btn-default visible-xs btnLogin" id="btnLogin" data-toggle="modal" data-target="#registrationSuccess">Sign me Up</button>
                                <a href="<?php echo base_url();?>login" type="button" class="btn btn-link cm-text-initial">Already have an account</a>
                                <button type="submit" class="btn btn-default hidden-xs btnLogin" id="btnLogin" data-toggle="modal" data-target="#registrationSuccess">Sign me Up</button>
                            </div>
                        </div>
                        <div class="login-method">
                            <h3 class="title">Sign in with</h3>
                            <ul>
                                <li class="login-item">
                                    <a href="<?Php echo $authUrl;?>" class="icon icon-facebook"></a>
                                </li>
                                <li class="login-item">
                                    <a href="<?php echo $google_login_url;?>" class="icon icon-google"></a>
                                </li>
                            </ul>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
    
    <?php $this->load->view('include/footer.php'); ?>
   <?php $this->load->view('include/footer_2.php'); ?>
   <?php $this->load->view('include/footer_js.php'); ?>

   <script type="text/javascript">
        $(document).ready(function() {
            $("#validation-form").validate({
                rules: {
                        customer_name: "required",
                        
                        user_password: {
                            required: true,
                            minlength: 5
                        },
                        user_confirm_password: {
                            required: true,
                            minlength: 5,
                            equalTo: "#user_password"
                        },
                        email_id: {
                            required: true,
                            email: true
                        },
                        phone_no: {
                            required: true,
                            number: true,
                            minlength : 10,
                            maxlength : 15
                        }
                    },
                     errorElement: "span",
                        errorClass: "error-msg",
                        errorPlacement: function(error, element) {
                             error.appendTo($(element).parent());
                        },
                        highlight: function (element) {
                            $(element).parents(".form-group").addClass("error");
                        },
                        unhighlight: function (element) {
                            $(element).parents(".form-group").removeClass("error");
                        },
                    submitHandler: function (form) {
                        var url = $(form).attr('action');
                        //alert(url)
                        $('.btnLogin').attr('disabled', true);
                        $('.btnLogin').addClass('btn-loader');
                        $.ajax({
                        type: "POST",
                        url: url,
                        data: $(form).serialize(), // serializes the form's elements.
                        dataType:"JSON",
                        success: function(data)
                        {
                                //$('#btnLogin').html('processing...'); //change button text
                                //$('#btnLogin').addClass("btn-load");
                                
                            if (data.status=="error") //if success close modal and reload ajax table
                            {
                                
                                $("#status_field").attr("class","error");
                                $('.btnLogin').removeClass('btn-loader');
                                $('.btnLogin').attr('disabled', false); //set button enable
                                $("#toaster-text").text(data.message);
                                $("#normal_toaster").addClass("active");
                                
                            } else
                            {
                                $("#status_field").attr("class","success"); 
                                $('#validation-form').trigger("reset");
                                //$('.btnLogin').removeClass('btn-loader');
                                window.location.href="<?php echo base_url();?>login"
        
                            }
                                //$('#btnLogin').html('Sign Up'); //change button text
                                //$('#btnLogin').removeClass("btn-load");
                                //$('.btnLogin').attr('disabled', false); //set button enable 
                                //$("#status_field").html(data.message);
                        }
                        });
                        return false; 
                    }
            });
        });
    </script>
    <!-- js group start -->
    <!-- <script type="text/javascript" src="assets/js/plugins/libraries.js"></script>
<!-- endbuild -->
    <!-- js group end -->

</body>

</html>