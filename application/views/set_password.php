
<!DOCTYPE html>
<html>

<?php $this->load->view('include/head.php'); ?>

<body>

    <?php $this->load->view('include/header.php'); ?>

    <main>
        <div class="bs-main lyt-login">
            <div class="bs-login typ-no-img">
                <div class="head-wrap">
                    <h2 class="title">Reset Password</h2>
                </div>
                <div class="cont-wrap">
                    <form method="post" id="form" action="<?php echo base_url('forgot_password/save_password') ?>">
                    <input type="hidden" name="token" value="<?php echo $this->uri->segment('3') ?>">
                        <div class="bs-form">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="input-label">New password</label>
                                        <input type="password" name="password" id="password"  required="" class="form-control" placeholder="Enter your New Password">
                                        <!-- <span class="error-msg">Lorem Ipsum</span> -->
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="input-label">Confirm password</label>
                                        <input type="password" name="confirm_password"  required="" class="form-control" placeholder="Confirm Password">
                                        <!-- <span class="error-msg">Lorem Ipsum</span> -->
                                    </div>
                                </div>
                            </div>

                            <div class="form-action">
                                <button type="submit" id="btnForget" class="btn btn-default">Submit<span class="loader"></span></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
    <?php $this->load->view('include/footer.php'); ?>
    <span class="cm-overlay"></span>
 <?php $this->load->view('include/footer_2.php'); ?>
<?php $this->load->view('include/footer_js.php'); ?>


    <div class="bs-modal typ-message modal fade" id="set-pass-message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bs-video">
                        <div class="bs-message typ-static">
                            <div class="msg-wrap">
                                <div class="ty-head">
                                    <span class="icon icon-remove"></span>
                                    <p class="ty-desc"><?php echo $this->session->flashdata('msg'); ?> </p>
                                </div>
                                <div class="ty-cont">
                                    <button type="button" id="cancel_btn" class="btn btn-outline" data-dismiss="modal">Ok</button>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
    $(document).ready(function () {

        jQuery("#form").validate({
        rules: {
                    password: "required",              
                confirm_password: {
                    required: true,
                        equalTo : "#password"
                }
            },
           errorElement: "span",
                        errorClass: "error-msg",
                        errorPlacement: function(error, element) {
                             error.appendTo($(element).parent());
                        },
                        highlight: function (element) {
                            $(element).parents(".form-group").addClass("error");
                        },
                        unhighlight: function (element) {
                            $(element).parents(".form-group").removeClass("error");
                        },
        });
        var message = "<?php echo $this->session->flashdata('reset_pass_msg'); ?>";
             if(message !=""){
                $('#set-pass-message').modal('show');

             }
    });
</script>
</body>

</html>