
<!DOCTYPE html>
<html>

<?php $this->load->view('include/head.php'); ?>

<body>

    <?php $this->load->view('include/header.php'); ?>

    <main>
        <div class="bs-main lyt-login">
            <div class="bs-login typ-no-img">
                <div class="head-wrap">
                    <h2 class="title">Link Expired.</h2>
                </div>
                <div class="cont-wrap">
                    <form method="post" id="form" action="<?php echo base_url('forgot_password/save_password') ?>">
                    <input type="hidden" name="token" value="<?php echo $this->uri->segment('3') ?>">
                        <div class="bs-form">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="input-label">Your reset password link has been expired.</label>
                                        
                                        <!-- <span class="error-msg">Lorem Ipsum</span> -->
                                    </div>
                                </div>
                            </div>

                            

                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
    <?php $this->load->view('include/footer.php'); ?>
    <span class="cm-overlay"></span>
 <?php $this->load->view('include/footer_2.php'); ?>
<?php $this->load->view('include/footer_js.php'); ?>



</body>

</html>