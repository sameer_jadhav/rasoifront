
<!DOCTYPE html>
<html>

<?php $this->load->view('include/head.php'); ?>

<body class="pg-banner">
    
<?php $this->load->view('include/header.php'); ?>

    <main>
        <div class="bs-main">
            <div class="container">
                <ul class="mod-breadcrumbs">
                    <!--li><a href="#">Homepage</a></li>
                    <li><a href="#" class="active">Cart</a></li-->
                </ul>
                <section>
                    <div class="bs-progress-tab js-element">
                        <ul class="progress-list">
                            <li class="progress-item">
                                <a href="<?php echo base_url();?>cart" class="progress-link completed"><span class="icon icon-cart"></span>In Cart <span class="count">(<?php echo count($cart);?>)</span></a>
                            </li>
                            <li class="progress-item">
                                <a href="<?php echo base_url();?>shipping" class="progress-link active"><span class="icon icon-track"></span>Shipping Details</a>
                            </li>
                            <li class="progress-item">
                                <a href="#" class="progress-link"><span class="icon icon-pay"></span>Payment</a>
                            </li>
                        </ul>
                        <div class="progress-cont">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="shipping-wrap">
                                        <h2 class="title">Shipping Address</h2>
                                        <form  id="select_address" method="post" name="select_address" action="<?php echo base_url('checkout');?>">
                                        <div class="lyt-address">
                                            <ul class="address-list">
                                                <?php 
                                                //echo  $this->session->flashdata('set-address-id');
                                                    $auto_pin = "";
                                                    for($j=0;$j<count($address);$j++){
                                                    if ($address[$j]['shipping_address_is_default']=="true" || ($this->session->flashdata('set-address-id') == $address[$j]['add_id'])){ 
                                                        $auto_pin =  $address[$j]['shipping_postcode'];
                                                    }
                                                    $chk_address = "";
                                                    if($this->session->flashdata('set-address-id') == $address[$j]['add_id']){
                                                        $chk_address = "checked='checked'";
                                                    }
                                                ?>
                                                <li class="address-item" id="address_<?php echo $address[$j]['add_id'];?>">
                                                    <div class="mod-add-card <?php if ($address[$j]['shipping_address_is_default']=="true"): ?>typ-default<?php endif ?>">
                                                        <div class="bs-radiobox typ-card">
                                                            <input type="radio" <?php echo $chk_address ; if ($address[$j]['shipping_address_is_default']=="true"){ echo "checked='checked'"; }?> id="address_radio_<?php echo $address[$j]['add_id'];?>" name="address" value="<?php echo $address[$j]['add_id'];?>" onclick="getPincode(<?php echo $address[$j]['shipping_postcode'];?>)">
                                                            <label for="address_radio_<?php echo $address[$j]['add_id'];?>" >
                                                                <strong class="title"><?php echo $address[$j]['label'];?></strong>
                                                                <address>
                                                                    <strong><?php echo $address[$j]['shipping_first_name']." ".$address[$j]['shipping_last_name'];?></strong>
                                                                    <span><?php echo $address[$j]['shipping_address_1'];?></span>
                                                                    <span><?php echo $address[$j]['shipping_address_2'];?>  <?php echo $address[$j]['shipping_city'];?> <?php echo $address[$j]['shipping_state'];?> <?php echo $address[$j]['shipping_postcode'];?></span>
                                                                </address>
                                                            </label>
                                                        </div>
                                                        <button type="button" class="btn btn-link" onclick="edit_address('<?php echo $address[$j]['add_id']; ?>')">Edit</button>
                                                        
                                                        <?php if ($address[$j]['shipping_address_is_default']=="true"): ?>
                                                            <span class="tag">default</span>
                                                        <?php endif ?>
                                                    </div>
                                                </li>
                                                <?php } ?>
                                            </ul>
                                           
                                        </div>
                                        </form>
                                         <button onclick="add_address()" class="btn btn-add" ><span class="icon icon-plus"></span> Add New Address</button>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="order-wrap">
                                        <div class="order-head">
                                            <h3 class="title">Order Summary</h3>
                                        </div>
                                        <div class="order-cont">
                                            <div class="mod-data-list">
                                                <dl>
                                                    <dt class="data-label">Sub Total</dt>
                                                    <dd class="data-value">&#8377; <span id="cart_subtotal"> <?php echo $cart_total['cart_subtotal'];?></span></dd>
                                                </dl>
                                                
                                                 <dl>
                                                    <dt class="data-label">Delivery Charges</dt>
                                                    <dd class="data-value">&#8377; <span id="cart_shipping"><?php echo $cart_total['shipping_total'];?></span></dd>
                                                </dl>

                                                <dl>
                                                    <dt class="data-label">Tax (5% GST)</dt>
                                                    <dd class="data-value">&#8377; <span id="cart_taxes"><?php echo $cart_total['cart_tax'];?></span></dd>
                                                </dl>
                                               
                                            </div>
                                        </div>
                                        <div class="order-foot">
                                            <div class="mod-data-list">
                                                <dl>
                                                    <dt class="data-label">Total Amount</dt>
                                                    <dd class="data-value"><span class="amount-val">&#8377; <span id="cart_total"><?php echo $cart_total['total'];?></span></span></dd>
                                                </dl>
                                            </div>
                                            <!-- <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Apply Coupon Code">
                                                <button class="btn btn-link input-btn">Apply</button>
                                            </div> -->
                                            <!--div class="input-group">
                                                <input type="text" id="shipping_pincode" value="<?php //echo $this->session->//userdata('pincode');?>" class="form-control" placeholder="Delivery Pincode">
                                                <button class="btn btn-link input-btn" onclick="getShipping()">Change</button>
                                            </div-->
                                            <btn id="continue_checkout" onclick="submit_address()"  class="btn btn-default hidden-xs">Proceed</btn>
                                            <div class="visible-xs cm-fly-action-wrap js-targetEle">
                                                <btn id="continue_checkout" onclick="submit_address()"   class="btn btn-default">Proceed</btn>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </main>
    <?php $this->load->view('include/footer.php'); ?>
    <?php //$this->load->view('include/footer_2.php'); ?>

    <!--div class="bs-modal typ-message modal fade" id="message-alert-pincode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bs-video">
                        <div class="bs-message typ-static">
                            <div class="msg-wrap">
                                <div class="ty-head">
                                    <span class="icon icon-remove"></span>
                                    <p class="ty-desc">Pincode entered for shipping address is not serviceable by us.Please enter different Pincode.</p>
                                </div>
                                <div class="ty-cont">
                                    <button type="button" id="cancel_btn" class="btn btn-outline" data-dismiss="modal">Ok</button>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div-->

    <div class="bs-modal typ-message modal fade" id="message-alert-pincode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="bs-video">
                        <div class="bs-message typ-static">
                            <div class="msg-wrap">
                                <div class="ty-head">
                                    <span class="icon icon-address-error"></span>
                                    <h2 class="ty-title">oops!</h2>
                                    <p class="ty-desc">Your address is not serviceable.</p>
                                </div>
                                <div class="ty-cont">
                                    <p>Please try some other address.</p>
                                    <button type="button" id="cancel_btn" class="btn btn-outline" data-dismiss="modal">Ok</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bs-modal modal typ-form fade" id="addressModal" tabindex="-1" role="dialog" aria-labelledby="addressModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="bs-form">
                <form action="#" id="address_form_modal" class="form address_form" name="address_form_modal">
                    <div class="modal-header">
                        <h2 class="form-title">Add new address</h2>
                    </div>
                    <input type="hidden" id="modal_label" name="add_id">
                    <div class="modal-body">
                    <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="input-label">Name</label>
                                    <input type="text" class="form-control" placeholder="Enter your full name" id="modal_shipping_first_name" name="shipping_first_name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="input-label">Mobile no.</label>
                                    <input type="text" class="form-control" placeholder="Enter 10 digit mobile no." id="modal_mobile_no" name="shipping_mobile_no">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group input-group delivery-input">
                                    <label for="" class="input-label">Pin code</label>
                                    <input type="text" class="form-control" placeholder="Enter pincode" id="modal_shipping_postcode"  name="shipping_postcode">
                                    
                                <span class="error-msg show" id="delivery_error"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="input-label">State</label>
                                    <input type="text"  name="shipping_state" id="shipping_state" class="form-control" value="" readonly>
                                </div>
                            </div>
                             <input type="hidden" name="shipping_country" value="India" readonly>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="input-label">City</label>
                                    <input type="text" name="shipping_city" id="shipping_city" class="form-control" value="" readonly>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="" class="input-label">Address</label>
                                    <input type="text" class="form-control" placeholder="Enter address" id="modal_shipping_address_1" name="shipping_address_1">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="" class="input-label">Landmark</label>
                                    <input type="text"  required="" id="modal_shipping_address_2" name="shipping_address_2" class="form-control" placeholder="Enter address">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group radio-wrap last-child">
                                    <label class="input-label" for="label">type of address</label>
                                    <div class="bs-radiobox">
                                        <input type="radio" id="home" value="home" name="label[]">
                                        <label for="home">home</label>
                                    </div>
                                    <div class="bs-radiobox">
                                        <input type="radio" id="office" value="office" name="label[]">
                                        <label for="office">office</label>
                                    </div>
                                    <div class="bs-radiobox">
                                        <input type="radio" id="other1" value="other" name="label[]">
                                        <label for="other1">other</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group last-child chk-wrap">
                                    <div class="bs-checkbox">
                                        <input type="checkbox" name="shipping_address_is_default"  value="true" id="addressDefault">
                                        <label for="addressDefault"><span class="check"></span>Make this my default address</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-outline" data-dismiss="modal" aria-label="Close">cancel</button>
                        <button type="submit"  id="btnSave" class="btn btn-default">save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="bs-toaster " id="normal_toaster" >
    <p class="toaster-info" id="toaster-text">Item removed from bag. To add item back to yhe bag click on undo.</p>
    <!--button type="button" class="btn btn-link js-close-toaster">Ok</button-->
</div>

    <?php $this->load->view('include/footer_js.php'); ?>
    <?php $this->load->view('include/commonsite_js.php'); ?>
    <script type="text/javascript">

            $("#modal_shipping_postcode").blur(function(){
                var id = $(this).val()
                if(id!=""){
                    var shipping_pin = id;
                    $.ajax({
                    url: "<?php echo site_url('manageaddress/getpincodedata/') ?>/" + id,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data)
                    {
                        console.log(data.pindata)
                        if(data.pindata=="0"){
                            $("#delivery_error").text("Not deliverable on this Pincode")
                            $("#delivery_error").addClass("show")
                            $(".delivery-input").removeClass("success")
                            $(".delivery-input").addClass("error")
                            //$("#delivery_error").css("color","#c1272d")
                            //$(".continue_shipping").attr("disabled","disabled")
                            //$("#message-alert-pincode").modal('show');
                            $("#pincode_status").val("invalid")
                            $('#shipping_state').val();
                            $('#shipping_city').val();

                        }else{
                            $("#delivery_error").addClass("show")
                            $('#shipping_state').val(data.pindata[0].state);
                            $('#shipping_city').val(data.pindata[0].city);
                            $(".delivery-input").removeClass("error")
                            $(".delivery-input").addClass("success")
                            $("#delivery_error").text("Deliverable on this Pincode")

                            $("#pincode_status").val("valid")
                            $("#shipping_pincode").val(shipping_pin)
                        }
                        
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error get data from ajax');
                    }
                });
                }
                
            });

            var save_method = '';            
            function edit_address(id) {
                $(".delivery-input").removeClass("success error")
                $("#delivery_error").removeClass("show")
                $("#js-country-add").val(null)
                $("#js-city").val(null)
                $("#js-state").val(null)

                save_method = 'update';
                $('#address_form_modal')[0].reset(); // reset form on modals                
                $('.form-group').removeClass('has-error'); // clear error class
                $('.help-block').empty(); // clear error string

                //Ajax Load data from ajax
                  
                $.ajax({
                    url: "<?php echo site_url('manageaddress/get_by_id/') ?>/" + id,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data)
                    {        
                              
                        //$('[name="label"]').val(data.meta_value.label);
                        //$("input[name=label[]][value=" + data.meta_value.label + "]").attr('checked', 'checked');
                        //$('[name="shipping_last_name"]').val(data.meta_value.shipping_last_name);
                        //$('[name="shipping_country"]').val(data.meta_value.shipping_country);
                        console.log(data.data.meta_value)
                        $('[name="add_id"]').val(id);
                        $('[name="shipping_address_2"]').val(data.data.meta_value.shipping_address_2);
                        $('[name="shipping_postcode"]').val(data.data.meta_value.shipping_postcode);
                        //$('[name="shipping_address_is_default"]').val(data.data.meta_value.shipping_address_is_default);
                        $('[name="shipping_first_name"]').val(data.data.meta_value.shipping_first_name);
                        $('[name="shipping_address_1"]').val(data.data.meta_value.shipping_address_1);
                        $('[name="shipping_mobile_no"]').val(data.data.meta_value.shipping_mobile_no);
                        //$('[name="shipping_email_id"]').val(data.data.meta_value.shipping_email_id);
                        //$('[name="shipping_city"]').val(data.data.meta_value.shipping_city);
                        //$('[name="shipping_state"]').val(data.data.meta_value.shipping_state);
                         $('[name="shipping_country"]').val(data.data.meta_value.shipping_country);
                         $('#js-country-add').select2("val", data.data.meta_value.shipping_country);

                        //get_state(data.data.meta_value.shipping_country , data.data.meta_value.shipping_state)
                        //$('#js-state').select2("val", data.data.meta_value.shipping_state);
                        $('[name="shipping_state"]').val(data.data.meta_value.shipping_state);
                        // get_city(data.data.meta_value.shipping_state,data.data.meta_value.shipping_city)
                        $('[name="shipping_city"]').val(data.data.meta_value.shipping_city);
                        //$('[name="shipping_address_is_default"]').("val", data.data.meta_value.shipping_address_is_default);
                        if(data.data.meta_value.shipping_address_is_default=="true"){
                            $("input[name='shipping_address_is_default']").prop("checked","checked")
                        }
                        
                        //if()
                        
                        $('#'+data.data.meta_value.label).prop("checked", true);
                        $('#addressModal').modal('show'); // show bootstrap modal when complete loaded
                        $('.modal-title').text('Edit Address'); // Set title to Bootstrap modal title
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error get data from ajax');
                    }
                });

            }
            function add_address() {
                
                var validator = $( "#address_form_modal" ).validate();
                validator.resetForm();
                $(".delivery-input").removeClass("success error")
                $("#delivery_error").removeClass("show")
                $(".form-group").removeClass("error")
                $("#js-country-add").val(null)
                $("#js-city").val(null)
                $("#js-state").val(null)
                save_method = 'add';
                $('#address_form_modal')[0].reset(); // reset form on modals
                $('.form-group').removeClass('has-error'); // clear error class
                $('.help-block').empty(); // clear error string                
                $('#addressModal').modal('show'); // show bootstrap modal
                $('.modal-title').text('add new address'); // Set Title to Bootstrap modal title                
            }

             function save()
                {                  
                   // $('#btnSave').text('saving...'); //change button text
                    $('#btnSave').addClass("btn-load");
                    $('#btnSave').attr('disabled', true); //set button disable 
                    var url;

                    if (save_method=='add') {                        
                        url = "<?php echo site_url('manageaddress/add') ?>";
                    }else{                        
                        url = "<?php echo site_url('manageaddress/update') ?>";
                    }

                    // ajax adding data to database
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: $('.address_form').serialize(),//get data of all address from all div address
                        dataType: "JSON",
                        success: function (data)
                        {
                            console.log(data)
                            if (data.status) //if success close modal and reload ajax table
                            {   
                                //$('#newAddress').modal('hide'); // show bootstrap modal                                
                                //alert('success');
                                location.reload();
                            } else
                            {
                                for (var i = 0; i < data.inputerror.length; i++)
                                {
                                    $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                                    $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
                                }
                            }
                            //$('#btnSave').text('save'); //change button text
                            $('#btnSave').removeClass("btn-load");
                            $('#btnSave').attr('disabled', false); //set button enable 


                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error adding / update data');
                            $('#btnSave').text('save'); //change button text
                            $('#btnSave').attr('disabled', false); //set button enable 

                        }
                    });
                }

            
            function remove_add_modal(btn_data){
                $("#confirm_remove_add").modal('show');
                var id = $(btn_data).attr('data-add-id');
                $("#proceed_btn_remove_add").attr("data-address-id",id)
                $('#cancel_btn_remove_add').on('click', function(e) {
                   $('#confirm_remove_add').modal('hide');
                    return false; 
                })
            }

            $("#proceed_btn_remove_add").on("click",function(){

                var id = $("#proceed_btn_remove_add").attr("data-address-id");
                //alert(id)
                var url = "<?php echo site_url('manageaddress/delete/') ?>/"+id
                // ajax adding data to database
                    $.ajax({
                        url: url,
                        type: "DELETE",
                        data: id,//get data of all address from all div address
                        dataType: "JSON",
                        success: function (data)
                        {

                            if (data.code=="success") //if success close modal and reload ajax table
                            {   
                                $('#confirm_remove_add').modal('hide');
                                $('#address_'+id).hide(); // show bootstrap modal  
                                 $('#add-message-remove').modal('show');                              
                                //alert(data.code);
                                //location.reload();
                            } else
                            {
                                for (var i = 0; i < data.inputerror.length; i++)
                                {
                                    $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                                    $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
                                }
                            }
                            


                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error adding / update data');
                            

                        }
                    });
            
            })

            function remove_add(btn_data){
                var id = $(btn_data).attr('data-add-id');
                //alert(id)
                var url = "<?php echo site_url('manageaddress/delete/') ?>/"+id
                // ajax adding data to database
                    $.ajax({
                        url: url,
                        type: "DELETE",
                        data: id,//get data of all address from all div address
                        dataType: "JSON",
                        success: function (data)
                        {

                            if (data.code=="success") //if success close modal and reload ajax table
                            {   
                                $('#address_'+id).hide(); // show bootstrap modal                                
                               // alert(data.code);
                                //location.reload();
                                 $('#add-message-remove').modal('show');
                            } else
                            {
                                for (var i = 0; i < data.inputerror.length; i++)
                                {
                                    $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                                    $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
                                }
                            }
                            


                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error adding / update data');
                            

                        }
                    });
            }  
            function make_default(add_id){
                
                //alert(id)
                var url = "<?php echo site_url('manageaddress/make_default/') ?>/"+add_id
                // ajax adding data to database
                    $.ajax({
                        url: url,
                        type: "GET",                        
                        dataType: "JSON",
                        success: function (data)
                        {

                            if (data.status=="success") //if success close modal and reload ajax table
                            {   
                                $("#toaster-text").text("Your address has been marked as default");
                                $("#normal_toaster").addClass("active");
                                setTimeout(function(){
                                    $("#normal_toaster").removeClass("active");
                                    location.reload();
                                }, 3000);                                
                                //location.reload();
                            } else
                            {
                                alert('error');
                            }
                            


                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error adding / update data');
                            

                        }
                    });
            }  


       

        jQuery("#address_form_modal").validate({
            
               rules: {
                    shipping_first_name: "required",
                    shipping_address_1: "required",
                    shipping_address_2: "required",
                    shipping_country: "required",
                    shipping_state: "required",
                    /*shipping_email_id: {
                        required: true,
                        email: true
                    },*/
                    shipping_city: "required",
                    shipping_postcode: "required",
                    shipping_mobile_no: {
                        required: true,
                        number: true,
                        minlength : 10,
                        maxlength : 15
                    },
                    'label[]':{ 
                        required:true 
                    },
                   },
                   errorElement: "span",
                        errorClass: "error-msg",
                        errorPlacement: function(error, element) {
                            if ( element.is(":radio") ){
                                error.appendTo($(element).parents('.radio-wrap'));
                            } else { // This is the default behavior 
                                error.appendTo($(element).parent());
                            }
                        },
                        highlight: function (element) {
                            $(element).parents(".form-group").addClass("error");
                        },
                        unhighlight: function (element) {
                            $(element).parents(".form-group").removeClass("error");
                        },
                    
                submitHandler: function() {
                    //$('#btnSave').text('saving...'); //change button text
                    $('#btnSave').addClass("btn-load");
                    $('#btnSave').attr('disabled', true); //set button disable 
                    var url;

                    if (save_method=='add') {                        
                        url = "<?php echo site_url('manageaddress/add') ?>";
                    }else{                        
                        url = "<?php echo site_url('manageaddress/update') ?>";
                    }
              $.ajax({
                        url: url,
                        type: "POST",
                        data: $('.address_form').serialize(),//get data of all address from all div address
                        dataType: "JSON",
                        success: function (data)
                        {

                            if (data.status) //if success close modal and reload ajax table
                            {   
                                //$('#newAddress').modal('hide'); // show bootstrap modal                                
                                //alert('success');
                                location.reload();
                            } else
                            {
                                for (var i = 0; i < data.inputerror.length; i++)
                                {
                                    $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                                    $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-block class set text error string
                                }
                            }
                            $('#btnSave').removeClass("btn-load");
                            //$('#btnSave').text('save'); //change button text
                            $('#btnSave').attr('disabled', false); //set button enable 


                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error adding / update data');
                            //$('#btnSave').text('save'); //change button text
                            $('#btnSave').removeClass("btn-load");
                            $('#btnSave').attr('disabled', false); //set button enable 

                        }
                    });
                }
        
           });
           
           var message = "<?php echo $this->session->flashdata('address-msg'); ?>";
             if(message=="address-ad-success"){
                $("#ad-msg-title").text("Address added Successfully")
                $("#ad-msg-content").text("Address has been added successfully.")
                $('#add-message').modal('show');
             } 

             if(message=="address-up-success"){
                $("#ad-msg-title").text("Address updated Successfully")
                $("#ad-msg-content").text("Address has been updated successfully.")
                $('#add-message').modal('show');
             }  

             if(message=="address-del-success"){
                $("#ad-msg-title").text("Address deleted Successfully")
                $("#ad-msg-content").text("Address has been deleted successfully.")
                //$('#add-message').modal('show');
             }  


   

    function submit_address(){
            $( "#select_address" ).submit();
        }

        $("#select_address").submit(function( event ) {
          if (!$("input[name='address']:checked").val()) {

               //alert('Please select atleast one shipping address');
                $("#toaster-text").text("Please select atleast one shipping address");
                                $("#normal_toaster").addClass("active");
                                setTimeout(function(){
                                    $("#normal_toaster").removeClass("active");
                                }, 5000);
                return false;
                event.preventDefault();
            }else{
                var add_id = $("input[name='address']:checked").val();
                add_id = "address_radio_"+add_id
                var function_add = $("#"+add_id).attr("onclick")
                var pincode = function_add.match(/\d+/); // 123456
                console.log(pincode);
                validatePincode(parseInt(pincode))
                var status = validatePincodepop(parseInt(pincode))
                if(status==0){
                    return false;
                    event.preventDefault();
                }

                //validatePincode()
            }
            
          
        });



    function validatePincodepop(pincode){
            var shipping_pin = pincode;
            var pinstatus = 0;
            if(shipping_pin!=""){
                $.ajax({
                    url: '<?php echo site_url("cart/validate_shipping") ?>',
                    type: "POST",
                    async: false,
                    data: { pincode : shipping_pin},
                    dataType: "JSON",
                    success: function (data)
                    {    
                       

                       if(data=="0"){
                        pinstatus = 0;
                       }else{
                        pinstatus = 1;
                       }
                        //callback(pinstatus);
                      // callback(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('error');

                    }
                });
                return pinstatus;
            }
           
        }



    function getPincode(pincode){
           //getShipping(pincode)
           //alert(pincode)
           if(pincode!="" && pincode!=undefined){
            validatePincode(pincode)
           }
        }

     
           
    function validatePincode(pincode){
            var shipping_pin = pincode;
            if(shipping_pin!=""){
                $.ajax({
                    url: '<?php echo site_url("cart/validate_shipping") ?>',
                    type: "POST",
                    data: { pincode : shipping_pin},
                    dataType: "JSON",
                    success: function (data)
                    {    
                       

                       if(data=="0"){
                        
                        //$("#continue_checkout").attr("disabled","disabled")
                        //$("#continue_checkout").removeAttr("onclick");
                        $("#message-alert-pincode").modal('show');
                        //return false;
                        //preventDefault();
                       }else{
                       
                        $("#shipping_pincode").val(pincode)
                        //$("#continue_checkout").attr("disabled", false)
                        //$("#continue_checkout").attr("onclick","submit_address()");
                        $("#message-alert-pincode").modal('hide');
                        var current_cart_subtotal =  $("#cart_subtotal").text();
                        var current_cart_tax =  $("#cart_taxes").text();
                       //$("#cart_shipping").text(data);
                       /*$("#cart_total").text(Number(data) + Number(current_cart_subtotal) + Number(current_cart_tax))*/
                           
                       }
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('error');

                    }
                });
            }
        }

         $( document ).ready(function() {
            var pincode = "<?php echo $auto_pin;?>"
           if(pincode!=""){
            $("#shipping_pincode").val(pincode)
                //getShipping()
                getPincode(<?php echo $auto_pin;?>);
           }

           
        });
             
         </script>


</body>

</html>