
<!DOCTYPE html>
<html>
<?php $this->load->view('include/head.php'); ?>

<body>
    <div>
        <h1 class="cm-not-in-page">rasoi tatva</h1>
       <?php $this->load->view('include/header.php'); ?>
        <main>
            <div class="bs-main">
                <?php if($status=="success"){ ?>
                <div class="bs-message">
                    <div class="msg-wrap">
                        <div class="ty-head">
                            <span class="icon icon-thankyou"></span>
                            <h2 class="ty-title">Thank you.</h2>
                            <p class="ty-desc">Your order has been placed successfully.</p>
                        </div>
                        <?php 
                        $startDate =  date('d-M', strtotime("+4 days"));
                        $endDate =  date('d-M-Y', strtotime("+6 days"));
                        ?>

                        <div class="ty-cont">
                            <div class="order-data">
                                <p class="order-id">Order ID :<span><?php echo $order_id;?></span></p>
                                <a type="button" href="<?php echo base_url();?>orders" class="btn btn-link">Review Order</a>
                                <p class="order-time"><span class="bold">Estimated Delivery</span><span><?php echo $startDate;?> - <?php echo $endDate;?></span></p>
                            </div>
                            <a type="button" class="btn btn-default" href="<?php echo base_url();?>shop">Shop more</a>
                        </div>
                    </div>
                </div>
                <?php } else if($status=="failure"){?>
                <div class="bs-message">
                    <div class="msg-wrap">
                        <div class="ty-head">
                            <span class="icon icon-fail"></span>
                            <h2 class="ty-title">payment failed!</h2>
                        </div>
                        <div class="ty-cont">
                            <p>Something went wrong with your transaction. <span class="cm-line-break">Please try again to complete your order.</span></p>
                            <a href="<?php echo base_url();?>cart" class="btn btn-default">try again</a>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </main>
    </div>

<?php $this->load->view('include/footer_js.php'); ?>
</body>

</html>