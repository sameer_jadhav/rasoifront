
<!DOCTYPE html>
<html>

<?php $this->load->view('include/head.php'); ?>

<body class="pg-banner">
    
    <?php $this->load->view('include/header.php'); ?>

    <main>
        <div class="bs-main">
                <div class="container">
                    <section>
                        <div class="bs-sec sec-first-child">
                            <div class="sec-head">
                                <h1 class="sec-title">Terms &amp; Conditions</h1>
                            </div>
                            <div class="sec-cont">
                                <div class="bs-static-cont typ-left">
                                <ul class="typ-num">
                                    <li>
                                        <p class="highlight">Company details</p>
                                        <p>Spicemantra Private Limited. (“SMPL”) is the licensed owner of the brand “Rasoi Tatva” and the website <a href="<?php echo base_url();?>" class="cm-underline">https://www.rasoitatva.com</a> ”The Site” from Spicemantra Private Limited. SMPL respects your privacy. This Terms & Conditions provides succinctly the manner your data is collected and used by SMPL on the Site. As a visitor to the Site/ Customer you are advised to please read the Terms & Conditions carefully. By accessing the services provided by the Site you agree to the collection and use of your data by SMPL in the manner provided in this Terms & Conditions.</p>
                                    </li>
                                    <li>
                                        <p class="highlight">Services overview</p>
                                        <p>As part of the registration process on the Site, SMPL may collect the following personally identifiable information about you: Name including first and last name, alternate email address, mobile phone number and contact details, Postal code, Demographic profile (like your age, gender, occupation, education, address etc.) and information about the pages on the site you visit/access, the links you click on the site, the number of times you access the page and any such browsing information.</p>
                                        <p>Eligibility Services of the Site would be available to only selected geographies in India. Persons who are “incompetent to contract” within the meaning of the Indian Contract Act, 1872 including un-discharged insolvents etc. are not eligible to use the Site. If you are a minor i.e. under the age of 18 years but at least 13 years of age you may use the Site only under the supervision of a parent or legal guardian who agrees to be bound by these Terms of Use. If your age is below 18 years your parents or legal guardians can transact on behalf of you if they are registered users. You are prohibited from purchasing any material which is for adult consumption and the sale of which to minors is prohibited.</p>
                                    </li>
                                    <li>
                                        <p class="highlight">License and access to Site</p>
                                        <p>SMPL grants you a limited sub-license to access and make personal use of this site and not to download (other than page caching) or modify it, or any portion of it, except with express written consent of SMPL. This license does not include any resale or commercial use of this site or its contents; any collection and use of any product listings, descriptions, or prices; any derivative use of this site or its contents; any downloading or copying of account information for the benefit of another merchant; or any use of data mining, robots, or similar data gathering and extraction tools. This site or any portion of this site may not be reproduced, duplicated, copied, sold, resold, visited, or otherwise exploited for any commercial purpose without express written consent of SMPL. You may not frame or utilize framing techniques to enclose any trademark, logo, or other proprietary information (including images, text, page layout, or form) of the Site or of SMPL and its affiliates without express written consent. You may not use any tags or any other “hidden text” utilizing the Site’s or SMPL’s name or trademarks without the express written consent of SMPL. Any unauthorized use terminates the permission or license granted by SMPL.</p>
                                    </li>
                                    <li>
                                        <p class="highlight">Account & Registration Obligations</p>
                                        <p>All shoppers have to register and login for placing orders on the Site. You have to keep your account and registration details current and correct for communications related to your purchases from the site. By agreeing to the terms and conditions, the shopper agrees to receive promotional communication and newsletters upon registration. The customer can opt out either by unsubscribing in “My Account” or by contacting the customer service. Pricing all the products listed on the Site will be sold at MRP unless otherwise specified. The prices mentioned at the time of ordering will be the prices charged on the date of the delivery. Although prices of most of the products do not fluctuate on a daily basis but some of the commodities and fresh food prices do change on a daily basis. In case the prices are higher or lower on the date of delivery not additional charges will be collected or refunded as the case may be at the time of the delivery of the order.</p>
                                    </li>
                                    <li>
                                        <p class="highlight">Colours</p>
                                        <p>We have made every effort to display the colours of our products that appear on the Website as accurately as possible. However, as the actual colours you see will depend on your monitor, we cannot guarantee that your monitor’s display of any colour will be accurate. Modification of Terms & Conditions of Service SMPL may at any time modify the Terms & Conditions of Use of the Website without any prior notification to you. You can access the latest version of these Terms & Conditions at any given time on the Site. You should regularly review the Terms & Conditions on the Site. In the event the modified Terms & Conditions is not acceptable to you, you should discontinue using the Service. However, if you continue to use the Service you shall be deemed to have agreed to accept and abide by the modified Terms & Conditions of Use of this Site.</p>
                                        <p>Governing Law and Jurisdiction This User Agreement shall be construed in accordance with the applicable laws of India. The Court shall have exclusive jurisdiction in any proceedings arising out of this agreement. Any dispute or difference either in interpretation or otherwise, of any terms of this User Agreement between the parties hereto, the same shall be referred to an independent arbitrator who will be appointed by SMPL and his decision shall be final and binding on the parties hereto. The above arbitration shall be in accordance with the Arbitration and Conciliation Act, 1996 as amended from time to time. The High Court of judicature alone shall have the jurisdiction and the Laws of India shall apply.</p>
                                        <p>Reviews, Feedback, Submissions All reviews, comments, feedback, postcards, suggestions, ideas, and other submissions disclosed, submitted or offered to the Site on or by this Site or otherwise disclosed, submitted or offered in connection with your use of this Site (collectively, the “Comments”) shall be and remain the property of SMPL. Such disclosure, submission or offer of any Comments shall constitute an assignment to SMPL of all worldwide rights, titles and interests in all copyrights and other intellectual properties in the Comments. Thus, SMPL owns exclusively all such rights, titles and interests and shall not be limited in any way in its use, commercial or otherwise, of any Comments. SMPL will be entitled to use, reproduce, disclose, modify, adapt, create derivative works from, publish, display and distribute any Comments you submit for any purpose whatsoever, without restriction and without compensating you in any way. SMPL is and shall be under no obligation (1) to maintain any Comments in confidence; (2) to pay you any compensation for any Comments; or (3) to respond to any Comments. You agree that any Comments submitted by you to the Site will not violate this policy or any right of any third party, including copyright, trademark, privacy or other personal or proprietary right(s), and will not cause injury to any person or entity. You further agree that no Comments submitted by you to the Website will be or contain libellous or otherwise unlawful, threatening, abusive or obscene material, or contain software viruses, political campaigning, commercial solicitation, chain letters, mass mailings or any form of “spam”. SMPL does not regularly review posted Comments, but does reserve the right (but not the obligation) to monitor and edit or remove any Comments submitted to the Site. You grant SMPL the right to use the name that you submit in connection with any Comments. You agree not to use a false email address, impersonate any person or entity, or otherwise mislead as to the origin of any Comments you submit. You are and shall remain solely responsible for the content of any Comments you make and you agree to indemnify SMPL and its affiliates for all claims resulting from any Comments you submit. SMPL and its affiliates take no responsibility and assume no liability for any Comments submitted by you or any third party.</p>
                                    </li>
                                    <li>
                                        <p class="highlight">Copyright & Trademark</p>
                                        <p>SMPL, its suppliers and licensors expressly reserve all intellectual property rights in all text, programs, products, processes, technology, content and other materials, which appear on this Site. Access to this Website does not confer and shall not be considered as conferring upon anyone any license under any of SMPL or any third party’s intellectual property rights. All rights, including copyright, in this website are owned by or licensed to SMPL. Any use of this website or its contents, including copying or storing it or them in whole or part, other than for your own personal, non-commercial use is prohibited without the permission of SMPL. You may not modify, distribute or re-post anything on this website for any purpose.</p>
                                    </li>
                                    <li>
                                        <p class="highlight">Return-Refund-Cancellation</p>
                                        <ul class="typ-bullet">
                                            <li>
                                                <p>First and foremost, we want you to be happy with your purchase.</p>
                                            </li>
                                            <li>
                                                <p>You do not need a Return Authorization to exchange or return any item assuming it meets the guidelines below. Just ship it back to us, and we will take care of the rest.</p>
                                            </li>
                                        </ul>
                                        <p>We find that the vast majority of our customers do not elect to return or exchange their items, so we have elected to pass along the most competitive prices and shipping rates we can by not paying for return shipping on returns and exchanges.</p>
                                    </li>
                                    <li>
                                        <p class="highlight">Cancellation by Site / Customer</p>
                                        <p>You as a customer can cancel your order anytime before the delivery/dispatch by calling our customer service. In such a case we will refund any payments already made by you for the order. If we suspect any fraudulent transaction by any customer or any transaction which defies the terms & conditions of using the website, we at our sole discretion could cancel such orders. We will maintain a negative list of all fraudulent transactions and customers and would deny access to them or cancel any orders placed by them.</p>
                                    </li>
                                    <li>
                                        <p class="highlight">Return & Refunds</p>
                                        <p>To be eligible for a return, your item must be unused and in the same condition that you received it. It must also be in the original packaging. Please do note that the food products which are ordered is of top quality and it does not contain any preservatives, as a result the shelf life of the products will be below the normally used packaged products. This makes it difficult for us to provide the return facility for food products, hence this facility will not be available for food products.</p>
                                        <p>We have a “no questions asked return and refund policy” which entitles all our members to return the product with in 7 days if due to some reason they are not satisfied with the quality or freshness of the product. We will take the returned product back with us and return the money or issue a credit note for the value of the return products which will be credited to your account on the Site. This can be used to pay your subsequent shopping bills.</p>
                                        <p>All payments for refund of the Transaction Price shall be done within 7 days as follows and in accordance with the provisions contained herein and applicable laws in India, particularly the directions issued by the Reserve Bank of India from time to time for opening and operation of accounts and settlement of payments for electronic payment transactions involving intermediaries dated 24 November 2009.</p>
                                        <ul class="typ-bullet">
                                            <li>
                                                <p>Except for Payment on Delivery Transaction, refund, if any, shall be made at the same Issuing Bank from where Transaction Price was received.</p>
                                            </li>
                                            <li>
                                                <p>For Payment on Delivery Transactions, refunds, if any, will be made via demand draft in favour of the Buyer (as per the details provided by the Buyer at the time of registration with the Website).</p>
                                            </li>
                                            <li>
                                                <p>Refunds shall only be made in Indian Rupees and shall be equivalent to the Transaction Price received. Buyer shall bear any foreign exchange conversion risk, loss, charges or fees, if any.</p>
                                            </li>
                                            <li>
                                                <p>For electronics payments, refund shall be made through the Green mart spices Facility using NEFT / RTGS or any other online banking / electronic funds transfer system approved by Reserve Bank India.</p>
                                            </li>
                                            <li>
                                                <p>Refund shall be subject to the buyer complying with the provisions of the User Agreement and the rules and polices made hereunder and the Company shall have recourse to such refund in case of any misuse by buyer.  For any queries on Return, Refund & cancellation, feel free to connect with us at <span class="cm-line-break"><span class="bold">Email</span> <a href="mailto:customercare@rasoitatva.com" class="cm-underline">customercare@rasoitatva.com</a></span></p>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </main>
    <?php $this->load->view('include/footer.php'); ?>
    <?php $this->load->view('include/footer_2.php'); ?>
    <?php $this->load->view('include/footer_js.php'); ?>
    </body>

</html>