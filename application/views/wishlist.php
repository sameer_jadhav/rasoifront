
<!DOCTYPE html>
<html>

<?php $this->load->view('include/head.php'); ?>

<body class="pg-banner">
    <?php $this->load->view('include/header.php'); ?>

    <main>
        <div class="bs-main">
            <div class="container">
                <ul class="mod-breadcrumbs">
                     <li><a href="<?php echo base_url();?>">home</a></li>
                    <li><a href="<?php echo base_url();?>profile">my account</a></li>
                    <li><a href="#" class="active">Wishlist</a></li>
                </ul>
                <section>
                    <div class="bs-sec sec-first-child">
                        <div class="sec-head">
                            <h2 class="sec-title">
                                <span class="cm-cursive cm-line-break">Hi, <?php echo $userdata['user_display_name'] ?></span>
                            </h2>
                        </div>
                        <div class="sec-cont">
                            <div class="lyt-account">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="nav-wrap hidden-xs">
                                            <ul class="nav-list">
                                                <li class="nav-item">
                                                    <a href="<?php echo base_url();?>profile" class="nav-link "><span class="icon icon-user"></span> Personal Details</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="<?php echo base_url();?>orders" class="nav-link "><span class="icon icon-cart"></span> My Orders <span class="count">(<?php echo count($cust_orders);?>)</span></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="<?php echo base_url();?>manage-address" class="nav-link "><span class="icon icon-agenda"></span> Saved Addresses</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="<?php echo base_url();?>wishlist" class="nav-link active"><span class="icon icon-wishlist"></span> Wishlist</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="bs-custom-select typ-box visible-xs">
                                            <select id="tabSelect">
                                                <option value="profile" data-href="<?php echo base_url();?>profile"> Personal Details</option>
                                                <option value="orders" data-href="<?php echo base_url();?>orders"> My Orders <span class="count">(<?php echo count($cust_orders);?>)</span></option>
                                                <option value="manage-address" data-href="<?php echo base_url();?>manage-address"> Saved Addresses</option>
                                                <option value="wishlist" data-href="<?php echo base_url();?>wishlist" selected> Wishlist</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="cont-wrap">
                                            <div class="bs-wishlist">
                                                <ul class="w-wrap row">
                                                <?php
                                                if(count($products)>0){
                                                for($j=0;$j<count($products);$j++){ ?>
                                                    <li class="w-item col-md-4 col-xs-6" id="wishproductlist<?php echo $products[$j]['id'];?>">
                                                        <div class="bs-product">
                                                            <div class="pd-head">
                                                                <figure class="pd-img-wrap">
                                                                    <img src="<?php echo $products[$j]['images'][0]['src'];?>" alt="<?php echo $products[$j]['name'];?>">
                                                                </figure>
                                                                <div class="pd-desc">
                                                                    <p><?php echo strip_tags($products[$j]['short_description']);?></p>
                                                                </div>
                                                            </div>
                                                            <div class="pd-cont">
                                                                <h3 class="pd-title"><?php echo $products[$j]['name'];?></h3>
                                                                <div class="bs-amount">
                                                                   <strong class="amount-val">&#8377; <?php echo $products[$j]['variations_price'][0];?></strong>
                                                            <strong class="amount-qty">/ <?php echo $products[$j]['attributes'][0]['options'][0];?> gm</strong>
                                                                </div>
                                                                <div class="pd-action">
                                                                    <button type="button" class="btn btn-icon btn-outline js-like-btn hidden-xs" id="btnRemoveWishlist<?php echo $products[$j]['id'];?>" onclick="remove_wishlist_modal('<?php echo $products[$j]['id'];?>')"><span class="icon icon-wishlist-o active"></span></button>
                                                                    <button type="button" class="btn btn-icon btn-outline js-btn-modal" data-toggle="modal" onclick="product_summary(<?php echo $products[$j]['id'];?>);"><span class="icon icon-eye"></span></button>
                                                                    <button type="button" class="btn btn-default js-add-cart" onclick="add_to_cart(this)" data-product-id="<?php echo $products[$j]['id'];?>" data-var-id="<?php echo $products[$j]['variations'][0];?>" id="add_to_cart">add to cart</button>
                                                                </div>
                                                            </div>
                                                            
                                                            <button type="button" class="btn pd-btn-like js-like icon-wishlist-o"  id="btnRemoveWishlist<?php echo $products[$j]['id'];?>" onclick="remove_wishlist_modal('<?php echo $products[$j]['id'];?>')"></button>
                                                            <label class="pd-tag">Greatest Blend</label>
                                                            <div class="pd-action">
                                                                <div class="action-wrap">
                                                                    <button type="button" class="btn btn-icon btn-outline js-like-btn hidden-xs" id="btnRemoveWishlist<?php echo $products[$j]['id'];?>" onclick="remove_wishlist_modal('<?php echo $products[$j]['id'];?>')"><span class="icon icon-wishlist-o active"></span></button>
                                                                    <button type="button" class="btn btn-icon btn-outline js-btn-modal" data-toggle="modal" onclick="product_summary(<?php echo $products[$j]['id'];?>);"><span class="icon icon-eye"></span></button>
                                                                    <button type="button" class="btn btn-icon btn-default js-add-cart" onclick="add_to_cart(this)" data-product-id="<?php echo $products[$j]['id'];?>" data-var-id="<?php echo $products[$j]['variations'][0];?>" id="add_to_cart"><span class="icon icon-cart"></span></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php } 
                                                    }else{ ?>
                                                        <div class="bs-message typ-static">
                                                            <div class="msg-wrap">
                                                                <div class="ty-head">
                                                                    <span class="icon icon-bag"></span>
                                                                </div>
                                                                <div class="ty-cont">
                                                                    <p>No Products in Wishlist</p>
                                                                    <a href="<?php echo base_url();?>shop" type="button" class="btn btn-default">return to shop</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                   <?php  }   ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </main>
    <?php $this->load->view('include/footer.php'); ?>

    <?php $this->load->view('include/footer_2.php'); ?>

    <!-- js group start -->
    <!-- <script type="text/javascript" src="assets/js/plugins/libraries.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script> -->

<!-- build:js assets/js/vendor.js -->
<!-- <script type="text/javascript" src="assets/js/plugins/libraries.js"></script> -->
<?php $this->load->view('include/footer_js.php'); ?>
<?php $this->load->view('include/commonsite_js.php'); ?>
    <!-- js group end -->

</body>

</html>