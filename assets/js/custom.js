var winW = $(window).width();

var winH = $(window).height();
var galleryTop, galleryThumbs;

function getCurrentYear() {
    var d = new Date();
    var year = d.getFullYear();
    $(".js-current-year").text(year);
}

function imgToBg(objItem, objSrc) {
    $(objItem).each(function() {
        var imgSrc = $(this).find(objSrc).attr("src");
        $(this).css("background-image", "url(" + imgSrc + ")");
    });
}

function bannerSwiper() {
    var bannerSwiper = new Swiper(".js-banner .swiper-container", {
        slidesPerView: 1,
        loop: true,
        autoplay: 3e3,
        autoplayDisableOnInteraction: false,
        speed: 1500,
        spaceBetween: 30,
        effect: "fade",
        pagination: ".js-banner .swiper-pagination",
        paginationClickable: true,
    });
}

function mobileBannerResponsive() {
    $(".js-responsive").each(function() {
        var mobileTestImg = $(this).find(".addto-img").data('mbsrc');
        $(this).find(".addto-img").attr("src", mobileTestImg);
        setTimeout(function() {
            imgToBg(".js-addto-img", ".addto-img");
        }, 100);
    });
}

function testominalSlider() {
    var testimonialSlider = new Swiper(".bs-testimonials .swiper-container", {
        loop: true,
        autoplay: 1e4,
        autoplayDisableOnInteraction: false,
        speed: 1500,
        spaceBetween: 30,
        slidesPerView: 1,
        pagination: ".bs-testimonials .swiper-pagination",
        paginationClickable: true,
        nextButton: ".bs-testimonials .swiper-button-next",
        prevButton: ".bs-testimonials .swiper-button-prev",
    });
}

function spiceSlider() {
    var spiceSwiper = new Swiper('.bs-world-spice .swiper-container', {
        speed: 1500,
        spaceBetween: 30,
        slidesPerView: 1,
        nextButton: ".bs-world-spice .swiper-button-next",
        prevButton: ".bs-world-spice .swiper-button-prev",
    })
}

function headerChange() {
    var getUrl = window.location;
    var baseUrl = getUrl.protocol + "//" + getUrl.host + "/";
    if ($('body').hasClass('pg-home') == false) {
        $('.bs-header').removeClass('typ-black');
        $('.bs-header .logo img').attr('src', baseUrl + 'assets/images/logo-black.svg');
    } else {
        $('.bs-header').addClass('typ-black');
        $('.bs-header.typ-black .logo img').attr('src', baseUrl + 'assets/images/logo-white.svg');
    }
}

function tabSelection() {
    $(".bs-tabs").each(function(e) {
        var obj = this;
        var selectId = $(obj).find(".tab-select select").attr("id");
        $("#" + selectId).on("change", function(e) {
            var currSel = e.currentTarget.selectedOptions[0].index;
            var selTab = $(obj).find(".nav.nav-tabs li").eq(currSel);
            $(selTab).find("a").click();
        });
    });
}

function productViewSwiper() {
    var indexEle;
    galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 20,
        slidesPerView: 4,
        freeMode: true,
        loopedSlides: 5, //looped slides should be the same
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
    });
    galleryTop = new Swiper(".gallery-top", {
        spaceBetween: 10,
        slidesPerView: 1,
        thumbs: {
            swiper: galleryThumbs,
        },
    });
    galleryTop.on("onSlideChangeEnd", function(e) {
        galleryTop.slideTo(e.activeIndex);
        $(".bs-view .view-item-list .view-link").removeClass("active");
        $(".bs-view .view-item-list .view-link").eq(e.activeIndex).addClass("active");
    });
    $(".bs-view .view-item-list .view-link").on("click", function() {
        indexEle = $(this).parent(".view-item").index();
        galleryTop.slideTo(indexEle);
        $(".bs-view .view-item-list .view-link").removeClass("active");
        $(this).addClass("active");
    });
}

function moreProductsSwiper() {
    var moreProducts = new Swiper("#moreProductsSwiper .swiper-container", {
        speed: 400,
        slidesPerView: 3,
        spaceBetween: 10,
        nextButton: "#moreProductsSwiper .swiper-button-next",
        prevButton: "#moreProductsSwiper .swiper-button-prev",
        pagination: "#moreProductsSwiper .swiper-pagination",
        paginationClickable: true,
        breakpoints: {
            768: {
                slidesPerView: 2,
                autoHeight: true,
                spaceBetween: 15,
            },
            1024: {
                slidesPerGroup: 1,
                slidesPerView: 2
            }
        }
    });
}

function videoModal() {
    var theModal;
    $('.js-btn-video').on('click', function() {
        theModal = $(this).data("target");
        var videoSrc = $(this).data('src');
        var iframeSrc = videoSrc + '?autoplay=1&modestbranding=1&rel=0&controls=1&showinfo=0&html5=1';
        $(theModal + " iframe").attr("src", iframeSrc);
    });

    $('.bs-modal').on('hidden.bs.modal', function(e) {
        if ($(this).find('iframe').length != 0) {
            $(this).find('iframe').attr('src', '');
        }
    })
}

function dummyFunc() {
    $('.js-add-cart').on('click', function() {
        $('.bs-header .mod-cart').addClass('add');
        $('.cm-overlay').addClass('active');
        $('.bs-cart-fly').addClass('open');
    });
    $('.bs-cart-fly .close-btn').on('click', function() {
        $('.bs-cart-fly').removeClass('open');
        $('.cm-overlay').removeClass('active');
    })
    $('.js-product-remove').on('click', function() {
        $(this).parents('.cart-item').remove();
    });

    $('.js-trigger-toaster').on('click', function() {
        $('.bs-toaster').addClass('active');
        if ($('.bs-toaster').hasClass('active')) {
            setTimeout(function() {
                $('.bs-toaster').removeClass('active');
            }, 3000);
        }
    })

}

function stickyTabs() {
    var divTop = $('.js-nav-target').offset().top;
    var windowTop = $(window).scrollTop() - 0;
    if (windowTop > divTop) {
        if (!$('.js-nav-target').is('.typ-sticky')) {
            $('.js-nav-target').addClass('typ-sticky');
        }
    } else {
        $('.js-nav-target').removeClass('typ-sticky');
    }
}



function stickyCtaRevealMobile() {
    var targetEle = $('.js-targetEle');
    var ele = $('.js-element');
    var eleH = ele.height();
    var topPos = ele.offset().top;
    var bottomPos = topPos + eleH;
    $(window).scroll(function(event) {
        var scrollPos = $(window).scrollTop() + $(window).height();
        if (scrollPos >= topPos && scrollPos < bottomPos) {
            targetEle.addClass('active');
        } else {
            targetEle.removeClass('active');
        }
    });
}

function otpInput() {
    $(".bs-otp .input-group input").on('keyup', function(event) {
        this.value = this.value.replace(/[!,@,#,$,%,^,&,*,(,),_,\-,+,=,\[,\],\{,\},\:,\;,\",\',\?,<,>,\/,|,\\,~,` \.]/g, '');
        var obj = this;
        var key = event.keyCode || event.charCode;
        if ($(obj).val().length == 1) {
            $(obj).next().focus();
        }
        if (key == 8 || key == 46) {
            $(obj).val('').prev().focus();
        }
    })
}

function textTruncate() {
    var str = $('.bs-product.typ-hero .pd-desc p').text();
    var length, ending;
    if (length == null) {
        length = 150;
    }
    if (ending == null) {
        ending = '...';
    }
    if (str.length > length) {
        $('.bs-product.typ-hero .pd-desc p').text(str.substring(0, length - ending.length) + ending);
    } else {
        return str;
    }

}

function selectAndRedirect() {
    $('#tabSelect').on('change', function() {
        $(this).find("option:selected").each(function() {
            var optionValue = $(this).attr('data-href');
            console.log(optionValue);
            window.location.href = optionValue;
        });
    });
}


function addressCardCheck() {
    var target = $('input[name=address]:checked');
    $('.mod-add-card').removeClass('checked');
    target.parents('.mod-add-card').addClass('checked');
    $('input[name=address]').on('change', function() {
        $('.mod-add-card').removeClass('checked');
        $(this).parents('.mod-add-card').addClass('checked');
    });
}



$(function() {
    $("a").each(function() {
        if ($(this).attr("href") == "#" || $(this).attr("href") == "" || $(this).attr("href") == " ") {
            $(this).attr("href", "javascript:void(0)");
        }
    });

    $(document).on('click', function() {
        if ($('.mod-user .user-nav').hasClass('active')) {
            $('.mod-user .user-nav').removeClass('active');
        }
    });
    if ($('.bs-product.typ-hero').length != 0) {
        textTruncate();
    }
    new WOW().init();
    $('.js-like').on('click', function() {
        var obj = $(this);
        $(this).addClass('cm-heart-pulse');
        $(this).toggleClass('active');
        setTimeout(function() {
            obj.removeClass('cm-heart-pulse');
        }, 1000);
    });

    $('.js-like-btn').on('click', function() {
        var obj = $(this);
        var target = obj.children('.icon');
        target.addClass('cm-heart-pulse');
        target.toggleClass('active');
        setTimeout(function() {
            target.removeClass('cm-heart-pulse');
        }, 1000);
    });


    $('.mod-user .link').on('click', function(e) {
        e.stopPropagation();
        if ($(this).next('.user-nav').hasClass('active')) {
            $(this).next('.user-nav').removeClass('active');
        } else {
            $(this).next('.user-nav').addClass('active');
        }
    })

    if ($(".bs-footer").length != 0) {
        getCurrentYear();
    }

    if ($(".js-addto-img").length != 0) {
        imgToBg(".js-addto-img", ".addto-img");
    }

    if ($(".js-banner").length != 0) {
        bannerSwiper();
    }

    if ($(".bs-testimonials").length != 0) {
        testominalSlider();
    }

    if ($('.bs-world-spice').length != 0) {
        spiceSlider();
    }

    if ($('.bs-header').length != 0) {
        headerChange();
    }

    if ($(".bs-tabs").length != 0) {
        tabSelection();
    }

    if ($(".bs-view").length != 0) {
        productViewSwiper();
    }

    if ($("#moreProductsSwiper").length != 0) {
        moreProductsSwiper();
    }

    if (device.mobile() == true) {
        if ($(".js-responsive").length != 0) {
            mobileBannerResponsive();
        }
        if ($('.bs-header').length != 0) {
            $('.bs-header .menu-btn').on('click', function() {
                $(this).toggleClass('active');
                $(this).closest('.bs-header').find('nav').toggleClass('open');
            })
        }
    }

    $("#tabSelect").select2({
        minimumResultsForSearch: -1
    });

    $('.bs-custom-select.typ-default select').select2({
        minimumResultsForSearch: -1,
        placeholder: "Select",
    })

    $('.bs-custom-select.typ-box select').select2({
        minimumResultsForSearch: -1,
    })

    if ($('.js-btn-modal').length != 0) {
        $('.js-btn-modal').click(function(e) {
            var modalTarget = $(this).data('target');
            e.preventDefault();

            $(modalTarget).on('shown.bs.modal', function(e) {
                galleryTop.update();
                galleryThumbs.update();
            });
        });
    }

    if ($('.js-btn-video').length != 0) {
        videoModal();
    }

    if ($('.bs-pd-fly').length != 0) {
        $('.bs-pd-fly .fly-btn').on('click', function() {
            $('.bs-pd-fly').toggleClass('open');
            $('.cm-overlay').toggleClass('active');
        })
    }

    if ($('.bs-loader').length != 0) {
        setTimeout(function() {
            $('.bs-loader').removeClass('active');
        }, 2000)
    }

    if ($('.mobile').length != 0) {
        if ($('.js-element').length != 0) {
            stickyCtaRevealMobile();
        }
        if ($('.lyt-account').length != 0) {
            selectAndRedirect();
        }

    }

    if ($('.bs-otp').length != 0) {
        otpInput();
    }
    // dummy function
    dummyFunc();

    if ($('.cm-overlay').length != 0) {
        $('.cm-overlay').on('click', function() {
            if ($('.bs-pd-fly.open').length != 0) {
                $('.bs-pd-fly').removeClass('open');
            }
            if ($('.bs-cart-fly.open').length != 0) {
                $('.bs-cart-fly').removeClass('open');
            }
            $('.cm-overlay').removeClass('active');
        });
    }

    if ($('#pdSelect').length != 0) {
        $('#pdSelect').on("change", function(e) {
            var target = $(this).val();
            $('.js-tab-content').removeClass('active');
            $('.js-tab-content#' + target).addClass('active');
            $('.cm-loader').addClass('active');
            setTimeout(function() {
                $('.cm-loader').removeClass('active');
            }, 1000);
        });
    }

    if ($('.mod-add-card').length != 0) {
        addressCardCheck();
    }



});

$(window).scroll(function() {
    if ($('.js-nav-target').length != 0) {
        stickyTabs();
    }
});