function get_order_summary(order_id){
    //alert(order_id);
    $.ajax({
                    url: '<?php echo site_url("orders/order_summary") ?>',
                    type: "GET",
                    data: {order_id : order_id},
                    dataType: "JSON",
                    success: function (data)
                    {

                       $("#ord_sum_total").text(data.cart_total);
                       $("#ord_sum_tax").text(data.total_tax);
                       $("#ord_sum_subtotal").text(data.subtotal);
                       $("#ord_sum_shipping").text(data.shipping_total);
                       $("#ord_sum_pay_total").text(data.total);
                       $("#ord_sum_customer_name").text(data.first_name+' '+data.last_name);
                       $("#ord_sum_address").text(data.first_name+' '+data.last_name+','+data.address_1+','+data.address_2+','+data.city+','+data.state+','+data.postcode+','+data.country);
                        //  $('#order').modal('show'); 
                        setTimeout(() => {
                            tooltip(); 
                        }, 500);
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('error');

                    }
                });
}
