function getQuiz(quiz) {
        $.ajax({
            url: "<?php echo site_url('users/getData/quiz') ?>",                
            type: "GET",
            dataType: "json",
            success: function (data) {
                $('select[name="quiz"]').empty().append('<option value="">Select</option>');              
                $.each(data, function (key, value) {
                    $('select[name="quiz"]').append('<option value="' + value.id + '">' + value.title + '</option>');
                });
                if (quiz) { //using while edit user
                    $('select[name="quiz"]').val(quiz);
                }
            }
        });
    }

     function getRm(rm) {
        $.ajax({
            url: "<?php echo site_url('users/getRm/') ?>",                
            type: "GET",
            dataType: "json",
            success: function (data) {
                $('select[name="rm"]').empty().append('<option value="">Select</option>');              
                $.each(data, function (key, value) {
                    $('select[name="rm"]').append('<option value="' + value.id + '">' + value.name + '</option>');
                });
                if (rm) { //using while edit user
                    $('select[name="rm"]').val(rm);
                }
            }
        }); 
    }
    function getAm(rm, am) {
        $.ajax({
            url: "<?php echo site_url('users/getAm/') ?>/"+rm,                
            type: "GET",
            dataType: "json",
            success: function (data) {
                $('select[name="am"]').empty().append('<option value="">Select</option>');              
                $.each(data, function (key, value) {
                    $('select[name="am"]').append('<option value="' + value.id + '">' + value.name + '</option>');
                });
                if (am) { //using while edit user
                    $('select[name="am"]').val(am);
                }
            }
        }); 
    }
            
    function getHq(hq) {
        $.ajax({
            url: "<?php echo site_url('users/getData/hq') ?>",                
            type: "GET",
            dataType: "json",
            success: function (data) {
                $('select[name="hq"]').empty().append('<option value="">Select</option>');              
                $.each(data, function (key, value) {
                    $('select[name="hq"]').append('<option value="' + value.id + '">' + value.title + '</option>');
                });
                if (hq) { //using while edit user
                    $('select[name="hq"]').val(hq);
                }
            }
        });
    }
    function getZone(zone) {
    $.ajax({
        url: "<?php echo site_url('users/getData/zone') ?>",                
        type: "GET",
        dataType: "json",
        success: function (data) {
            $('select[name="zone"]').empty().append('<option value="">Select</option>');              
            $.each(data, function (key, value) {
                $('select[name="zone"]').append('<option value="' + value.id + '">' + value.title + '</option>');
            });
             if (zone) { //using while edit user
                    $('select[name="zone"]').val(zone);
                }
        }
    });
    }
    function getRole() {
        $.ajax({
            url: "<?php echo site_url('users/getData/role') ?>",                
            type: "GET",
            dataType: "json",
            success: function (data) {
                $('select[name="role"]').empty().append('<option value="">Select</option>');                      
                $.each(data, function (key, value) {
                    $('select[name="role"]').append('<option value="' + value.id + '">' + value.title + '</option>');                    
                });
            }
        });
    }